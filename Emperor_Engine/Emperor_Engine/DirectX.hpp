#ifndef __EMP_DIRECT_X_HPP__
#define __EMP_DIRECT_X_HPP__

#include "Windows.hpp"

#if EMP_USE_DIRECTX

#define DIRECTINPUT_VERSION 0x0800

#pragma warning( push )
#pragma warning( disable: 4005 )
#include <d3d11.h>
#include <d3dx11.h>
#include <d3dx10.h>
#include <XAudio2.h>
#include <X3DAudio.h>
#include <dinput.h>
#pragma warning( pop )

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "d3dx11.lib")
#pragma comment (lib, "d3dx10.lib")
#pragma comment (lib, "DXGI.lib")
#pragma comment (lib, "X3DAudio.lib")
#pragma comment (lib, "Dinput8.lib")
#pragma comment (lib, "dxguid.lib")

#endif

#endif