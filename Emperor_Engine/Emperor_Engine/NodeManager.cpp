#include "NodeManager.hpp"

namespace Emperor
   {
   Node<>* NodeManager::createObject()
      {
      auto i = nodeMatrices.push_back(Matrix<float, 4>());
      auto n = new Node<>(i);

      objects.push_back(n);
      activeObjects.push_back(n);

      return n;
      }

   void NodeManager::removeObject(Node<>* o)
      {
      nodeMatrices.erase(o->getIndex());

      auto i = searchList(objects.begin(), objects.end(), o);
      if(i != objects.end())
         objects.erase(i);
      }

   void NodeManager::updateValues()
      {
      for(auto i = activeObjects.begin(), end = activeObjects.end(); i < end; i++)
         (*i)->_updateAbs(Matrix<float, 4>().identity());
      }

   //Still gross?
   static NodeManager __EMP_Node_Manager__;
   }