#include "Node.hpp"
#include "SceneObject.hpp"
#include "NodeManager.hpp"
#include <iPathnode.hpp>

namespace Emperor
   {
	template <class I>
   void Node<I>::setPosition(const Vector<float, 3>& pos)
      {
      Emperor::setPosition(localT, pos);
      }
   template <class I>
   Vector<float, 3> Node<I>::getPosition() const
      {
      return Emperor::getPosition(localT);
      }
   template <class I>
   void Node<I>::translate(const Vector<float, 3>& p)
      {
      Emperor::translate(localT, p);
      }
   template <class I>
   void Node<I>::setScale(const Vector<float, 3>& s)
      {
      Emperor::scale(Vector3(1 / localScale.x(), 1 / localScale.y(),
         1 / localScale.z()), localT);
      localScale = s;
      Emperor::scale(s, localT);
      }
   template <class I>
   void Node<I>::scale(const Vector<float, 3>& s)
      {
      localScale = Vector3(localScale.x() * s.x(), localScale.y() * s.y(),
         localScale.z() * s.z());
      Emperor::scale(s, localT);
      }
   template <class I>
   void Node<I>::setRotation(const Matrix<float, 3>& r)
      {
      Matrix<float, 3> tr;
      tr[0][0] = localScale.x();
      tr[1][1] = localScale.y();
      tr[2][2] = localScale.z();
      tr[0][1] = tr[0][2] = tr[1][0] = tr[1][2] = tr[2][0] = tr[2][1] = 0;

      tr = tr * r;

      localT = Matrix4(Vector4(tr[0], 0), Vector4(tr[1], 0), Vector4(tr[2], 0),
         Vector4(getPosition(), 1));
      }
   template <class I>
   Matrix<float, 3> Node<I>::getRotation() const
      {
      Matrix<float, 3> t;
      for(int i = 0; i < 3; i++)
         {
         for(int j = 0; j < 3; j++)
            t[i][j] = localT[i][j];
         }
      Matrix<float, 3> tr;
      tr[0][0] = 1 / localScale.x();
      tr[1][1] = 1 / localScale.y();
      tr[2][2] = 1 / localScale.z();
      tr[0][1] = tr[0][2] = tr[1][0] = tr[1][2] = tr[2][0] = tr[2][1] = 0;

      return tr * t;
      }
   template <class I>
   void Node<I>::rotate(const Vector<float, 3>& axis, float radians)
      {
      Emperor::rotate(localT, axis, radians);
      }
   template <class I>
   void Node<I>::attachTo(iNode* p)
      {
      //If (p) is not null and current node has a parent, remove the node
      //from the parent
      //If (p) is not null and current node doesn't have a parent, deactivate
      //the node
      //If (p) is not null, add the current node to (p)'s child list
      //If (p) is null and the current node has a parent, remove the node
      //from the parent and active the node
      //set the current node's parent to (p)

		  if (p != nullptr)
		  {
			  if (parent != nullptr)
			  {
				  parent->_removeChild((Node<I>*)p);
				  parent = 0;
			  }
			  else {
				  NodeManager::getPtr()->deactivateObject((Node<>*)this);
			  }
			  ((Node<I>*)p)->_addChild(this);
		  }
		  else {
			  if (parent != nullptr)
			  {
				  parent->_removeChild((Node<I>*)p);
				  parent = 0;

				  NodeManager::getPtr()->activateObject((Node<>*)this);
			  }
		  }

		  parent = (Node<I>*)p;
      }

   template <class I>
   void Node<I>::_removeChild(Node<I>* a)
      {
      //Remove (a) from the child vector
		  auto i = std::find(children.begin(), children.end(), a);
		  if (i != children.end()){
			  children.erase(i);
		  }
      }
   template <class I>
   void Node<I>::_updateAbs(const Matrix<float, 4>& a)
      {
      //Set the absolute transform of the current node and then recursively
      //call the update function on all the children
		  _tempAbs = localT * a;
		  
		  for (auto & element : children){
			  element->_updateAbs(_tempAbs);
		  }
      }
   template <class I>
   void Node<I>::_recalcAbsTransform()
      {
      _tempAbs = localT;
      auto* p = parent;
      while(p)
         {
         _tempAbs = _tempAbs * p->localT;
         p = p->parent;
         }
      }
   template <class I>
   const Matrix<float, 4>& Node<I>::_getAbsTransform() const
      {
      return _tempAbs;
      }
   template <class I>
   void Node<I>::destroy()
      {
      const auto t = children;
      for(auto i = t.begin(), end = t.end(); i < end; i++)
         (*i)->attachTo(0);
      const auto o = objects;
      for(auto i = o.begin(), end = o.end(); i < end; i++)
         (*i)->attachTo(0);
      if(parent)
         parent->_removeChild((Node<I>*)this);
	  NodeManager::getPtr()->deactivateObject((Node<>*)this);
	  NodeManager::getPtr()->removeObject((Node<>*)this);
      delete this;
      }
   template <class I>
   void Node<I>::_notifyDetach(SceneObject<>* a)
      {
      auto i = searchList(objects.begin(), objects.end(), a);
      if(i != objects.end())
         objects.erase(i);
      }

   template class Node < iNode > ;
   template class Node < iPathnode > ;
   }