#ifndef __EMP_RESOURCE_MANAGER_HPP__
#define __EMP_RESOURCE_MANAGER_HPP__

#include "Internals.hpp"

namespace Emperor
   {
   template <class T>
   struct _asyncResource
      {
      T* org;
      iFile* fst;

      void* tmp;

      _asyncResource(T* a, T* def, const String& f) : org(a) 
         {
         fst = File::create();
         fst->open(f.c_str(), std::ios::binary | std::ios::in);
         tmp = malloc(sizeof(T));
         memcpy(tmp, org, sizeof(T));
         memcpy(org, def, sizeof(T));
         }

      ~_asyncResource()
         {
         memcpy(org, tmp, sizeof(T));
         free(tmp);
         delete fst;
         }
      };

   template <class T>
   class ResourceManager
      {
      private:
      protected:
         HashMap<String, T*> resources;
         ArrayList<_asyncResource<T>*> defered;

         virtual T* createResource(const String& r) = 0;
         virtual void resolveLoad(byte*, uint32, T*) {}
      public:
         ResourceManager() {}
         virtual ~ResourceManager()
            {
            //Ignore this code (deferred loader)
            while(!defered.empty())
               {
               delete defered.back();
               defered.pop_back();
               }

            //Iterator over all objects and destroy them
            //Keep in mind that the destroy function automatically removes
            //them from the manager's array
			while (!resources.empty()){
				resources.begin()->second->destroy();
				}
            }

         void deferLoad(T* org, T* def, const String& f)
            {
            defered.push_back(new _asyncResource<T>(org, def, f));
            }

         void tickResources()
            {
            uint32 i = defered.size();
            while(i--)
               {
               auto& v = defered[i];
               if(v->fst->isLoaded())
                  {
                  uint32 cnt = v->fst->size();
                  byte* data = (byte*)malloc(cnt);
                  v->fst->read(data, cnt);
                  auto o = v->org;
                  delete v;
                  defered.erase(defered.begin() + i);

                  resolveLoad(data, cnt, o);
                  free(data);
                  }
               }
            }

         T* newResource(const String& r)
            {
            //find if the string (r) is already a key in the map
            //if so, emit a resource error (use EMP_RESOURCE_ERROR macro)
            //if not, create a new object, assign it to the map with (r) as
            //the key, then return the object
				if (resources.find(r) != resources.end()){
					EMP_RESOURCE_ERROR("Resource Already Exists!");
					return nullptr;
				}
				else{
					resources[r] = new T;
					return resources[r];
				}
            }

         T* getResource(const String& resource)
            {
            //Find if the string (r) is a key in the map
            //if so, return the object
            //if not, call the createResource function, and return the object
				if (resources.find(resource) != resources.end()){
					return resources[resource];
				}
				else{
					createResource(resource);
					return resources[resource];
				}
            }

         void removeResource(T* a)
            {
            //find the resource and remove it from the map
				auto i = resources.begin();
				while (i != resources.end()){
					if (a == i->second){
						resources.erase(i);
						i = resources.begin();
					}
					else
						++i;
				}
            }

         void preloadResources(const ArrayList<String>& s)
            {
            auto end = s.end();
            for(auto i = s.begin(); i<end; i++)
               getResource(*i);
            }
      };
   }
#endif