#ifndef __EMP_INTERNAL_DEFINES_HPP__
#define __EMP_INTERNAL_DEFINES_HPP__

#define HashMap std::map
#define Pair std::pair
#define ArrayList std::vector
#define LinkedList std::list
#define searchList std::find
#define sortList std::sort
#define getLine std::getline
#define boolText std::boolalpha
#define boolInt std::noboolalpha
#define PI 3.14159265358979323846f
#define EMP_SHADOW_CAM_ID 99
#define EMP_SHADOW_TEX_SLOT 0

#define NO_VIRT(type, ptr, fnc) ((type*)(ptr))->type::fnc
#define INPUT_CALLBACK(N) void(*N)(Emperor::Input::iInput*,uint64,void*,void*)
#define INPUT_CALLBACK_T void(*)(Emperor::Input::iInput*,uint64,void*,void*)
#define INPUT_CALLBACK_L [](Emperor::Input::iInput* a,uint64 b,void* c,void* d)

#define MAX_NODES 4096
#define NODE_SIZE 64

#define ON 1
#define OFF 0

//Basic and temporary log system
#define LOG(m)  Emperor::_Logger::stream << m , Emperor::_logCall()

#endif