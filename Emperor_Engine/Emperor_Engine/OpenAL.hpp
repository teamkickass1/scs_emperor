#ifndef __EMP_OPEN_AL_HPP__
#define __EMP_OPEN_AL_HPP__

#if EMP_USE_OPENAL
#include "Dependencies\AL\al.h"
#include "Dependencies\AL\alc.h"

#pragma comment (lib, "OpenAL32.lib")
#endif

#endif