#ifndef __EMP_AUD_API_SOUND_HPP__
#define __EMP_AUD_API_SOUND_HPP__
#include "../OpenAL.hpp"
#include "../DirectX.hpp"
#include "InternalTypes.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class APISound{};

#if EMP_USE_DIRECTX

      template<>
      class APISound<AS_DX11>
         {
         private:
            byte* data;
            uint32 cnt;
            WAVEFORMATEXTENSIBLE wfx;

            
            
         protected:
            HRESULT FindChunk(HANDLE hFile, uint32 fourcc, uint32 & dwChunkSize, uint32 & dwChunkDataPosition);
            HRESULT ReadChunkData(HANDLE hFile, void * buffer, uint32 buffersize, uint32 bufferoffset);
         public:
            APISound() : data(0), cnt(0)
               {
               ZeroMemory(&wfx, sizeof(wfx));
               }
            virtual ~APISound();

            void loadFromFile(const String&);
            void fill(const byte*, uint32);

            const byte* getData() {return data;}
            uint32 getSize() {return cnt;}
            uint32 getFrequency() {return 0;}
            const WAVEFORMATEXTENSIBLE& _exposeFormat() {return wfx;}
         };
#endif

#if EMP_USE_OPENAL

      template<>
      class APISound<AS_AL11>
         {
         private:
            byte* data;
            uint32 cnt;
            uint32 freq;
            ALenum wfx;

         public:
            APISound() : data(0), cnt(0), wfx(0), freq(0) {}
            virtual ~APISound();

            void loadFromFile(const String&);
            void fill(const byte*, uint32);

            const byte* getData() {return data;}
            uint32 getSize() {return cnt;}
            uint32 getFrequency() {return freq;}
            ALenum _exposeFormat() {return wfx;}
         };


#endif
      }

   }

#endif