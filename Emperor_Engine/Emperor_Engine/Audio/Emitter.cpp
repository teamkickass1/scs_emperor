#include "Emitter.hpp"
#include "Sound.hpp"
#include "Managers.hpp"
#include "../Node.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      void Emitter<AS>::play2D()
         {
         if(snd && !playing && !paused)
            {
            deactivate();
            emt.bindSound();
            emt.start();
            playing = emt.isPlaying();
            paused = false;
            }
         }

      
      template <AudioSystem AS>
      void Emitter<AS>::play3D() 
         {
         if(snd && !playing && !paused)
            {
            activate();
            emt.bindSound();
            emt.start();
            playing = emt.isPlaying();
            paused = false;
            }
         }
      
      template <AudioSystem AS>
      void Emitter<AS>::setSound(const LString& name) 
         {
         setSound(SoundManager<AS>::getPtr()->getResource(name.stdString()));
         }

      template <AudioSystem AS>
      void Emitter<AS>::setSound(const iSound* sound) 
         {
         if(!sound)
            deactivate();
         snd = (Sound<AS>*)sound;
         if(sound)
            {
            emt._setFormat(snd->_exposeAPI()._exposeFormat());
            emt.updateBuffer(snd->_exposeAPI().getData(), snd->_exposeAPI().getSize(), snd->_exposeAPI().getFrequency());
            emt.init();
            }
         else
            {
            emt.updateBuffer(0,0,0);
            //emt.init();
            }
         if(active)
            update();
         }


      template <AudioSystem AS>
      void Emitter<AS>::play() 
         {
         if(active)
            play3D();
         else
            play2D();
         }



      template <AudioSystem AS>
      void Emitter<AS>::pause() 
         {
         if(isPlaying())
            {
            emt.pause();
            paused = true;
            playing = false;
            }
         }
      template <AudioSystem AS>
      void Emitter<AS>::unpause() 
         {
         emt.unpause();
         paused = false;
         playing = emt.isPlaying();
         }


      template <AudioSystem AS>
      void Emitter<AS>::stop() 
         {
         emt.stop();
         paused = false;
         playing = false;
         //deactivate();
         }

      template <AudioSystem AS>
      void Emitter<AS>::setRepeat(bool r) 
         {
         emt.setRepeat(repeat = r);
         }

      template <AudioSystem AS>
      void Emitter<AS>::setVolume(float v) 
         {
         emt.setVolume(volume = v);
         }

      template <AudioSystem AS>
      void Emitter<AS>::activate() 
         {
         if(node && snd && !active)
            {
            EmitterManager<AS>::getPtr()->activateObject(this);
            active = true;
            }
         else
            LOG("Only inactive emitters attached to a node and sound can become active...");
         }


      template <AudioSystem AS>
      void Emitter<AS>::deactivate() 
         {
         if(active)
            {
            EmitterManager<AS>::getPtr()->deactivateObject(this);
            emt.resetState();
            active = false;
            }
         }
      
      template <AudioSystem AS>
      void Emitter<AS>::update() 
         {
         emt.update(node->_getAbs());
         }



      template <AudioSystem AS>
      void Emitter<AS>::destroy() 
         {
         deactivate();
         EmitterManager<AS>::getPtr()->removeObject(this);
         delete this;
         }

#if (EMP_USE_OPENAL == ON)
      template class Emitter<AS_AL11>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Emitter<AS_DX11>;
#endif

      }
   }