#ifndef __EMP_AUD_EMITTER_HPP__
#define __EMP_AUD_EMITTER_HPP__

#include <Audio/iEmitter.hpp>
#include "APIEmitter.hpp"
#include "Sound.hpp"
#include "../SceneObject.hpp"

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class Emitter : public SceneObject<iEmitter>
         {
         private:
            APIEmitter<AS> emt;
            Sound<AS>* snd;

            float volume;
            bool repeat;
            bool paused;
            bool playing;
         protected:
            virtual ~Emitter() {}

         public:
            Emitter() : snd(0), volume(1), repeat(false), paused(false), playing(false) {}

            void setSound(const LString& name);
            void setSound(const iSound* mesh);
            const iSound* getSound() {return snd;}

            void play();
            void play2D();
            void play3D();
            bool isPlaying() {return playing = emt.isPlaying();}

            void pause();
            void unpause();
            bool isPaused() {return paused;}

            void stop();

            void setRepeat(bool);
            bool isRepeat() {return repeat;}

            void setVolume(float);
            float getVolume() {return volume;}

            void activate();
            void deactivate();

            void update();
            void destroy();
         };
      }
   }

#endif