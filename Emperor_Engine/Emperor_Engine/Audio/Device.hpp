#ifndef __EMP_AUD_DEVICE_HPP__
#define __EMP_AUD_DEVICE_HPP__
#include "../OpenAL.hpp"
#include "../DirectX.hpp"
#include <Audio/Types.hpp>

namespace Emperor
   {
   namespace Audio
      {
      template <AudioSystem AS>
      class Device {};

      template <AudioSystem AS>
      class APIEmitter;

      template <AudioSystem AS>
      class APIListener;

#if EMP_USE_DIRECTX
      template<>
      class Device<AS_DX11>
         {
         private:
            IXAudio2* engine;
            IXAudio2MasteringVoice* master;
            X3DAUDIO_HANDLE x3h;
            X3DAUDIO_DSP_SETTINGS set;
            XAUDIO2_DEVICE_DETAILS vd;
            float* matrix;
         protected:
         public:
            Device();
            virtual ~Device();

            void init();
            void release();

            bool isActive() {return engine!=nullptr;}
            void updateSound(APIEmitter<AS_DX11>*, APIListener<AS_DX11>*);
            uint32 getOutputChannels() {return vd.OutputFormat.Format.nChannels;}


            IXAudio2* _exposeDevice() {return engine;}
            IXAudio2MasteringVoice* _exposeMaster() {return master;}
         };
#endif

#if EMP_USE_OPENAL
      template<>
      class Device<AS_AL11>
         {
         private:
            ALCdevice* dev;
            ALCcontext* con;
         protected:
         public:
            Device() : dev(0), con(0) {}
            virtual ~Device();

            void init();
            void release();

            bool isActive() {return dev!=nullptr;}
            void updateSound(APIEmitter<AS_AL11>*, APIListener<AS_AL11>*);
            uint32 getOutputChannels() {return 0;}//vd.OutputFormat.Format.nChannels;}


            ALCdevice* _exposeDevice() {return dev;}
            ALCcontext* _exposeContext() {return con;}
         };
#endif
      }
   }

#endif