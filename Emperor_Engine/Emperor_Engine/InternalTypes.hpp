#ifndef __EMP_INTERNAL_TYPES_HPP__
#define __EMP_INTERNAL_TYPES_HPP__

#include "Errors.hpp"
#include <Core/Types.hpp>
#include <fstream>

namespace Emperor
   {
   typedef std::string String;
   typedef std::stringstream StringStream;

   enum ios
      {
      in = std::ios_base::in,
      out = std::ios_base::out,
      trunc = std::ios_base::trunc,
      app = std::ios_base::app,
      ate = std::ios_base::ate,
      binary = std::ios_base::binary
      };

   class File : public iFile
      {
      private:
         std::fstream file;
         uint32 sz;
      public:
         File() { sz = 0; }
         File(const String& r, std::ios::openmode i, bool)
            { file.open(r, i); }

         void open(const char* filename, std::ios::openmode i, bool) override
            { 
            file.open(filename, i); 
            if(file.is_open())
               {
               uint32 i = (uint32)file.tellg();
               file.seekg(0, std::ios::end);
               sz = (uint32)file.tellg();
               file.seekg(i, std::ios::beg);
               }
            else
               sz = 0;
            }

         bool isLoaded()
            { return file.is_open(); }
         uint32 size()
            { return sz; }
         bool isError()
            { return !isLoaded(); }
         void read(byte* b, uint32 i)
            { file.read((char*)b, i); }
         bool good()
            { return file.good(); }
         char get()
            { return file.get(); }

         static iFile*(*create)();
      };


   template <class D, bool autoClean = false>
   class Singleton
      {
      private:
         static D* self;

         struct Cleaner
            {
            D*& target;
            Cleaner(D*& t) : target(t) {}
            ~Cleaner()
               {
               if(autoClean && target)
                  delete target;
               }
            };
         static Cleaner clean;
      protected:
      public:
         Singleton()
            {
            clean;
            if(!self)
               self = (D*)this;
            else
               EMP_FATAL_ERROR("A singleton object has been instantiated twice");
            }
         virtual ~Singleton()
            {
            self = 0;
            }

         static D* getPtr() { return self; }
      };

   template <class D, bool A>
   D* Singleton<D, A>::self = nullptr;

   template <class D, bool A>
   typename Singleton<D, A>::Cleaner Singleton<D, A>::clean(Singleton<D, A>::self);
   };

#endif