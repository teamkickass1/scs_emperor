#include <string>
#include <map>

namespace Emperor{
	class iPathnode : public iNode{
	private:
	public:
		iPathnode(){}
		~iPathnode(){}
		virtual void setID(int n) = 0;
		virtual void setNeighbour(int ID, iPathnode* neighbour) = 0;
		virtual std::map<int, iPathnode*> getNeighbours() = 0;
		virtual int getID() = 0;
	};
}