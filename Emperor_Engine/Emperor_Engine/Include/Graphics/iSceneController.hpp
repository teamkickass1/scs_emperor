#ifndef __EMP_GFX_I_SCENE_CONTROLLER_HPP__
#define __EMP_GFX_I_SCENE_CONTROLLER_HPP__

namespace Emperor
   {
   class iNode;
   class iPathnode;
   class iPathSolver;
   namespace Graphics
      {
      //Prototypes for other interfaces
      //Reduces need for includes
      class iActor;
      class iCamera;
      class iLight;

      /** Scene Controller Interface

      A simple controller class meant to provide the client programmer with
      the ability to create new objects in the simulated scene.
      */
      class iSceneController
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iSceneController() {}
            virtual ~iSceneController() {}
         public:

            //Creates a new empty scene object and registers it with the
            //associated scene manager.
            virtual iActor* createActor() = 0;
            virtual iCamera* createCamera() = 0;
            virtual iLight* createLight() = 0;
			virtual iNode* createNode() = 0;

			virtual iPathnode* createPathnode() = 0;
			virtual iPathSolver* createPathSolver(iPathnode* n, iNode* a) = 0;
         };
      }
   }

#endif