#ifndef __EMP_GFX_I_ENGINE_HPP__
#define __EMP_GFX_I_ENGINE_HPP__

#include "../Core/EmperorAPI.hpp"

//Basic Graphic Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Utility
      {
      class iWindow;
      }
   namespace Graphics
      {
      //Prototypes for other interfaces
      //Reduces need for includes
      class iResourceController;
      class iSceneController;

      /** Graphics Engine Interface

      Serves as the entry point into the Emperor Engine's graphics sub-engine.
      Primarily provides initialization and shutdown functionality for the
      3D device as well as some basic high level controls (render, fullscreen).
      Primary access to engine functionality comes from the retreival of the
      scene and resources controller interfaces.
      */
      class iEngine
         {
         private:
            //Allows global functions to access protected functions
            EMP_API friend iEngine* createEngine(RenderSystem, const LString&);
            EMP_API friend void releaseEngine(iEngine*);
         protected:
            //protected to prevent from standard deallocation
            virtual ~iEngine() {}
         public:
            //Initializes the engine's controllers
            virtual void initialize() = 0;
            //Releases any initialized devices or resources
            virtual void release() = 0;

            //Turns fullscreen mode on or off
            virtual void setFullScreen(bool) = 0;
            //Returns the current screen's fullscreen state
            virtual bool isFullScreen() = 0;

            //Turns vsync mode on or off
            virtual void setVsync(uint32) = 0;
            //Returns the current screen's vsync state
            virtual uint32 getVsync() = 0;

            //Sets the multisample count for antialiasing functionlaity.
            //must be a multipe of 2 and must be called before device is 
            //activated
            virtual void setMultisampleCount(Emperor::byte count) = 0;
            //Returns the multisample count of the system
            virtual Emperor::byte getMultisampleCount() const = 0;

            //Returns the resource path of the system
            virtual LString getResourceFolder() const = 0;

            //Aquires and actives the 3D device using the provided window
            //pointer, must be done before rendering can start, must be done
            //after engine has been initialized
            virtual void activateDevice(Utility::iWindow* window) = 0;
            virtual void activateDevice(WindowID hwnd, uint32 windowWidth, 
               uint32 windowHeight) = 0;

            //Returns the resource controller interface for the engine,
            //use to manage various graphical resources
            virtual iResourceController* getResourceController() = 0;

            //Returns the scene controller interface for the engine, use to
            //manage the simulation scene
            virtual iSceneController* getSceneController() = 0;

            //Causes the engine to do a single render from all active cameras
            virtual void render() = 0;
         };

      //Creates an instance of the graphics sub-engine. Requires the render 
      //system to be used (GL43 or DX11) and the path to the resources
      EMP_API iEngine* createEngine(RenderSystem rs, const LString& resourcePath);

      //Destroys the graphics sub-engine
      EMP_API void releaseEngine(iEngine*);
      }
   }

#endif