#ifndef __EMP_GFX_I_ACTOR_HPP__
#define __EMP_GFX_I_ACTOR_HPP__

//Core Scene Object required
#include "../Core/iSceneObject.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      //Prototypes for other interfaces
      //Reduces need for includes
      class iMesh;
      class iMaterial;

      /** Actor Interface

      A single renderable entity in the scene. Actor is a lightweight class
      that acts as a junction for all the components that make up a visible
      entity in a scene. A complete actor must have a mesh (to define its 
      shape), a material (to define how it is to be rendered), and a node (to 
      define its spatial information in the scene).
      */
      class iActor : public iSceneObject
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iActor() {}
            virtual ~iActor() {}
         public:
            //Sets the mesh for the actor. If a resource or file name is
            //provided, the mesh resource manager will be queried and the mesh
            //will be loaded if not already available
            virtual void setMesh(const LString& name) = 0;
            virtual void setMesh(const iMesh* mesh) = 0;

            //Returns an iMesh pointer to the mesh currently assigned to the 
            //actor, null if no mesh is assigned
            virtual const iMesh* getMesh() = 0;

            //Sets the material for the actor. If a resource or file name is
            //provided, the material resource manager will be queried and the 
            //material will be loaded if not already available
            virtual void setMaterial(const LString& name) = 0;
            virtual void setMaterial(const iMaterial* mat) = 0;

            //Returns an iMaterial pointer to the material currently assigned 
            //to the actor, null if no material is assigned
            virtual iMaterial* getMaterial() = 0;
         };
      }
   }

#endif