#ifndef __EMP_GFX_TYPES_HPP__
#define __EMP_GFX_TYPES_HPP__

#include "../Core/Types.hpp"

namespace Emperor
   {
   namespace Graphics
      {
////////System Wide

      //An enum for the supported render systems
      enum RenderSystem
         {
         RS_DX11, //Direct X 11
         RS_GL43 //OpenGL 4.3
         };

////////Blend States

      //List of blending factors
      enum BlendOption
         {
         BO_ZERO,
         BO_ONE,
         BO_SRC_COLOR,
         BO_INV_SRC_COLOR,
         BO_SRC_ALPHA,
         BO_INV_SRC_ALPHA,
         BO_DEST_ALPHA,
         BO_INV_DEST_ALPHA,
         BO_DEST_COLOR,
         BO_INV_DEST_COLOR,
         BO_SRC_ALPHA_SAT,
         BO_BLEND_FACTOR,
         BO_INV_BLEND_FACTOR,
         BO_SRC1_COLOR,
         BO_INV_SRC1_COLOR,
         BO_SRC1_ALPHA,
         BO_INV_SRC1_ALPHA
         };

      //List of blending operations
      enum BlendOperation
         {
         BO_ADD,
         BO_SUBTRACT,
         BO_REV_SUBTRACT,
         BO_MIN,
         BO_MAX
         };

      //Struct used to define how a blend state should work
      struct BlendStateDescription
         {
         bool blendEnabled;//If blending will take place, overwrite if false
         BlendOption srcBlend;//Source blend factor, coming from shader
         BlendOption destBlend;//Destination blend factor, on render target
         BlendOperation blendOp;//How the colors will blend (ex. src + dst)
         BlendOption srcBlendAlpha;//Source blend factor for alpha channel
         BlendOption destBlendAlpha;//Dest blend factor for alpha channel
         BlendOperation blendOpAlpha;//How the alpha will blend
         uint8 writeMask;//Which bits can be written to when multisampling
         BlendStateDescription() : blendEnabled(false), srcBlend(BO_ONE),
            destBlend(BO_ONE), srcBlendAlpha(BO_ONE), destBlendAlpha(BO_ZERO),
            writeMask(0x0f), blendOp(BO_ADD), blendOpAlpha(BO_ADD)
            {}
         };

/////////Raster State

      enum FillMode
         {
         FM_POINT,
         FM_WIREFRAME,
         FM_SOLID
         };

      enum CullMode
         {
         CL_NONE,
         CL_FRONT,
         CL_BACK
         };

      struct RasterizerStateDescription
         {
         FillMode fillMode;
         CullMode cullMode;
         bool windCounterClockwise;
         int depthBias;
         float depthBiasClamp;
         float slopeScaledDepthBias;
         bool depthClipEnabled;
         bool scissorEnabled;
         bool multisampleEnabled;
         bool antialiasedLineEnabled;

         RasterizerStateDescription() : fillMode(FM_SOLID), cullMode(CL_BACK),
            windCounterClockwise(false), depthBias(0), depthBiasClamp(0),
            slopeScaledDepthBias(0), depthClipEnabled(true), scissorEnabled(false),
            multisampleEnabled(false), antialiasedLineEnabled(false)
            {}
         };


      /////////Lights

      enum LightType
         {
         LT_DIRECTIONAL,
         LT_POINT,
         LT_SPOT
         };

      /////////Materials
      enum MaterialPassDescriptionOptions
         {
         MPDO_NONE = 0,
         MPDO_VERTEX = 1,
         MPDO_GEOMETRY = 2,
         MPDO_VG = 3,
         MPDO_FRAGMENT = 4,
         MPDO_VF = 5,
         MPDO_GF = 6,
         MPDO_VGF = 7
         };

      struct MaterialPassDescription
         {
         uint32 loopCount;
         bool loopThroughLights;
         bool loopThroughOneLightType;
         bool loopThroughShadowLights;
         LightType lightLoopType;

         MaterialPassDescriptionOptions useCamera;
         uint32 cameraSlot;
         MaterialPassDescriptionOptions useActor;
         uint32 actorSlot;
         MaterialPassDescriptionOptions useSingleLight;
         uint32 singleLightSlot;
         MaterialPassDescriptionOptions useMaterialBuffer;
         uint32 materialSlot;
         bool useShadowTexture;

         LArray<LString> textures;
         LArray<MaterialPassDescriptionOptions> textureOptions;

         LArray<LString> samplers;
         LArray<MaterialPassDescriptionOptions> samplerOptions;

         LString blendState;

         LString vertexFilename;
         LString vertexFunction;

         LString geometryFilename;
         LString geometryFunction;

         LString fragmentFilename;
         LString fragmentFunction;

         LString vertexFormat;

         MaterialPassDescription() : loopCount(1), loopThroughLights(false),
            loopThroughOneLightType(false), loopThroughShadowLights(false),
            lightLoopType(LT_DIRECTIONAL), useCamera(MPDO_NONE), 
            cameraSlot(0), useActor(MPDO_NONE), actorSlot(1), 
            useSingleLight(MPDO_NONE), singleLightSlot(2), 
            useMaterialBuffer(MPDO_NONE), materialSlot(3), 
            useShadowTexture(false) {}
         };

      //primitives
      enum PrimitiveTopologyEnum
         {
         PT_POINT_LIST,
         PT_LINE_LIST,
         PT_LINE_STRIP,
         PT_TRIANGLE_LIST,
         PT_TRIANGLE_STRIP,
         PT_LINE_LIST_ADJ,
         PT_LINE_STRIP_ADJ,
         PT_TRIANGLE_LIST_ADJ,
         PT_TRIANGLE_STRIP_ADJ
         };

      struct PrimitiveTopology
         {
         PrimitiveTopologyEnum pte;

         PrimitiveTopology() : pte(PT_TRIANGLE_LIST) {}
         PrimitiveTopology(PrimitiveTopologyEnum a) : pte(a) {}

         PrimitiveTopology& operator=(PrimitiveTopologyEnum a)
            {
            pte = a;
            return *this;
            }
         };

      //Formats
      enum StructureFormat
         {
         SF_UNKNOWN_FORMAT,
         SF_4X4BYTE_TYPELESS,
         SF_4X4BYTE_FLOAT,
         SF_4X4BYTE_UINT,
         SF_4X4BYTE_SINT,
         SF_3X4BYTE_TYPELESS,
         SF_3X4BYTE_FLOAT,
         SF_3X4BYTE_UINT,
         SF_3X4BYTE_SINT,
         SF_2X4BYTE_TYPELESS,
         SF_2X4BYTE_FLOAT,
         SF_2X4BYTE_UINT,
         SF_2X4BYTE_SINT,
         SF_1X4BYTE_TYPELESS,
         SF_1X4BYTE_FLOAT,
         SF_1X4BYTE_UINT,
         SF_1X4BYTE_SINT,
         SF_4X2BYTE_TYPELESS,
         SF_4X2BYTE_FLOAT,
         SF_4X2BYTE_UINT,
         SF_4X2BYTE_SINT,
         //No 3x2 support
         SF_2X2BYTE_TYPELESS,
         SF_2X2BYTE_FLOAT,
         SF_2X2BYTE_UINT,
         SF_2X2BYTE_SINT,
         SF_1X2BYTE_TYPELESS,
         SF_1X2BYTE_FLOAT,
         SF_1X2BYTE_UINT,
         SF_1X2BYTE_SINT,
         SF_4X1BYTE_TYPELESS,
         //No 4x1 floats
         SF_4X1BYTE_UINT,
         SF_4X1BYTE_SINT,
         //No 3x1 support
         SF_2X1BYTE_TYPELESS,
         //No 4x1 floats
         SF_2X1BYTE_UINT,
         SF_2X1BYTE_SINT,
         SF_1X1BYTE_TYPELESS,
         //No 1x1 floats
         SF_1X1BYTE_UINT,
         SF_1X1BYTE_SINT,
         SF_DEPTH_STENCIL,
         SF_DEPTH,
         SF_STENCIL
         };

      struct TextureFilter
         {
         bool linearMin, linearMag, linearMip;
         bool anisotropic, comparison;

         TextureFilter() : linearMin(false), linearMag(false), linearMip(false),
            anisotropic(false), comparison(false) {}
         };

      //Texture Sampler
      enum TextureAddressMode
         {
         TA_WRAP,
         TA_MIRROR,
         TA_CLAMP,
         TA_BORDER,
         TA_MIRROR_ONCE
         };

      enum TextureComparisonMethod
         {
         TC_NEVER,
         TC_LESS,
         TC_EQUAL,
         TC_LESS_EQUAL,
         TC_GREATER,
         TC_NOT_EQUAL,
         TC_GREATER_EQUAL,
         TC_ALWAYS
         };

      //Vertex

      struct InputElement
         {
         LString sem;
         uint32 semCount;
         StructureFormat format;
         uint32 input;
         uint32 aligned;
         uint32 bsize;
         };
      }
   }
#endif