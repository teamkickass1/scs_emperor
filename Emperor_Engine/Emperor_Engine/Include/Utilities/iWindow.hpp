#ifndef __EMP_UTL_I_WINDOW_HPP__
#define __EMP_UTL_I_WINDOW_HPP__

//Basic Utility Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Utility
      {
      /** Window Interface

      The window interface encapsulates a operating system window. This class
      will communicate with the OS to create, destroy and manipulate windows.
      */
      class iWindow
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iWindow() {}
            virtual ~iWindow() {}
         public:
            //Initializes the window with the operating system
            virtual void init() = 0;

            //Cleanly destroys the window, removing references from 
            //managers and deallocating resources.
            virtual void destroy() = 0;

            //Sets the name of the window that will appear in the title bar
            virtual void setName(const LString& name) = 0;

            //Returns the name of the window
            virtual LString getName() = 0;

            //Sets the class for the window. The class determines various 
            //attributes of the window, such as its style, format, and the 
            //callback for the window messages
            virtual void setClass(const WindowClass& cl) = 0;

            //Returns the class for the window
            virtual const WindowClass& getClass() = 0;

            //Sets the window's position on the screen. The parameters indicate
            //the window's position on the x and y axis of the screen
            virtual void setPosition(const Vector<int, 2>& pos) = 0;

            //Returns the position of the window on the screen
            virtual const Vector<int, 2>& getPosition() = 0;

            //Sets the window's size on the screen. The paramteres indicate
            //the window's size as height and width in pixels
            virtual void setSize(const Vector<int, 2>& sz) = 0;

            //Returns the size of the window
            virtual const Vector<int, 2>& getSize() = 0;

            //Returns the window ID/handle, can be cleanly cast into a
            //HWND variable for use with low level processes
            virtual WindowID getWindowID() const = 0;
         };
      }
   }

#endif