#ifndef __EMP_UTL_I_WINDOW_CONTROLLER_HPP__
#define __EMP_UTL_I_WINDOW_CONTROLLER_HPP__

#include "../Core/EmperorAPI.hpp"

#include "Types.hpp"

namespace Emperor
   {
   namespace Utility
      {
      class iWindow;

      /** Window Controller Interface

      This interface exposes the high level controls of the window system.
      Windows can be created and messages processed from this interface.
      */
      class iWindowController
         {
         private:
            //Global access function needed to create controller
            EMP_API friend iWindowController* getWindowController();
         protected:
            //protected to prevent from standard de/allocation
            iWindowController() {}
            ~iWindowController() {}
         public:
            //Creates a new window. Will need to be configured and initialized
            //before use.
            virtual iWindow* createWindow() = 0;

            //Retreives all of the window messages since it was last checked
            virtual LArray<MessagePackage> retrieveWindowMessages() = 0;
         };

      EMP_API iWindowController* getWindowController();
      }
   }

#endif