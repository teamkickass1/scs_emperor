#ifndef __EMP_INP_TYPES_HPP__
#define __EMP_INP_TYPES_HPP__

#include "../Core/Types.hpp"

namespace Emperor
   {
   namespace Input
      {
      enum InputState
         {
         IS_NONE,
         IS_UP,
         IS_DOWN
         };

      enum InputID
         {
         II_KEY_UNKNOWN,
         II_KEY_DOT = 0xBE,
         II_KEY_COMMA = 0xBC,
         II_KEY_SEMICOLON = 0xBA,
         II_KEY_TILDE = 0xC0,
         II_KEY_HYPHON = 0xBD,
         II_KEY_EQUAL = 193,
         II_KEY_BACKSLASH = 0xDC,
         II_KEY_RIGHT_BRACE = 0xDD,
         II_KEY_LEFT_BRACE = 0xDB,
         II_KEY_QUOTE = 0xDE,
         II_KEY_SLASH = 0xBF,
         II_KEY_BACKSPACE = 0x08,
         II_KEY_TAB,
         II_KEY_ENTER = 0x0D,
         II_KEY_PAUSE = 0x13,
         II_KEY_CAPS_LOCK,
         II_KEY_ESC = 0x1B,
         II_KEY_SPACE = 0x20,
         II_KEY_PG_UP,
         II_KEY_PG_DOWN,
         II_KEY_END,
         II_KEY_HOME,
         II_KEY_LEFT,
         II_KEY_UP,
         II_KEY_RIGHT,
         II_KEY_DOWN,
         II_KEY_PRT_SC = 0x2C,
         II_KEY_INS,
         II_KEY_DEL,
         II_KEY_0 = 0x30,
         II_KEY_1,
         II_KEY_2,
         II_KEY_3,
         II_KEY_4,
         II_KEY_5,
         II_KEY_6,
         II_KEY_7,
         II_KEY_8,
         II_KEY_9,
         II_KEY_A = 0x41,
         II_KEY_B,
         II_KEY_C,
         II_KEY_D,
         II_KEY_E,
         II_KEY_F,
         II_KEY_G,
         II_KEY_H,
         II_KEY_I,
         II_KEY_J,
         II_KEY_K,
         II_KEY_L,
         II_KEY_M,
         II_KEY_N,
         II_KEY_O,
         II_KEY_P,
         II_KEY_Q,
         II_KEY_R,
         II_KEY_S,
         II_KEY_T,
         II_KEY_U,
         II_KEY_V,
         II_KEY_W,
         II_KEY_X,
         II_KEY_Y,
         II_KEY_Z,
         II_KEY_LEFT_WINDOW = 0x5B,
         II_KEY_RIGHT_WINDOW,
         II_KEY_NP_0 = 0x60,
         II_KEY_NP_1,
         II_KEY_NP_2,
         II_KEY_NP_3,
         II_KEY_NP_4,
         II_KEY_NP_5,
         II_KEY_NP_6,
         II_KEY_NP_7,
         II_KEY_NP_8,
         II_KEY_NP_9,
         II_KEY_NP_MULTIPLY,
         II_KEY_NP_ADD,
         II_KEY_NP_ENTER,
         II_KEY_NP_SUBTRACT,
         II_KEY_NP_DECIMAL,
         II_KEY_NP_DIVIDE,
         II_KEY_F1,
         II_KEY_F2,
         II_KEY_F3,
         II_KEY_F4,
         II_KEY_F5,
         II_KEY_F6,
         II_KEY_F7,
         II_KEY_F8,
         II_KEY_F9,
         II_KEY_F10,
         II_KEY_F11,
         II_KEY_F12,
         II_KEY_NUMLK = 0x90,
         II_KEY_SCR_LK,
         II_KEY_LEFT_SHIFT = 0xA0,
         II_KEY_RIGHT_SHIFT,
         II_KEY_LEFT_CTRL,
         II_KEY_RIGHT_CTRL,
         II_KEY_LEFT_ALT,
         II_KEY_RIGHT_ALT,
         II_MOUSE_LEFT = 0X100,
         II_MOUSE_MIDDLE,
         II_MOUSE_RIGHT,
         II_MOUSE_BUTTON1,
         II_MOUSE_BUTTON2,
         II_MOUSE_WHEEL,
         II_MOUSE_MOVE,
         II_CONTROLLER_E1,
         II_CONTROLLER_E2,
         II_CONTROLLER_E3,
         II_CONTROLLER_E4,
         II_CONTROLLER_E5,
         II_CONTROLLER_E6,
         II_CONTROLLER_E7,
         II_CONTROLLER_E8,
         II_CONTROLLER_E9,
         II_CONTROLLER_E10,
         II_CONTROLLER_E11,
         II_CONTROLLER_E12,
         II_CONTROLLER_E13,
         II_CONTROLLER_E14,
         II_CONTROLLER_E15,
         II_CONTROLLER_E16,
         II_CONTROLLER_E17,
         II_CONTROLLER_E18
         };

      //struct MouseData
      //   {
      //   uint16 flags;
      //   uint16 eventFired;
      //   uint16 wheel;
      //   uint32 buttonState;
      //   int x;
      //   int y;
      //   uint32 extraData;
      //   };

      struct MouseData
         {
         int x;
         int y;
         int z;
         byte buttons[4];
         };

      struct KeyboardData
         {
         };
      }
   }

#endif