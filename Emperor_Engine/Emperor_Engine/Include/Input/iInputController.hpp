#ifndef __EMP_INP_I_INPUT_CONTROLLER_HPP__
#define __EMP_INP_I_INPUT_CONTROLLER_HPP__

#include "../Core/EmperorAPI.hpp"

namespace Emperor
   {
   namespace Utility
      {
      class iWindow;
      }

   namespace Input
      {
      class iInput;

      /** Input Controller Interface

      This interface exposes the high level controls of the input system.
      Binding to a window is required before other functionality becomes 
      available.
      */
      class iInputController
         {
         private:
            //Global access function needed to create controller
            EMP_API friend iInputController* getController();
         protected:
            //protected to prevent from standard de/allocation
            iInputController() {}
            ~iInputController() {}
         public:
            //Binds the input system to a window so that input can be received
            //from the operating system
            virtual void bindWindow(Utility::iWindow* w) = 0;
            
            //Returns the keyboard device, needed to register callbacks
            virtual iInput* getKeyboard() = 0;

            //Returns the mouse device, needed to register callbacks
            virtual iInput* getMouse() = 0;

            //Retrieves all system events and fires all registered callbacks
            virtual void fireEvents() = 0;
         };
      EMP_API iInputController* getController();
      }
   }

#endif