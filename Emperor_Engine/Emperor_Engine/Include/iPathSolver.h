#pragma once

#ifndef __EMP_I_PATHSOLVER_H__
#define __EMP_I_PATHSOLVER_H__

namespace Emperor
{
	class iPathnode;
	class iPathSolver
	{
		protected:
			iPathSolver() { }
			virtual ~iPathSolver() { }
		public:
			void destroy();
			virtual void setDestination(iPathnode* dest) = 0;
	};
}

#endif