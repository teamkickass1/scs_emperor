//to be included by Math.hpp
namespace Emperor
   {
   inline Vector<int,2> Coord(int X, int Y)
      {
      Vector<int,2> out;
      out.x() = X;
      out.y() = Y;
      return out;
      }

   inline Vector<float, 2> Vector2(float x, float y)
      {
      Vector<float,2> out;
      out.x() = x;
      out.y() = y;
      return out;
      }

   inline Vector<float, 3> Vector3(float X, float Y, float Z)
      {
      Vector<float, 3> out;
      out.x() = X;
      out.y() = Y;
      out.z() = Z;
      return out;
      }

   inline Vector<float, 4> Vector4(float X, float Y, float Z, float W)
      {
      Vector<float, 4> out;
      out.x() = X;
      out.y() = Y;
      out.z() = Z;
      out.w() = W;
      return out;
      }

   inline Vector<float, 4> Vector4(const Vector<float, 3>& a, float b)
      {
      Vector<float,4> out;
      out.x() = a.x();
      out.y() = a.y();
      out.z() = a.z();
      out.w() = b;
      return out;
      }

   inline Matrix<float, 4> Matrix4()
      {
      return Matrix<float,4>();
      }

   inline Matrix<float, 4> Matrix4(float a)
      {
      int i = 16;
      Matrix<float,4> out;
      while(i--)
         out.data[i] = a;
      return out;
      }

   inline Matrix<float, 4> Matrix4(Vector<float, 4> a, Vector<float, 4> b, Vector<float, 4> c, Vector<float, 4> d)
      {
      Matrix<float,4> out;
      out[0] = a;
      out[1] = b;
      out[2] = c;
      out[3] = d;
      return out;
      }

   inline Matrix<float, 3> Matrix3()
      {
      return Matrix<float,3>();
      }

   inline Matrix<float, 3> Matrix3(float a)
      {
      int i = 9;
      Matrix<float,3> out;
      while(i--)
         out.data[i] = a;
      return out;
      }

   inline Matrix<float, 3> Matrix3(Vector<float, 3> a, Vector<float, 3> b, Vector<float, 3> c)
      {
      Matrix<float,3> out;
      out[0] = a;
      out[1] = b;
      out[2] = c;
      return out;
      }

   inline float determinant(const Matrix<float, 4>& m)
      {
      float value;
      value =
         m(0,3)*m(1,2)*m(2,1)*m(3,0) - m(0,2)*m(1,3)*m(2,1)*m(3,0) - m(0,3)*m(1,1)*m(2,2)*m(3,0) + m(0,1)*m(1,3)*m(2,2)*m(3,0)+
         m(0,2)*m(1,1)*m(2,3)*m(3,0) - m(0,1)*m(1,2)*m(2,3)*m(3,0) - m(0,3)*m(1,2)*m(2,0)*m(3,1) + m(0,2)*m(1,3)*m(2,0)*m(3,1)+
         m(0,3)*m(1,0)*m(2,2)*m(3,1) - m(0,0)*m(1,3)*m(2,2)*m(3,1) - m(0,2)*m(1,0)*m(2,3)*m(3,1) + m(0,0)*m(1,2)*m(2,3)*m(3,1)+
         m(0,3)*m(1,1)*m(2,0)*m(3,2) - m(0,1)*m(1,3)*m(2,0)*m(3,2) - m(0,3)*m(1,0)*m(2,1)*m(3,2) + m(0,0)*m(1,3)*m(2,1)*m(3,2)+
         m(0,1)*m(1,0)*m(2,3)*m(3,2) - m(0,0)*m(1,1)*m(2,3)*m(3,2) - m(0,2)*m(1,1)*m(2,0)*m(3,3) + m(0,1)*m(1,2)*m(2,0)*m(3,3)+
         m(0,2)*m(1,0)*m(2,1)*m(3,3) - m(0,0)*m(1,2)*m(2,1)*m(3,3) - m(0,1)*m(1,0)*m(2,2)*m(3,3) + m(0,0)*m(1,1)*m(2,2)*m(3,3);
      return value;
      }

   inline Matrix<float, 4> inverse(const Matrix<float, 4> m)
      {
      Matrix<float,4> out;
      out.data[0] = m(1,2)*m(2,3)*m(3,1) - m(1,3)*m(2,2)*m(3,1) + m(1,3)*m(2,1)*m(3,2) - m(1,1)*m(2,3)*m(3,2) - m(1,2)*m(2,1)*m(3,3) + m(1,1)*m(2,2)*m(3,3);
      out.data[1] = m(0,3)*m(2,2)*m(3,1) - m(0,2)*m(2,3)*m(3,1) - m(0,3)*m(2,1)*m(3,2) + m(0,1)*m(2,3)*m(3,2) + m(0,2)*m(2,1)*m(3,3) - m(0,1)*m(2,2)*m(3,3);
      out.data[2] = m(0,2)*m(1,3)*m(3,1) - m(0,3)*m(1,2)*m(3,1) + m(0,3)*m(1,1)*m(3,2) - m(0,1)*m(1,3)*m(3,2) - m(0,2)*m(1,1)*m(3,3) + m(0,1)*m(1,2)*m(3,3);
      out.data[3] = m(0,3)*m(1,2)*m(2,1) - m(0,2)*m(1,3)*m(2,1) - m(0,3)*m(1,1)*m(2,2) + m(0,1)*m(1,3)*m(2,2) + m(0,2)*m(1,1)*m(2,3) - m(0,1)*m(1,2)*m(2,3);
      out.data[4] = m(1,3)*m(2,2)*m(3,0) - m(1,2)*m(2,3)*m(3,0) - m(1,3)*m(2,0)*m(3,2) + m(1,0)*m(2,3)*m(3,2) + m(1,2)*m(2,0)*m(3,3) - m(1,0)*m(2,2)*m(3,3);
      out.data[5] = m(0,2)*m(2,3)*m(3,0) - m(0,3)*m(2,2)*m(3,0) + m(0,3)*m(2,0)*m(3,2) - m(0,0)*m(2,3)*m(3,2) - m(0,2)*m(2,0)*m(3,3) + m(0,0)*m(2,2)*m(3,3);
      out.data[6] = m(0,3)*m(1,2)*m(3,0) - m(0,2)*m(1,3)*m(3,0) - m(0,3)*m(1,0)*m(3,2) + m(0,0)*m(1,3)*m(3,2) + m(0,2)*m(1,0)*m(3,3) - m(0,0)*m(1,2)*m(3,3);
      out.data[7] = m(0,2)*m(1,3)*m(2,0) - m(0,3)*m(1,2)*m(2,0) + m(0,3)*m(1,0)*m(2,2) - m(0,0)*m(1,3)*m(2,2) - m(0,2)*m(1,0)*m(2,3) + m(0,0)*m(1,2)*m(2,3);
      out.data[8] = m(1,1)*m(2,3)*m(3,0) - m(1,3)*m(2,1)*m(3,0) + m(1,3)*m(2,0)*m(3,1) - m(1,0)*m(2,3)*m(3,1) - m(1,1)*m(2,0)*m(3,3) + m(1,0)*m(2,1)*m(3,3);
      out.data[9] = m(0,3)*m(2,1)*m(3,0) - m(0,1)*m(2,3)*m(3,0) - m(0,3)*m(2,0)*m(3,1) + m(0,0)*m(2,3)*m(3,1) + m(0,1)*m(2,0)*m(3,3) - m(0,0)*m(2,1)*m(3,3);
      out.data[10] = m(0,1)*m(1,3)*m(3,0) - m(0,3)*m(1,1)*m(3,0) + m(0,3)*m(1,0)*m(3,1) - m(0,0)*m(1,3)*m(3,1) - m(0,1)*m(1,0)*m(3,3) + m(0,0)*m(1,1)*m(3,3);
      out.data[11] = m(0,3)*m(1,1)*m(2,0) - m(0,1)*m(1,3)*m(2,0) - m(0,3)*m(1,0)*m(2,1) + m(0,0)*m(1,3)*m(2,1) + m(0,1)*m(1,0)*m(2,3) - m(0,0)*m(1,1)*m(2,3);
      out.data[12] = m(1,2)*m(2,1)*m(3,0) - m(1,1)*m(2,2)*m(3,0) - m(1,2)*m(2,0)*m(3,1) + m(1,0)*m(2,2)*m(3,1) + m(1,1)*m(2,0)*m(3,2) - m(1,0)*m(2,1)*m(3,2);
      out.data[13] = m(0,1)*m(2,2)*m(3,0) - m(0,2)*m(2,1)*m(3,0) + m(0,2)*m(2,0)*m(3,1) - m(0,0)*m(2,2)*m(3,1) - m(0,1)*m(2,0)*m(3,2) + m(0,0)*m(2,1)*m(3,2);
      out.data[14] = m(0,2)*m(1,1)*m(3,0) - m(0,1)*m(1,2)*m(3,0) - m(0,2)*m(1,0)*m(3,1) + m(0,0)*m(1,2)*m(3,1) + m(0,1)*m(1,0)*m(3,2) - m(0,0)*m(1,1)*m(3,2);
      out.data[15] = m(0,1)*m(1,2)*m(2,0) - m(0,2)*m(1,1)*m(2,0) + m(0,2)*m(1,0)*m(2,1) - m(0,0)*m(1,2)*m(2,1) - m(0,1)*m(1,0)*m(2,2) + m(0,0)*m(1,1)*m(2,2);
      out = out / determinant(m);
      return out;
      }

   inline Matrix<float, 4> rotationMatrix(const Vector<float, 3>& axis, float radians)
      {
      float c = cosf(radians), s = sinf(radians), t = 1.0f - c;

      auto a = axis.normalizedCopy();
      Matrix<float,4> n;
      n(0,0) = t*a.x()*a.x() + c;       n(0,1) = t*a.x()*a.y() + a.z()*s; n(0,2) = t*a.x()*a.z() - a.y()*s; n(0,3) = 0;
      n(1,0) = t*a.x()*a.y() - a.z()*s; n(1,1) = t*a.y()*a.y() + c;       n(1,2) = t*a.y()*a.z() + a.x()*s; n(1,3) = 0;
      n(2,0) = t*a.x()*a.z() + a.y()*s; n(2,1) = t*a.y()*a.z() - a.x()*s; n(2,2) = t*a.z()*a.z() + c;       n(2,3) = 0;
      n(3,0) = 0;                       n(3,1) = 0;                       n(3,2) = 0;                       n(3,3) = 1;
      return n;
      }

   inline Matrix<float, 4> scaleMatrix(const Vector<float, 3>& scale)
      {
      //Fill Me
      Matrix<float,4> m;
	  m.identity();
	  for (int i = 0; i < 3; i++)
		  m.data[i * 5] = scale.data[i];
      return m;
      }

   inline Matrix<float, 4> translationMatrix(const Vector<float, 3>& translation)
      {
      //Fill Me
      Matrix<float,4> m;
	  m.identity();
	  for (int i = 0; i < 3; i++)
		  m.data[i + 12] = translation.data[i];
      return m;
      }

   inline Matrix<float, 4>& rotate(Matrix<float, 4>& dest, const Vector<float, 3>& axis, float radians)
      {
      //Extract the position out of the (dest) matrix and set to zero
      //Pre-Multiply the (dest) matrix with a rotation matrix based on (axis) and
      //(radians), set the position back in and return the matrix.
	   Vector<float, 3> pos;
	   for (int i = 0; i < 3; i++){
		   pos[i] = dest.data[i + 12];
		   dest.data[i + 12] = 0;
	   }
	   dest = dest * rotationMatrix(axis,radians);
	   for (int i = 0; i < 3; i++){
		   dest.data[i + 12] = pos[i];
	   }
      return dest;
      }

   inline Matrix<float, 4>& translate(Matrix<float, 4>& dest, const Vector<float, 3>& pos)
      {
      //Fill Me
	   for (int i = 0; i < 3; i++)
		dest.data[i + 12] += pos.data[i];
      return dest;
      }

   inline Matrix<float, 4>& scale(Matrix<float, 4>& dest, const Vector<float, 3>& scale)
      {
      //Extract the position out of the (dest) matrix and set to zero
      //Pre-Multiply the (dest) matrix with a scale matrix based on (scale)
      //set the position back in and return the matrix.
	   Vector<float, 3> pos;
	   Matrix<float, 4> scl = scaleMatrix(scale);
	   for (int i = 0; i < 3; i++){
		   pos[i] = dest.data[i + 12];
		   dest.data[i + 12] = 0;
	   }
	   dest = dest * scl;
	   for (int i = 0; i < 3; i++){
		   dest.data[i + 12] = pos[i];
	   }
      return dest;
      }

   inline Matrix<float, 4>& scale(const Vector<float, 3>& scale, Matrix<float, 4>& dest)
      {
      //Extract the position out of the (dest) matrix and set to zero
      //Post-Multiply the (dest) matrix with a scale matrix based on (scale)
      //set the position back in and return the matrix.
	   Vector<float, 3> pos;
	   Matrix<float, 4> scl = scaleMatrix(scale);
	   for (int i = 0; i < 3; i++){
		   pos[i] = dest.data[i + 12];
		   dest.data[i + 12] = 0;
	   }
	   dest =  scl * dest;
	   for (int i = 0; i < 3; i++){
		   dest.data[i + 12] = pos[i];
	   }
      return dest;
      }

   inline Matrix<float, 4>& setPosition(Matrix<float, 4>& dest, const Vector<float, 3>& pos)
      {
      //Fill Me
	   for (int i = 0; i < 3; i++){
		   dest.data[i + 12] = pos[i];
	   }
      return dest;
      }

   inline Vector<float, 3> getPosition(const Matrix<float, 4>& src)
      {
      return Vector<float, 3>(src.data + 12);
      }

   inline Vector<float, 3> crossProduct(const Vector<float, 3>& a, const Vector<float, 3>& b)
      {
      return Vector3(a.y()*b.z() - a.z()*b.y(),a.z()*b.x() - a.x()*b.z(),a.x()*b.y() - a.y()*b.x());
      }
   }
