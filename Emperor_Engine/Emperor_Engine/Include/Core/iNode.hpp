#ifndef __EMP_I_NODE_HPP__
#define __EMP_I_NODE_HPP__

//Math Library
#include "Math.hpp"

namespace Emperor
   {
   /** Node Interface

   A node represents a single set of spatial information inside the scene. The
   node is the most basic building block in the scene and is used to represent
   any kind of spatial presence in the scene. For a scene object to exist in a 
   scene, it must be attached to a node. Nodes can also be attached to other 
   nodes to make complex node trees, where the child node's spatial information
   is dependant on it's parent's spatial information.
   */
   class iNode
      {
      private:
      protected:
         //protected to prevent from standard de/allocation
         iNode() {}
         virtual ~iNode() {}
      public:
         //Cleanly destroys the node, removing references from managers and
         //deallocating resources.
         virtual void destroy() = 0;

         //Sets the node's 3D position in the scene. The parameters correlate
         //with the 3 spatial axis
         virtual void setPosition(float x, float y, float z) = 0;
         virtual void setPosition(const Vector<float,3>&) = 0;

         //Returns the node's 3D postion in the scene
         virtual Vector<float,3> getPosition() const = 0;

         //Alters the node's current 3D position in the scene. The parameters
         //correlate with the amount of change on each satial axis
         virtual void translate(float x, float y, float z) = 0;
         virtual void translate(const Vector<float,3>&) = 0;

         //Sets the node's 3D scale in the scene. The parameters correlate
         //with the 3 spatial axis
         virtual void setScale(float x, float y, float z) = 0;
         virtual void setScale(const Vector<float, 3>&) = 0;

         //Returns the node's 3D scale in the scene
         virtual Vector<float, 3> getScale() const = 0;

         //Alters the node's current scale in the scene. The parameters
         //indicate the factors the node and children will scale by on each
         //spatial axis
         virtual void scale(float x, float y, float z) = 0;
         virtual void scale(const Vector<float, 3>&) = 0;


         //Sets the node's 3D rotation in the scene. The parameters correlate
         //with the 3x3 rotation matrix
         virtual void setRotation(const Matrix<float, 3>&) = 0;

         //Returns the node's 3D rotation in the scene
         virtual Matrix<float, 3> getRotation() const = 0;

         //Alters the node's current rotation in the scene. The parameters
         //indicate the axis in which to rotate upon (x, y, z) and the angle
         //of which the node will be rotated
         virtual void rotate(float x, float y, float z, float angle) = 0;
         virtual void rotate(const Vector<float,3>& axis, float angle) = 0;

         //Returns the node's local tranformation matrix which represents the
         //node's position, rotation and scale
         virtual const Matrix<float,4>& getLocalTransform() const = 0;

         //Attaches this node to another node as a child. The node will become
         //a dependant of it's parent node and the parent node's absolute 
         //transformation will affect the absolute tranformation of the child
         //node. If null is provided, the node will detach from the parent and
         //become parentless
         virtual void attachTo(iNode* node) = 0;

         //Returns the last calculated absolute tranformation of the node. This
         //result will be one frame old if the _recalcAbsTransform is not
         //called first
         virtual const Matrix<float,4>& _getAbsTransform() const = 0;

         //Recalculates the absolute position of the node on the spot. A slow
         //operation, try to avoid
         virtual void _recalcAbsTransform() = 0;
      };

   }

#endif