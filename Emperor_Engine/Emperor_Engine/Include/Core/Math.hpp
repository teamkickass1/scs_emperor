#ifndef __EMP_MATH_HPP__
#define __EMP_MATH_HPP__

#include <cstring> //need for memcpy
#include <iostream>
#include <type_traits>
#include <vector>
#include <math.h>

namespace Emperor
{
	template <class T, int N>
	struct Vector;

	template <class T, int N>
	inline static T _detachedDot(const T* a, const T* b)
	{
		//Performs a dot operation on (a) and (b), returning the result
		T result = 0;
		for (int i = 0; i < N; i++){
			result += a[i] * b[i];
		}
		return result;
	}

	// Row vector class, multiplies left of a matrix
	template <class T = float, int N = 3>
	struct Vector
	{
		T data[N];

		Vector() {}

		Vector(const Vector<T, N>& a)
		{
			*this = a;
		}

		Vector(const T d[N])
		{
			//Copy all values from array (d) into (data)
			for (int i = 0; i < N; i++){
				data[i] = d[i];
			}
		}

		Vector(const T& d)
		{
			//Copy (d) into all elements of (data)
			for (int i = 0; i < N; i++){
				data[i] = d;
			}
		}

		// operator= is default provided by the compiler

		inline T& x() { return data[0]; }
		inline const T& x() const { return data[0]; }

		inline T& y() { return data[1]; }
		inline const T& y() const { return data[1]; }

		inline T& z() { return data[2]; }
		inline const T& z() const { return data[2]; }

		inline T& w() { return data[3]; }
		inline const T& w() const { return data[3]; }

		inline void write_x(T i) { data[0] = i; }
		inline void write_y(T i) { data[1] = i; }
		inline void write_z(T i) { data[2] = i; }
		inline void write_w(T i) { data[3] = i; }

		inline T read_x() const { return data[0]; }
		inline T read_y() const { return data[1]; }
		inline T read_z() const { return data[2]; }
		inline T read_w() const { return data[3]; }

		inline Vector<T, 2>& xy() { return *((Vector<T, 2>*)data); }
		inline const Vector<T, 2>& xy() const { return *((Vector<T, 2>*)data); }

		inline Vector<T, 3>& xyz() { return *((Vector<T, 3>*)data); }
		inline const Vector<T, 3>& xyz() const { return *((Vector<T, 3>*)data); }


		inline T& operator[](int idx)
		{
			return data[idx];
		}

		inline const T& operator[](int idx) const
		{
			return data[idx];
		}

		inline Vector<T, N> operator-(const Vector<T, N>& a) const
		{
			//Returns a vector that is equal to the current vector minus (a)
			Vector<T, N> temp;
			for (int i = 0; i < N; i++){
				temp.data[i] = data[i] - a.data[i];
			}
			return temp;
		}

		inline Vector<T, N> operator*(float a) const
		{
			//Returns a vector that is equal to the current vector times (a)
			Vector<T, N> temp;
			for (int i = 0; i < N; i++){
				temp.data[i] = data[i] * a;
			}
			return temp;
		}

		inline bool operator==(const Vector& n) const
		{
			for (int i = 0; i < N; i++)
			{
				if (data[i] != n.data[i])
					return false;
			}
			return true;
		}

		inline bool operator!=(const Vector& n) const
		{
			return !(*this == n);
		}

		inline float dot(const Vector<T, N>& a) const
		{
			return (float)_detachedDot<T, N>(data, a.data);
		}

		inline float length() const
		{
			//Returns the magnitude of the vector
			float total = 0, result = 0;
			for (auto i : data){
				total += pow(i, 2);
			}
			result = sqrt(total);
			return result;
		}

		inline float fLength() const
		{
			//Returns the squared magnitude of the vector
			float total = 0;
			for (auto i : data){
				total += pow(i, 2);
			}
			return total;
		}

		inline void normalize()
		{
			//Normalizes the vector, giving it a length of one
			float magnitute = length();
			for (int i = 0; i < N; i++){
				data[i] = data[i] / magnitute;
			}
		}

		inline Vector<T, N> normalizedCopy() const
		{
			//Makes a copy of the current vector and normalizes it, 
			//returning the result
			Vector<T, N> temp;
			float magnitute = length();
			for (int i = 0; i < N; i++){
				temp.data[i] = data[i] / magnitute;
			}
			return temp;
		}

		inline float radiansBetween(const Vector<T, N>& other) const
		{
			//Returns the number of radians between two vectors
			return acos(_detachedDot<T, N>(data, other.data) / (length() * other.length()));
		}

		static inline int getByteSize()
		{
			//Returns the size of the vector in bytes
			int size = sizeof(T) * N;
			return size;
		}
	};

	template <typename T, int N>
	std::ostream& operator<<(std::ostream& os, const Vector<T, N>& v)
	{
		os << '[';
		if (N > 0)
		{
			os << v.data[0];

			for (int i = 1; i < N; ++i)
			{
				os << ',' << ' ' << v.data[i];
			}
		}
		return os << ']';
	}

	Vector<int, 2> Coord(int X, int Y);

	Vector<float, 2> Vector2(float x, float y);

	Vector<float, 3> Vector3(float X, float Y, float Z);

	Vector<float, 4> Vector4(float X, float Y, float Z, float W);

	Vector<float, 4> Vector4(const Vector<float, 3>&, float);

	struct Color : public Vector<float, 4>
	{
		Color() {}

		Color(float R, float G, float B, float A)
		{
			data[0] = R;
			data[1] = G;
			data[2] = B;
			data[3] = A;
		}

		inline float& r() { return data[0]; }
		inline const float& x() const { return data[0]; }

		inline float& g() { return data[1]; }
		inline const float& g() const { return data[1]; }

		inline float& b() { return data[2]; }
		inline const float& b() const { return data[2]; }

		inline float& a() { return data[3]; }
		inline const float& a() const { return data[3]; }

		// operator= is default provided by the compiler
	};

	// Row-major matrix class, position elements in 12, 13, 14
	template <class T = float, int N = 3>
	struct Matrix : public Vector<T, N*N>
	{
		Matrix() {}

		inline Vector<T, N>& operator[](int row)
		{
			//Returns a single row of the matrix as a reference
			//You must return the address of the row in the matrix so any changes
			//done to the returned vector will be reflected by the matrix
			return *(Vector<T, N>*)(data + N * row);
		}

		inline const Vector<T, N>& operator[](int row) const
		{
			//Fill Me			
			return *(Vector<T, N>*)(data + N * row);
		}

		inline T& operator()(int row, int col)
		{
			//Returns a T reference at the specified (row) and (col) in the matrix
			return *(data + N * row + col);
		}

		inline const T& operator()(int row, int col) const
		{
			//Fill Me
			return *(data + N * row + col);
		}

		inline Matrix<T, N> operator*(float a) const
		{
			//Fill Me
			Matrix<T, N> temp;
			for (auto i = 0; i < N*N; i++){
				temp.data[i] = data[i] * a;
			}
			return temp;
		}

		inline Matrix<T, N> operator/(float a) const
		{
			//Fill Me
			Matrix<T, N> temp;
			for (auto i = 0; i < N*N; i++){
				temp.data[i] = data[i] / a;
			}
			return temp;
		}

		inline Matrix<T, N> operator*(const Matrix<T, N>& m) const
		{
			//Transforms the local matrix with (a), returning the result
			//Note: an easy way of accomplishing this is to transpose (m)
			//and perform a dot operation on the rows and columns
			Matrix<T, N> out;
			for (auto i = 0; i < N; i++){ //row
				for (auto j = 0; j < N; j++){ //column
					T l = 0;
					for (auto k = 0; k < N; k++){
						l += data[N * i + k] * m.data[N * k + j];
					}
					out.data[N * i + j] = l;
				}
			}
			return out;
		}

		inline Matrix<T, N>& identity()
		{
			//Sets the current matrix to an identity matrix
			for (auto i = 0; i < N; i++){
				for (auto j = 0; j < N; j++){
					if (i == j){
						data[N * i + j] = 1;
					}
					else{
						data[N * i + j] = 0;
					}
				}
			}
			return *this;
		}

		inline Matrix<T, N> transposeMatrix() const
		{
			//Transpose the current matrix and return the result
			Matrix<T, N> out;
			for (auto i = 0; i < N; i++){
				for (auto j = 0; j < N; j++){
					out.data[N * i + j] = data[N * j + i];
				}
			}
			return out;
		}

		inline Matrix<T, N>& transpose()
		{
			return *this = transposeMatrix();
		}
	};

	template <typename T, int N>
	inline std::ostream& operator<<(std::ostream& os, const Matrix<T, N>& m)
	{
		os << '<';
		if (N > 0)
		{
			os << (*this)[0];
			for (int i = 1; i < N; ++i)
			{
				os << ',' << std::endl << (*this)[i];
			}
		}
		return os << '>';
	}

	template <typename T, int N>
	inline Vector<T, N> operator*(const Vector<T, N>& a, const Matrix<T, N>& m)
	{
		//Transforms the vector (a) with matrix (m), returning the result
		Vector<T, N> out;
		Matrix<T, N> temp = m.transposeMatrix();
		T t = 0;
		for (auto i = 0; i < N; i++){
			t = 0;
			for (auto j = 0; j < N; j++){
				t += temp.data[N * i + j] * a.data[j];
			}
			out.data[i] = t;
		}
		return out;
	}

	Matrix<float, 4> Matrix4();

	Matrix<float, 4> Matrix4(float a);

	Matrix<float, 4> Matrix4(Vector<float, 4> a, Vector<float, 4> b, Vector<float, 4> c,
		Vector<float, 4> d);

	Matrix<float, 3> Matrix3();

	Matrix<float, 3> Matrix3(float a);

	Matrix<float, 3> Matrix3(Vector<float, 3> a, Vector<float, 3> b, Vector<float, 3> c);

	float determinant(const Matrix<float, 4>& src);
	Matrix<float, 4> inverse(const Matrix<float, 4>);

	Matrix<float, 4> rotationMatrix(const Vector<float, 3>& axis, float radians);
	Matrix<float, 4> scaleMatrix(const Vector<float, 3>& scale);
	Matrix<float, 4> translationMatrix(const Vector<float, 3>& translation);
	Matrix<float, 4>& rotate(Matrix<float, 4>& dest, const Vector<float, 3>& axis, float radians);
	Matrix<float, 4>& translate(Matrix<float, 4>& dest, const Vector<float, 3>& pos);
	Matrix<float, 4>& scale(Matrix<float, 4>& dest, const Vector<float, 3>& scale);
	Matrix<float, 4>& setPosition(Matrix<float, 4>& dest, const Vector<float, 3>& pos);
	Vector<float, 3> getPosition(const Matrix<float, 4>& src);

	Vector<float, 3> crossProduct(const Vector<float, 3>&, const Vector<float, 3>&);
}

#include "Math.inl"

#endif