#ifndef __EMP_I_SCENE_OBJECT_HPP__
#define __EMP_I_SCENE_OBJECT_HPP__

namespace Emperor
   {
   class iNode;

   /** SceneObject Interface

   The scene object class is a base class for all scene object to inherit from.
   The scene object represents any kind of object that will be in the scene,
   can attach to a node and is activately managed by a scene manager
   */
   class iSceneObject
      {
      private:
      protected:
         //protected to prevent from standard de/allocation
         iSceneObject() {}
         virtual ~iSceneObject() {}

         //Pure virtual class used to update the object
         virtual void update() = 0;
      public:
         //Attaches the object to a node in the scene. This action give the 
         //object spatial information and makes it a part of the scene. 
         //Providing null will detach the object and remove it from the scene
         virtual void attachTo(const iNode* node) = 0;

         //Returns the node the object is attached to, null if not attached
         virtual const iNode* getNode() const = 0;

         //Returns true if the object is currently active
         virtual bool isActive() = 0;

         //Activates the object, indicating to it's manager that it should be
         //processed whenever the scene is being simulated
         virtual void activate() = 0;

         //Deactivates the object, indicating to it's manager that it should 
         //not be processed during scene simulation
         virtual void deactivate() = 0;

         //Cleanly destroys the object, removing references from managers and
         //deallocating resources.
         virtual void destroy() = 0;
      };

   }

#endif