#ifndef __EMP_AUD_I_ENGINE_HPP__
#define __EMP_AUD_I_ENGINE_HPP__

//Basic Audio Types
#include "Types.hpp"

#include "../Core/EmperorAPI.hpp"

namespace Emperor
   {
   namespace Audio
      {
      class iAudioController;

      /** Engine Interface

      Serves as the entry point into the Emperor Engine's audio sub-engine.
      Primarily provides initialization and shutdown functionality for the
      audio device as well as some basic high level controls. Primary access to
      engine functionality comes from the retreival of the audio controller 
      interface.
      */
      class iEngine
         {
         private:
            //Allows global functions to access protected functions
            EMP_API friend iEngine* createEngine(AudioSystem, const LString&);
            EMP_API friend void releaseEngine(iEngine*);
         protected:
            //protected to prevent from standard deallocation
            iEngine() {}
            virtual ~iEngine() {}
         public:
            //Initializes the engine's controllers
            virtual void initialize() = 0;
            //Releases any initialized devices or resources
            virtual void release() = 0;

            //Returns the resource path of the system
            virtual LString getResourceFolder() const = 0;

            //Updates the device level data with the most recent node and 
            //system data
            virtual void updateSystem() = 0;

            //Returns the audio controller interface for the engine, use to
            //manage the simulation scene
            virtual iAudioController* getAudioController() = 0;
         };

      //Creates an instance of the audio sub-engine. Requires the audio 
      //system to be used (AL11 or DX11) and the path to the resources
      EMP_API iEngine* createEngine(AudioSystem system, const LString& resoruceFolder);

      //Destroys the audio sub-engine
      EMP_API void releaseEngine(iEngine*);
      }

   }

#endif