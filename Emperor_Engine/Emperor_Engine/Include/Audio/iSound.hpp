#ifndef __EMP_AUD_I_SOUND_HPP__
#define __EMP_AUD_I_SOUND_HPP__

//Basic Audio Types
#include "Types.hpp"

namespace Emperor
   {
   namespace Audio
      {
      /** Sound Interface

      The sound class encapsulates a single sound resource. Sounds resources
      need to be filled before use, either by the sound manager or by the 
      client programmer. For sounds to be played they must be bound to an 
      emitter.
      */
      class iSound
         {
         private:
         protected:
            //protected to prevent from standard de/allocation
            iSound() {}
            virtual ~iSound() {}
         public:
            //Fills the sound's data component. Parameters dictate the address
            //from which to copy the data from and the number of bytes to 
            //copy
            virtual void fill(const byte* data,uint32 size) = 0;

            //Cleanly destroys the sound, removing references from 
            //managers and deallocating resources.
            virtual void destroy() = 0;
         };
      }

   }

#endif