#ifndef __EMP_SCENE_OBJECT_HPP__
#define __EMP_SCENE_OBJECT_HPP__

#include <Core/iSceneObject.hpp>
#include "Node.hpp"

namespace Emperor
   {
   template <class I = iSceneObject>
   class SceneObject : public I
      {
      private:
      protected:
         bool active;
         Node<>* node;

         SceneObject() : active(false), node(0) {}
         virtual ~SceneObject()
            {
            //Fill Me
            //Note: action may require casting
				if (node != nullptr){
					node->_notifyDetach((SceneObject<iSceneObject>*)this);
				}
            }

      public:
         void attachTo(const iNode* n)
            {
            //If object is already attached, notify the node of the detachment
            //set the node to (n)
            //if detaching, deactivate the object
            //if attaching, notify the attachment to the node
				
				if (node != nullptr){
					node->_notifyDetach((SceneObject<>*)this);

				}

				node = (Node<>*)n;

				if (n == nullptr){
					deactivate();
				}
				else {
					//node->attachTo((Node*)this);
					node->_notifyAttach((SceneObject<>*)this);
				}
            //Leave this here
            if(active)
               update();
            }
         const iNode* getNode() const {return node;}

         bool isActive() {return active;}
      };
   }

#endif