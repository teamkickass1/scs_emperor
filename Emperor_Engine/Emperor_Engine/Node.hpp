#ifndef __EMP_NODE_HPP__
#define __EMP_NODE_HPP__

#include "Internals.hpp"
#include <Core/iNode.hpp>

namespace Emperor
   {
   class iSceneObject;

   template <class I>
   class SceneObject;
   template <class T = iNode>
   class Node : public T
      {
      private:
         Matrix<float,4> localT;
         Matrix<float,4> _tempAbs;
         Vector<float, 3> localScale;

         uint32 nIndex;

         Node<T>* parent;
         ArrayList<Node*> children;
         ArrayList<SceneObject<iSceneObject>*> objects;
      protected:
         ~Node() {}
      public:
         Node(uint32 i) : parent(0), _tempAbs(Matrix4(0)), nIndex(i),
            localScale(1)
            {
            //Fill Me
				localT.identity();
            }

         void destroy();

         void setPosition(float x, float y, float z)
            { setPosition(Vector3(x, y, z)); }
         void setPosition(const Vector<float,3>&);
         Vector<float,3> getPosition() const;

         void translate(float x, float y, float z)
            {
            translate(Vector3(x, y, z));
            }
         void translate(const Vector<float,3>&);

         void setScale(float x, float y, float z)
            { setScale(Vector3(x, y, z)); }
         void setScale(const Vector <float, 3>&);

         Vector<float, 3> getScale() const
            { return localScale; }

         void scale(float x, float y, float z)
            {
            scale(Vector3(x, y, z));
            }
         void scale(const Vector<float,3>&);

         void setRotation(const Matrix<float, 3>&);
         Matrix<float, 3> getRotation() const;

         void rotate(float x, float y, float z, float a)
            {
            rotate(Vector3(x, y, z), a);
            }
         void rotate(const Vector<float,3>& axis, float angle);

         const Matrix<float,4>& getLocalTransform() const {return localT;}

         void attachTo(iNode* p);

         void _addChild(Node* a) 
            {
            //Fill Me
				children.push_back(a);
            }
         void _removeChild(Node<T>*);

         uint32 getIndex() {return nIndex;}

         inline Matrix<float,4> _getAbs() const {return _tempAbs;}
         inline void _setAbs(const Matrix<float,4>& a) {_tempAbs = a;}
         void _updateAbs(const Matrix<float,4>&);

         const Matrix<float,4>& _getAbsTransform() const;

         void _recalcAbsTransform();

         void _notifyAttach(SceneObject<iSceneObject>* a) 
            { 
            //Fill Me
				objects.push_back(a);
            }
         void _notifyDetach(SceneObject<iSceneObject>*);
      };
   }

#endif
