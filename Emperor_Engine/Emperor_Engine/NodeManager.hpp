#ifndef __EMP_NODE_MANAGER_HPP__
#define __EMP_NODE_MANAGER_HPP__

#include "Internals.hpp"
#include "BaseManager.hpp"
#include "Node.hpp"
namespace Emperor
   {
   class NodeManager : public Singleton<NodeManager>, public BaseManager<Node<>>
      {
      private:
         Roster<Matrix<float, 4>, MAX_NODES> nodeMatrices;
      protected:
      public:
         NodeManager() {}
         virtual ~NodeManager() {}

         Node<>* createObject();
         void removeObject(Node<>*);


         void updateValues();

         Roster<Matrix<float, 4>, MAX_NODES>& _getRoster()
            { return nodeMatrices; }
         ArrayList<Node<>*>& _getObjects()
            { return objects; }
      };
   }
#endif