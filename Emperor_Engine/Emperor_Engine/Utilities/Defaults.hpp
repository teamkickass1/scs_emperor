#ifndef __EMP_UTL_DEFAULTS_HPP__
#define __EMP_UTL_DEFAULTS_HPP__

namespace Emperor
   {

   class Defaults
      {
      public:
         static const char vertexFormatName[];
         static const char vertexFormatPNT[];
         static const char shaderDefGL[];
         static const char shaderDefDX[];
         static const char vertexFunc[];
         static const char geometryFunc[];
         static const char fragmentFunc[];
         static const char mapName[];
         static const float textureDef[16];
         static const char materialDef[];
         static const int modelSize;
         static const char modelDef[][52];
         static const char VShaderInjectDX[];
         static const char GShaderInjectDX[];
         static const char FShaderInjectDX[];
         static const char VShaderInjectGL[];
         static const char GShaderInjectGL[];
         static const char FShaderInjectGL[];
      };

   }

#endif