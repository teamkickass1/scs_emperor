#if EMP_USE_WINDOWS

#include "../Windows.hpp"
#include "Window.hpp"
#include "WindowManager.hpp"
#include "../Graphics/Managers.hpp"

namespace Emperor
   {
   namespace Utility
      {
      void Window::init()
         {
         //Retrieve the application handle and give the window class a default
         //name if it does not already have a name set, then set the class
         //Adjust the window dimensions to match the size attribute with a 
         //position of 0,0, use a RECT struct to accomplish this
         //Create the window (casting the windowStyle into a DWORD*) and store
         //the handle in the window handle attribute
         //Retreive the window placement and reveal the window

			 appInst = GetModuleHandle(0);
			 if (windowClass.className.stdString().empty())
				 windowClass.className = LString("BELIEVE IT!!!!");
			 setClass(windowClass);
			 RECT r;
			 r.left = 0, r.top = 0;
			 r.bottom = size.y(), r.right = size.x();
			 AdjustWindowRect(&r, WS_OVERLAPPEDWINDOW, FALSE);
			 winHnd = CreateWindow(windowClass.className.stdString().c_str(), windowName.c_str(), (DWORD)windowStyle, position.x(), position.y(), (r.right - r.left), (r.bottom - r.top), 0, 0, appInst, 0);
			 GetWindowPlacement(winHnd, &winPlac);
			 ShowWindow(winHnd, winPlac.showCmd);
         }

      void Window::showWindow(bool a)
         {
         //Fill Me
         //Note: use null to hide window
			 if (a == true)
				 ShowWindow(winHnd, winPlac.showCmd);
			 else
				 ShowWindow(winHnd, NULL);
         }

      void Window::destroy()
         {
         //Destroy the window, remove from manager, deallocate
			 if (winHnd != 0)
				 DestroyWindow(winHnd);
			 WindowManager::getPtr()->removeObject(this);
			 delete this;
         }

      void Window::setName(const LString& name)
         {
         //Fill Me
			 windowName = name.stdString();
			 if (winHnd != 0)
				 SetWindowText(winHnd, windowName.c_str());
         }

      void Window::setClass(const WindowClass& cl)
         {
         windowClass = cl;
         WNDCLASS w;
         w.style = 0;
         w.cbClsExtra = 0;
         w.cbWndExtra = 0;
         w.hIcon = 0;
         w.hCursor = 0;
         w.hbrBackground = 0;
         w.lpszMenuName = 0;
         w.lpfnWndProc = _windowProcess;
         w.hInstance = appInst;
         w.lpszClassName = &cl.className.front();
         RegisterClass(&w);
         }

      void Window::setPosition(const Vector<int, 2>& pos)
         {
         //Fill Me
			 position = pos;
			 if (winHnd != 0)
				 MoveWindow(winHnd, position.x(), position.y(),  size.x(),  size.y(), true);
	  }

      void Window::setSize(const Vector<int, 2>& sz)
         {
         //Fill Me
			 size = sz;
			 if (winHnd != 0)
				 MoveWindow(winHnd, position.x(), position.y(), size.x(), size.y(), true);
	  }

      void Window::_processMessage()
         {
         //Loop while the message queue is not empty
         //Retreive the next message, translate it, and dispatch it
			 MSG msg;
				 while (PeekMessage(&msg, winHnd, 0, 0, PM_NOREMOVE))
				 {
					 GetMessage(&msg, winHnd, 0, 0);
					 TranslateMessage(&msg);
					 DispatchMessage(&msg);
				 }
         }

      LRESULT CALLBACK Window::_windowProcess(HWND hWnd, UINT msg, WPARAM wParam,
         LPARAM lParam)
         {
         //Translate the message using _translateMessage, the pass the message
         //to the Window Manager's collect message function
			 WindowManager::getPtr()->collectMessage(_translateMessage(msg));
			 return DefWindowProc(hWnd, msg, wParam, lParam);
         }
      }
   }

#endif