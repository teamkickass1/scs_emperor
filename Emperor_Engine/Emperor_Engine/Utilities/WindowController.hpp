#ifndef __EMP_UTL_WINDOW_CONTROLLER_HPP__
#define __EMP_UTL_WINDOW_CONTROLLER_HPP__

#include <Utilities/iWindowController.hpp>
#include <Utilities/iWindow.hpp>
#include "WindowManager.hpp"

namespace Emperor
   {
   namespace Utility
      {
      class WindowController : public iWindowController,
         public Singleton<WindowController, true>
         {
         private:
            WindowManager winMan;
         protected:
         public:
            iWindow* createWindow();
            LArray<MessagePackage> retrieveWindowMessages();
         };
      }
   }

#endif