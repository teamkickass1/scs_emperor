#ifndef __EMP_UTL_INTERNAL_TYPES_HPP__
#define __EMP_UTL_INTERNAL_TYPES_HPP__

#include <Utilities/Types.hpp>
#include "../Windows.hpp"
#include "../Internals.hpp"

namespace Emperor
   {
   namespace Utility
      {
      inline WindowMessage _translateMessage(uint32 m)
         {
         switch(m)
            {
            case WM_NULL: return EMP_WINDOW_NULL;
            case WM_CREATE: return EMP_WINDOW_CREATE;
            case WM_DESTROY: return EMP_WINDOW_DESTROY;
            case WM_MOVE: return EMP_WINDOW_MOVE;
            case WM_SIZE: return EMP_WINDOW_SIZE;
            case WM_ACTIVATE: return EMP_WINDOW_ACTIVATE;
            case WM_SETFOCUS: return EMP_WINDOW_SETFOCUS;
            case WM_KILLFOCUS: return EMP_WINDOW_KILLFOCUS;
            case WM_ENABLE: return EMP_WINDOW_ENABLE;
            case WM_SETREDRAW: return EMP_WINDOW_SETREDRAW;
            case WM_SETTEXT: return EMP_WINDOW_SETTEXT;
            case WM_GETTEXT: return EMP_WINDOW_GETTEXT;
            case WM_GETTEXTLENGTH: return EMP_WINDOW_GETTEXTLENGTH;
            case WM_PAINT: return EMP_WINDOW_PAINT;
            case WM_CLOSE: return EMP_WINDOW_CLOSE;
            case WM_QUERYENDSESSION: return EMP_WINDOW_QUERYENDSESSION;
            case WM_QUIT: return EMP_WINDOW_QUIT;
            case WM_QUERYOPEN: return EMP_WINDOW_QUERYOPEN;
            case WM_ERASEBKGND: return EMP_WINDOW_ERASEBKGND;
            case WM_SYSCOLORCHANGE: return EMP_WINDOW_SYSCOLORCHANGE;
            case WM_ENDSESSION: return EMP_WINDOW_ENDSESSION;
            case WM_SHOWWINDOW: return EMP_WINDOW_SHOWWINDOW;
            case WM_SETTINGCHANGE: return EMP_WINDOW_SETTINGCHANGE;
            case WM_DEVMODECHANGE: return EMP_WINDOW_DEVMODECHANGE;
            case WM_ACTIVATEAPP: return EMP_WINDOW_ACTIVATEAPP;
            case WM_FONTCHANGE: return EMP_WINDOW_FONTCHANGE;
            case WM_TIMECHANGE: return EMP_WINDOW_TIMECHANGE;
            case WM_CANCELMODE: return EMP_WINDOW_CANCELMODE;
            case WM_SETCURSOR: return EMP_WINDOW_SETCURSOR;
            case WM_MOUSEACTIVATE: return EMP_WINDOW_MOUSEACTIVATE;
            case WM_CHILDACTIVATE: return EMP_WINDOW_CHILDACTIVATE;
            case WM_QUEUESYNC: return EMP_WINDOW_QUEUESYNC;
            case WM_GETMINMAXINFO: return EMP_WINDOW_GETMINMAXINFO;
            case WM_PAINTICON: return EMP_WINDOW_PAINTICON;
            case WM_ICONERASEBKGND: return EMP_WINDOW_ICONERASEBKGND;
            case WM_NEXTDLGCTL: return EMP_WINDOW_NEXTDLGCTL;
            case WM_SPOOLERSTATUS: return EMP_WINDOW_SPOOLERSTATUS;
            case WM_DRAWITEM: return EMP_WINDOW_DRAWITEM;
            case WM_MEASUREITEM: return EMP_WINDOW_MEASUREITEM;
            case WM_DELETEITEM: return EMP_WINDOW_DELETEITEM;
            case WM_VKEYTOITEM: return EMP_WINDOW_VKEYTOITEM;
            case WM_CHARTOITEM: return EMP_WINDOW_CHARTOITEM;
            case WM_SETFONT: return EMP_WINDOW_SETFONT;
            case WM_GETFONT: return EMP_WINDOW_GETFONT;
            case WM_SETHOTKEY: return EMP_WINDOW_SETHOTKEY;
            case WM_GETHOTKEY: return EMP_WINDOW_GETHOTKEY;
            case WM_QUERYDRAGICON: return EMP_WINDOW_QUERYDRAGICON;
            case WM_COMPAREITEM: return EMP_WINDOW_COMPAREITEM;
            case WM_COMPACTING: return EMP_WINDOW_COMPACTING;
            case WM_WINDOWPOSCHANGING: return EMP_WINDOW_WINDOWPOSCHANGING;
            case WM_WINDOWPOSCHANGED: return EMP_WINDOW_WINDOWPOSCHANGED;
            case WM_POWER: return EMP_WINDOW_POWER;
            case WM_COPYDATA: return EMP_WINDOW_COPYDATA;
            case WM_CANCELJOURNAL: return EMP_WINDOW_CANCELJOURNAL;
            case WM_NOTIFY: return EMP_WINDOW_NOTIFY;
            case WM_INPUTLANGCHANGEREQUEST: 
               return EMP_WINDOW_INPUTLANGCHANGEREQUEST;
            case WM_INPUTLANGCHANGE: return EMP_WINDOW_INPUTLANGCHANGE;
            case WM_TCARD: return EMP_WINDOW_TCARD;
            case WM_HELP: return EMP_WINDOW_HELP;
            case WM_USERCHANGED: return EMP_WINDOW_USERCHANGED;
            case WM_NOTIFYFORMAT: return EMP_WINDOW_NOTIFYFORMAT;
            case WM_CONTEXTMENU: return EMP_WINDOW_CONTEXTMENU;
            case WM_STYLECHANGING: return EMP_WINDOW_STYLECHANGING;
            case WM_STYLECHANGED: return EMP_WINDOW_STYLECHANGED;
            case WM_DISPLAYCHANGE: return EMP_WINDOW_DISPLAYCHANGE;
            case WM_GETICON: return EMP_WINDOW_GETICON;
            case WM_SETICON: return EMP_WINDOW_SETICON;
            case WM_NCCREATE: return EMP_WINDOW_NCCREATE;
            case WM_NCDESTROY: return EMP_WINDOW_NCDESTROY;
            case WM_NCCALCSIZE: return EMP_WINDOW_NCCALCSIZE;
            case WM_NCHITTEST: return EMP_WINDOW_NCHITTEST;
            case WM_NCPAINT: return EMP_WINDOW_NCPAINT;
            case WM_NCACTIVATE: return EMP_WINDOW_NCACTIVATE;
            case WM_GETDLGCODE: return EMP_WINDOW_GETDLGCODE;
            case WM_NCMOUSEMOVE: return EMP_WINDOW_NCMOUSEMOVE;
            case WM_NCLBUTTONDOWN: return EMP_WINDOW_NCLBUTTONDOWN;
            case WM_NCLBUTTONUP: return EMP_WINDOW_NCLBUTTONUP;
            case WM_NCLBUTTONDBLCLK: return EMP_WINDOW_NCLBUTTONDBLCLK;
            case WM_NCRBUTTONDOWN: return EMP_WINDOW_NCRBUTTONDOWN;
            case WM_NCRBUTTONUP: return EMP_WINDOW_NCRBUTTONUP;
            case WM_NCRBUTTONDBLCLK: return EMP_WINDOW_NCRBUTTONDBLCLK;
            case WM_NCMBUTTONDOWN: return EMP_WINDOW_NCMBUTTONDOWN;
            case WM_NCMBUTTONUP: return EMP_WINDOW_NCMBUTTONUP;
            case WM_NCMBUTTONDBLCLK: return EMP_WINDOW_NCMBUTTONDBLCLK;
            case WM_KEYDOWN: return EMP_WINDOW_KEYDOWN;
            case WM_KEYUP: return EMP_WINDOW_KEYUP;
            case WM_CHAR: return EMP_WINDOW_CHAR;
            case WM_DEADCHAR: return EMP_WINDOW_DEADCHAR;
            case WM_SYSKEYDOWN: return EMP_WINDOW_SYSKEYDOWN;
            case WM_SYSKEYUP: return EMP_WINDOW_SYSKEYUP;
            case WM_SYSCHAR: return EMP_WINDOW_SYSCHAR;
            case WM_SYSDEADCHAR: return EMP_WINDOW_SYSDEADCHAR;
            case WM_KEYLAST: return EMP_WINDOW_KEYLAST;
            case WM_IME_STARTCOMPOSITION: 
               return EMP_WINDOW_IME_STARTCOMPOSITION;
            case WM_IME_ENDCOMPOSITION: return EMP_WINDOW_IME_ENDCOMPOSITION;
            case WM_IME_COMPOSITION: return EMP_WINDOW_IME_COMPOSITION;
            case WM_INITDIALOG: return EMP_WINDOW_INITDIALOG;
            case WM_COMMAND: return EMP_WINDOW_COMMAND;
            case WM_SYSCOMMAND: return EMP_WINDOW_SYSCOMMAND;
            case WM_TIMER: return EMP_WINDOW_TIMER;
            case WM_HSCROLL: return EMP_WINDOW_HSCROLL;
            case WM_VSCROLL: return EMP_WINDOW_VSCROLL;
            case WM_INITMENU: return EMP_WINDOW_INITMENU;
            case WM_INITMENUPOPUP: return EMP_WINDOW_INITMENUPOPUP;
            case WM_MENUSELECT: return EMP_WINDOW_MENUSELECT;
            case WM_MENUCHAR: return EMP_WINDOW_MENUCHAR;
            case WM_ENTERIDLE: return EMP_WINDOW_ENTERIDLE;
            case WM_CTLCOLORMSGBOX: return EMP_WINDOW_CTLCOLORMSGBOX;
            case WM_CTLCOLOREDIT: return EMP_WINDOW_CTLCOLOREDIT;
            case WM_CTLCOLORLISTBOX: return EMP_WINDOW_CTLCOLORLISTBOX;
            case WM_CTLCOLORBTN: return EMP_WINDOW_CTLCOLORBTN;
            case WM_CTLCOLORDLG: return EMP_WINDOW_CTLCOLORDLG;
            case WM_CTLCOLORSCROLLBAR: return EMP_WINDOW_CTLCOLORSCROLLBAR;
            case WM_CTLCOLORSTATIC: return EMP_WINDOW_CTLCOLORSTATIC;
            case WM_MOUSEMOVE: return EMP_WINDOW_MOUSEMOVE;
            case WM_LBUTTONDOWN: return EMP_WINDOW_LBUTTONDOWN;
            case WM_LBUTTONUP: return EMP_WINDOW_LBUTTONUP;
            case WM_LBUTTONDBLCLK: return EMP_WINDOW_LBUTTONDBLCLK;
            case WM_RBUTTONDOWN: return EMP_WINDOW_RBUTTONDOWN;
            case WM_RBUTTONUP: return EMP_WINDOW_RBUTTONUP;
            case WM_RBUTTONDBLCLK: return EMP_WINDOW_RBUTTONDBLCLK;
            case WM_MBUTTONDOWN: return EMP_WINDOW_MBUTTONDOWN;
            case WM_MBUTTONUP: return EMP_WINDOW_MBUTTONUP;
            case WM_MBUTTONDBLCLK: return EMP_WINDOW_MBUTTONDBLCLK;
            case WM_MOUSEWHEEL: return EMP_WINDOW_MOUSEWHEEL;
            case WM_MOUSEHWHEEL: return EMP_WINDOW_MOUSEHWHEEL;
            case WM_PARENTNOTIFY: return EMP_WINDOW_PARENTNOTIFY;
            case WM_ENTERMENULOOP: return EMP_WINDOW_ENTERMENULOOP;
            case WM_EXITMENULOOP: return EMP_WINDOW_EXITMENULOOP;
            case WM_NEXTMENU: return EMP_WINDOW_NEXTMENU;
            case WM_SIZING: return EMP_WINDOW_SIZING;
            case WM_CAPTURECHANGED: return EMP_WINDOW_CAPTURECHANGED;
            case WM_MOVING: return EMP_WINDOW_MOVING;
            case WM_POWERBROADCAST: return EMP_WINDOW_POWERBROADCAST;
            case WM_DEVICECHANGE: return EMP_WINDOW_DEVICECHANGE;
            case WM_MDICREATE: return EMP_WINDOW_MDICREATE;
            case WM_MDIDESTROY: return EMP_WINDOW_MDIDESTROY;
            case WM_MDIACTIVATE: return EMP_WINDOW_MDIACTIVATE;
            case WM_MDIRESTORE: return EMP_WINDOW_MDIRESTORE;
            case WM_MDINEXT: return EMP_WINDOW_MDINEXT;
            case WM_MDIMAXIMIZE: return EMP_WINDOW_MDIMAXIMIZE;
            case WM_MDITILE: return EMP_WINDOW_MDITILE;
            case WM_MDICASCADE: return EMP_WINDOW_MDICASCADE;
            case WM_MDIICONARRANGE: return EMP_WINDOW_MDIICONARRANGE;
            case WM_MDIGETACTIVE: return EMP_WINDOW_MDIGETACTIVE;
            case WM_MDISETMENU: return EMP_WINDOW_MDISETMENU;
            case WM_ENTERSIZEMOVE: return EMP_WINDOW_ENTERSIZEMOVE;
            case WM_EXITSIZEMOVE: return EMP_WINDOW_EXITSIZEMOVE;
            case WM_DROPFILES: return EMP_WINDOW_DROPFILES;
            case WM_MDIREFRESHMENU: return EMP_WINDOW_MDIREFRESHMENU;
            case WM_IME_SETCONTEXT: return EMP_WINDOW_IME_SETCONTEXT;
            case WM_IME_NOTIFY: return EMP_WINDOW_IME_NOTIFY;
            case WM_IME_CONTROL: return EMP_WINDOW_IME_CONTROL;
            case WM_IME_COMPOSITIONFULL: return EMP_WINDOW_IME_COMPOSITIONFULL;
            case WM_IME_SELECT: return EMP_WINDOW_IME_SELECT;
            case WM_IME_CHAR: return EMP_WINDOW_IME_CHAR;
            case WM_IME_KEYDOWN: return EMP_WINDOW_IME_KEYDOWN;
            case WM_IME_KEYUP: return EMP_WINDOW_IME_KEYUP;
            case WM_MOUSEHOVER: return EMP_WINDOW_MOUSEHOVER;
            case WM_NCMOUSELEAVE: return EMP_WINDOW_NCMOUSELEAVE;
            case WM_MOUSELEAVE: return EMP_WINDOW_MOUSELEAVE;
            case WM_CUT: return EMP_WINDOW_CUT;
            case WM_COPY: return EMP_WINDOW_COPY;
            case WM_PASTE: return EMP_WINDOW_PASTE;
            case WM_CLEAR: return EMP_WINDOW_CLEAR;
            case WM_UNDO: return EMP_WINDOW_UNDO;
            case WM_RENDERFORMAT: return EMP_WINDOW_RENDERFORMAT;
            case WM_RENDERALLFORMATS: return EMP_WINDOW_RENDERALLFORMATS;
            case WM_DESTROYCLIPBOARD: return EMP_WINDOW_DESTROYCLIPBOARD;
            case WM_DRAWCLIPBOARD: return EMP_WINDOW_DRAWCLIPBOARD;
            case WM_PAINTCLIPBOARD: return EMP_WINDOW_PAINTCLIPBOARD;
            case WM_VSCROLLCLIPBOARD: return EMP_WINDOW_VSCROLLCLIPBOARD;
            case WM_SIZECLIPBOARD: return EMP_WINDOW_SIZECLIPBOARD;
            case WM_ASKCBFORMATNAME: return EMP_WINDOW_ASKCBFORMATNAME;
            case WM_CHANGECBCHAIN: return EMP_WINDOW_CHANGECBCHAIN;
            case WM_HSCROLLCLIPBOARD: return EMP_WINDOW_HSCROLLCLIPBOARD;
            case WM_QUERYNEWPALETTE: return EMP_WINDOW_QUERYNEWPALETTE;
            case WM_PALETTEISCHANGING: return EMP_WINDOW_PALETTEISCHANGING;
            case WM_PALETTECHANGED: return EMP_WINDOW_PALETTECHANGED;
            case WM_HOTKEY: return EMP_WINDOW_HOTKEY;
            case WM_PRINT: return EMP_WINDOW_PRINT;
            case WM_PRINTCLIENT: return EMP_WINDOW_PRINTCLIENT;
            case WM_HANDHELDFIRST: return EMP_WINDOW_HANDHELDFIRST;
            case WM_HANDHELDLAST: return EMP_WINDOW_HANDHELDLAST;
            case WM_PENWINFIRST: return EMP_WINDOW_PENWINFIRST;
            case WM_PENWINLAST: return EMP_WINDOW_PENWINLAST;
            case WM_USER: return EMP_WINDOW_USER;
            case WM_APP: return EMP_WINDOW_APP;
            default: return EMP_WINDOW_NULL;
            }
         }
      }
   }

#endif