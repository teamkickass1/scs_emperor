#pragma once

#include <../Include/iPathSolver.h>
#include "Pathnode.hpp"
#include "SolverManager.hpp"

namespace Emperor
{
	class PathSolver : public iPathSolver
	{
	private:
		//For movement of actor.
		iNode* actor;
		Vector<float, 3> old_loc; //Actor moving from which pathnode location.
		iPathnode* next_loc;	//Actor moving to which pathnode
		ArrayList<iPathnode*> pathChildren;
		iPathnode* current_Loc = nullptr;
		iPathnode* aCurrent = nullptr;
		iPathnode* destination = nullptr;
		HashMap<int, iPathnode*> openList;
		HashMap<int, iPathnode*> closedList;
		HashMap<int, iPathnode*> cameFrom;
		HashMap<int, float> gScore;
		HashMap<int, float> fScore;

		bool solved = false;
		bool interupt;
	protected:
		iPathnode* aStar(iPathnode* start, iPathnode* end);
		virtual ~PathSolver() { }
	public:
		void notifyInterupt();
		void MoveToNext();

		iPathnode* reconstructPath(HashMap<int, iPathnode*>, iPathnode*);

		PathSolver(iPathnode* Location, iNode* act);

		bool getSolved() { return solved; }
		void setDestination(iPathnode* dest);
		void setStart(iPathnode* start);
		void solvePath();
		float hValue(iPathnode* start, iPathnode* end);
		iPathnode* getNextStep() { return pathChildren.front(); }
		float max3(float,float,float);
		void destroy();

	};

}
