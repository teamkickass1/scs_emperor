#pragma once

#include "PathSolver.hpp"
#include "../Node.hpp"

namespace Emperor
{
	//Note: I don't think this needs to be templated because we know that the manager is only going to be using the PathSolver and nothing else.
	class PathSolver;
	template <class P = PathSolver>
	class SolverManager : public Singleton<SolverManager<P>>
	{
	private:
		ArrayList<P*> solvers;
	protected:
	public:
		SolverManager() { }
		virtual ~SolverManager()
		{
			//while (!solvers.empty())
			//{
		//		solvers.front()->destroy();
			//}
		}

		void tickSolver()
		{
			if (!solvers.empty())
			{
				for (auto& i : solvers)
				{
					i->solvePath();
				}
			}
		}

		iPathSolver* getPathSolver(iPathnode* Location, iNode* actor)
		{
			solvers.push_back(new P(Location, actor));
			return (iPathSolver*)solvers.back();
		}

		void removePathSolver(P* element)
		{
			auto i = std::find(solvers.begin(), solvers.end(), element);
			if (i != solvers.end())
				solvers.erase(i);
		}

	};

}
