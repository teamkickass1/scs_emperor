#pragma once
#include "../Include/Core/Math.hpp"
#include <unordered_set>
#include <unordered_map>
#include "Pathnode.hpp"
#include <array>
//Override hash to use UnorderedSet
namespace std
{
	template <>
	struct hash<Emperor::Vector<float, 3>>
	{
		size_t operator()(Emperor::Vector<float,3> const & x) const
		{
			return (
			(51 + std::hash<int>()(x.x())) * 51
			+ std::hash<int>()(x.y() + std::hash<int>()(x.z()))
			);
		}
	};
}

namespace Emperor{
	template<class Location>
	struct Graph
	{
		typedef typename std::vector<Location>::iterator iterator;
		std::unordered_map<Location, ArrayList<Location>> edges;
		inline const ArrayList<Location> neighbors(Location id) { return edges[id]; }
	};

	struct SquareGrid {
		typedef Vector<float, 3> Location;
		static std::array<Location, 4> DIRS;

		int width, height;
		std::unordered_set<Location> walls;

		SquareGrid(int width_, int height_)
			: width(width_), height(height_) {}

		inline bool in_bounds(Location id) {
			int x = id.x(), y = id.y(), z = id.z();
			return 0 <= x && x < width && 0 <= y && y < height;
		}

		inline bool passable(Location id) {
			return !walls.count(id);
		}

		std::vector<Location> neighbors(Location id) {
			int x = id.x(), y = id.y(), z = id.z(), dx, dy, dz;
			std::vector<Location> results;

			for (auto dir : DIRS) {
				dx = dir.x(), dy = dir.y(), dz = dir.z();
				Location next = Vector3(x + dx, y + dy, z + dz);
				if (in_bounds(next) && passable(next)) {
					results.push_back(next);
				}
			}

			if ((x + y) % 2 == 0) {
				// aesthetic improvement on square grids
				std::reverse(results.begin(), results.end());
			}

			return results;
		}
	};

	//This gives a Linker error, not sure why at the moment.
	//std::array<SquareGrid::Location, 4> SquareGrid::DIRS{ { Vector3(1, 0, 0) ,  Vector3(0, -1, 0), Vector3(-1,0,0), Vector3(0,1,0)  } };

	struct GridWithWeight : SquareGrid
	{
		std::unordered_set<Location> tiles;
		GridWithWeight(int w, int h) : SquareGrid(w, h) {}
		int cost(Location a, Location b) {
			return tiles.count(b) ? 5 : 1;
		}
	};

	//Function may only work for command line
	template<class Graph>
	void draw_grid(const Graph& graph, int field_width,
		std::unordered_map<Pathnode, int>* distances = nullptr,
		std::unordered_map<Pathnode,  Pathnode>* point_to = nullptr,
		ArrayList<Vector<float,3>>* path = nullptr) {
		int z = 0;
		for (int y = 0; y != graph.height; ++y) {
			for (int x = 0; x != graph.width; ++x) {
				Graph::Location id = Vector3(x,y,z);
				std::cout << std::left << std::setw(field_width);
				if (graph.walls.count(id)) {
					std::cout << string(field_width, '#');
				}
				else if (point_to != nullptr && point_to->count(id)) {
					int x2, y2;
					tie(x2, y2) = (*point_to)[id];
					// TODO: how do I get setw to work with utf8?
					if (x2 == x + 1) { std::cout << "\u2192 "; }
					else if (x2 == x - 1) { std::cout << "\u2190 "; }
					else if (y2 == y + 1) { std::cout << "\u2193 "; }
					else if (y2 == y - 1) { std::cout << "\u2191 "; }
					else { std::cout << "* "; }
				}
				else if (distances != nullptr && distances->count(id)) {
					std::cout << (*distances)[id];
				}
				else if (path != nullptr && find(path->begin(), path->end(), id) != path->end()) {
					std::cout << '@';
				}
				else {
					std::cout << '.';
				}
			}
			std::cout << std::endl;
		}
	}
}