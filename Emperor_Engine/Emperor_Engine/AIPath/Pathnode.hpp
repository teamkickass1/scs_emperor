#pragma once
#include <map>
#include "../Include/Core/Math.hpp"
#include "../Node.hpp"
#include <../Include/iPathnode.hpp>

namespace Emperor{
	class Pathnode: public Node<iPathnode>{
	private:
		int ID;
	public:
		std::map<int, iPathnode*> neighbours;

		//Default
		Pathnode(): Node(0){
			neighbours.clear();
			ID = -1;
		}
		/*
		//Add name only
		Pathnode(int b) : Node(0){
			ID= b;
		}*/
		//Setters
		void setID(int n){
			ID = n;
		}
		void setNeighbour(int id, iPathnode* neighbour){
			neighbours[id] = neighbour;

			//neighbour->setNeighbour(ID, this);
		}
		//Getters
		int getID(){
			return ID;
		}
		std::map<int, iPathnode*> getNeighbours(){
			return neighbours;
		}
		~Pathnode(){
			/*
			for (auto i = 0; i < neighbour.size(); i++){
				delete(neighbour[i]);
			}
			name.clear();
			name = nullptr;
			*/
		}
	};
}