#include "PathSolver.hpp"
#include <algorithm>
#include <math.h>
#include "SolverManager.hpp"
namespace Emperor
{
	PathSolver::PathSolver(iPathnode* Location, iNode* act)
	{
		current_Loc = Location;
		destination = nullptr;
		actor = act;
		old_loc = current_Loc->getPosition();
		next_loc = current_Loc;
	}

	void PathSolver::setDestination(iPathnode* dest)
	{
		destination = dest;
		if (next_loc == nullptr)
			notifyInterupt();
	}

	void PathSolver::MoveToNext()
	{
		Vector<float, 3> act_pos = actor->getPosition();
		Vector<float, 3> dir = (next_loc->getPosition() - act_pos).normalizedCopy();
		dir = dir * 0.01;
		actor->translate(dir);
		act_pos.write_x(act_pos.x() + dir.x());
		act_pos.write_y(act_pos.y() + dir.y());
		act_pos.write_z(act_pos.z() + dir.z());
		actor->setPosition(act_pos);

	//	LOG("From MoveToNext... actor_pos is: [" << roundf(act_pos.x() * 100) / 100 << "," << roundf(act_pos.y() * 100) / 100 
		//	<< "," << roundf(act_pos.z() * 100) / 100 << "]" << " || next_loc is: " << next_loc->getPosition());
	}
	
	void PathSolver::solvePath()
	{
		if (destination != nullptr)
		{
			Vector<float, 3> act = actor->getPosition();
			if (next_loc == nullptr) next_loc = current_Loc;
			Vector<float, 3> nxt = next_loc->getPosition();
			float actx, acty, actz, nxtx, nxty, nxtz;
			actx = floor(act.x() * 100.00 + 0.5) / 100.00;
			acty = floor(act.y() * 100.00 + 0.5) / 100.00;
			actz = floor(act.z() * 100.00 + 0.5) / 100.00;
			nxtx = floor(nxt.x() * 100.00 + 0.5) / 100.00;
			nxty = floor(nxt.y() * 100.00 + 0.5) / 100.00;
			nxtz = floor(nxt.z() * 100.00 + 0.5) / 100.00;

			if (actx == nxtx && acty == nxty && actz == nxtz)
			{
				actor->setPosition(next_loc->getPosition());
				LOG("Actor's location is from solvePath func : " << actor->getPosition());
				current_Loc = next_loc;
				iPathnode* p = nullptr;

				if (destination->getPosition() != current_Loc->getPosition())
				{
					p = aStar(current_Loc, destination);
				}
				else
				{
					destination = nullptr;
					solved = true;
					next_loc = nullptr;
					fScore.clear();
					gScore.clear();
					openList.clear();
					closedList.clear();

				}

				if (p != nullptr)
				{
					next_loc = p;
				}
			}
			if (next_loc != nullptr)
			{
				old_loc = current_Loc->getPosition();
				MoveToNext();
			}
		}
	}

	void PathSolver::notifyInterupt()
	{
		if (next_loc != nullptr)
		{ 
			if (destination != nullptr && next_loc->getPosition() != actor->getPosition())
			{ 
				float home_length = (old_loc.length() - actor->getPosition().length());
				float dest_length = (next_loc->getPosition().length() - actor->getPosition().length());

				if (home_length < dest_length)
				{
					next_loc->setPosition(old_loc);
				}
			}
		}
	}

	iPathnode* PathSolver::aStar(iPathnode* start, iPathnode* end)
	{
		iPathnode* next = nullptr;
		aCurrent = start;
		//Remove start node from children list
		if (!pathChildren.empty())
			pathChildren.pop_back();
		float lowestF = 9999999;
		int tentGScore = 0;
		int id;
		gScore[aCurrent->getID()] = 0;
		fScore[aCurrent->getID()] = gScore[aCurrent->getID()] + hValue(aCurrent, end);
		//add start node to open list
		openList[aCurrent->getID()] = aCurrent;
		
			//current node is node with lowest fScore
			for (auto i : fScore)
				if (i.second < lowestF)
				{
					if (openList.find(i.first) != openList.end())
					{
						lowestF = i.second;
						id = i.first;
					}
				}
			aCurrent = openList[id];
			auto q = openList.find(id);
			openList.erase(q);
			closedList[aCurrent->getID()] = aCurrent;
			//Look at current nodes neighbors
//Added by Gaurav
			std::map<int, iPathnode*> neigh = aCurrent->getNeighbours();
			for (auto i : neigh)
			{
				//Check if neighbor already evaluated
				if (closedList.find(i.first) != closedList.end()){ LOG("HERE");					continue; }
				//Find length of path
				tentGScore = gScore[aCurrent->getID()] + hValue(aCurrent, i.second);
				//Discover new node
				if (openList.find(i.first) == openList.end())
					openList[i.first] = i.second;
				else if (tentGScore >= gScore[i.first])
				{
					continue;
				}

				//This is the best path until now
				gScore[i.first] = tentGScore;
				fScore[i.first] = gScore[i.first] + hValue(i.second, end);
				LOG("ID: " << i.first << " Gscore: " << gScore[i.first] << " TentG: " << tentGScore 
					<< " hVal: " << hValue(aCurrent, i.second) << " FScore: " << fScore[i.first]);

			}
			float lowG = 1000000.0f;
			//Find lowest F again to find next node to go to
			for (auto i : fScore)
				if (i.second < lowG)
				{
					if (openList.find(i.first) != openList.end())
					{
						lowG = i.second;
						id = i.first;
					}
				}
			lowestF = 9999999;
			next = openList[id];
		//	next->attachTo(aCurrent);
		//Failed
			LOG("Returning: " << next->getID());
		return next;
	}

	float PathSolver::hValue(iPathnode* start, iPathnode* end)
	{
		//Return variable corresponding to heuristic function you want to use

		//Manhattan Distance: distance between two points measured along axes at right angles
		float manhattanDistance = abs(start->getPosition().x() - end->getPosition().x()) + 
			abs(start->getPosition().y() - end->getPosition().y()) + 
			abs(start->getPosition().z() - end->getPosition().z()); 

		//Euclidian Distance: "ordinary" distance between two points (includes diagonal)
		float euclidianDistance = sqrt(((start->getPosition().x() - end->getPosition().x()) * (start->getPosition().x() - end->getPosition().x())) +
			((start->getPosition().y() - end->getPosition().y()) * (start->getPosition().y() - end->getPosition().y())) +
			((start->getPosition().z() - end->getPosition().z()) * (start->getPosition().z() - end->getPosition().z())));

		//Chebyshev Distance: defined on a vector space where the distance between two vectors is the greatest of their differences along any coordinate dimension.
		float chebyDistance = max3(abs(start->getPosition().x() - end->getPosition().x()),
			abs(start->getPosition().y() - end->getPosition().y()),
			abs(start->getPosition().z() - end->getPosition().z()));

		return euclidianDistance;

	}

	float PathSolver::max3(float x, float y, float z)
	{
		float max = x;

		if (y > max)
			max = y;
		else if (z > max)
			max = z;
		return max;
	}
	
	void PathSolver::destroy()
	{
		SolverManager<>::getPtr()->removePathSolver(this);
		delete this;
	}
}