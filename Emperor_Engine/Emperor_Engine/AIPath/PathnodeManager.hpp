#pragma once

#include "Pathnode.hpp"

namespace Emperor
{
	class PathnodeManager : public Singleton<PathnodeManager>
	{
		private:
			ArrayList<Pathnode*> pathnodes;
		protected:
		public:
			PathnodeManager() { }
			virtual ~PathnodeManager()
			{
			//	while (!pathnodes.empty())
				//{
					//pathnodes.front()->destroy();
				//}
			}

			iPathnode* createPathnode()
			{
				pathnodes.push_back(new Pathnode());
				return (iPathnode*)pathnodes.back();
			}

			void removePathnode(iPathnode* element)
			{
				Pathnode* p = (Pathnode*)element;
				auto i = std::find(pathnodes.begin(), pathnodes.end(), p);
				if (i != pathnodes.end())
					pathnodes.erase(i);
			}
	};
}