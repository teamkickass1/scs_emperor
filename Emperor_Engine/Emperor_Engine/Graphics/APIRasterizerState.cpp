#include "APIRasterizerState.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
{
	namespace Graphics
	{
#if EMP_USE_DIRECTX
		APIRasterizerState<RS_DX11>::~APIRasterizerState()
		{
			if (rasterState)
				rasterState->Release();
		}

		void APIRasterizerState<RS_DX11>::init(const RasterizerStateDescription& r)
		{
			//Create a new DX11 rasterizer desc and fill it with the information
			//in (r), converting where nessisary (check APITranslation.hpp)
			//Use EMP_DEVICE_ASSERT_DX when creating the rasterizer state
			if (rasterState != nullptr)
				rasterState->Release();

			D3D11_RASTERIZER_DESC d;

			d.AntialiasedLineEnable = r.antialiasedLineEnabled;//true or false
			d.CullMode = _dxTranCull(r.cullMode);//face cull mode, none, front, or back
			d.DepthBias = r.depthBias;//The depth bias
			d.DepthBiasClamp = r.depthBiasClamp;//The maximum depth bias
			d.DepthClipEnable = r.depthClipEnabled;//true or false, clips far plane
			d.FillMode = _dxTranFill(r.fillMode);//wireframe or solid
			d.FrontCounterClockwise = !r.windCounterClockwise;//true or false, wind order
			d.MultisampleEnable = r.multisampleEnabled;//true or false, anti-aliasing
			d.ScissorEnable = r.scissorEnabled;//true or false, scissor culling
			d.SlopeScaledDepthBias = r.slopeScaledDepthBias;//sloped depth bias

			EMP_DEVICE_ASSERT_DX("Creating API Resterizer", CreateRasterizerState(&d, &rasterState));

		}

		void APIRasterizerState<RS_DX11>::bind()
		{
			//Fill Me
			//Note: use singleton to expose context
			Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->RSSetState(rasterState);
		}
#endif

#if EMP_USE_OPENGL
		inline void _glEnProx(GLuint a)
		{
			glEnable(a);
		}

		inline void _glDsProx(GLuint a)
		{
			glDisable(a);
		}

		void APIRasterizerState<RS_GL43>::init(const RasterizerStateDescription& r)
		{
			//Fill for GL component

			//for some attributes, check if the associated value in (r) is true,
			//if so, assign _glEnProx, else assign _glDsProx. aaLine is provided
			//as an example. This only applies to attributes which are function
			//pointers.

			aaLine = r.antialiasedLineEnabled ? _glEnProx : _glDsProx;
			cullFace = r.cullMode ? _glEnProx : _glDsProx;
			cullDir = r.cullMode;
			depthBias = r.depthBias;
			depthClamp = r.depthBiasClamp;
			fillMode = _glTranFill(r.fillMode);
			windOrder = r.windCounterClockwise;
			multiSample = r.multisampleEnabled ? _glEnProx : _glDsProx;
			scissorTest = r.scissorEnabled ? _glEnProx : _glDsProx;
			depthClip = r.depthClipEnabled ? _glEnProx : _glDsProx;
		}

		void APIRasterizerState<RS_GL43>::bind()
		{
			//Fill for GL component

			//for function pointer attributes, call the function and pass in the
			//associated #define. aaLine provided as an example. All other
			//attributes must call correct gl functions
			aaLine(GL_LINE_SMOOTH);
			cullFace(GL_CULL_FACE);
			glCullFace(cullDir);
			glPolygonOffset(depthClamp, (GLfloat)depthBias);
			glPolygonMode(GL_FRONT_AND_BACK, fillMode);
			glFrontFace(windOrder);
			multiSample(GL_MULTISAMPLE);
			scissorTest(GL_SCISSOR_TEST);
			depthClip(GL_DEPTH_CLAMP);

		}

		APIRasterizerState<RS_GL43>::APIRasterizerState()
		{//defaults
			aaLine = _glDsProx;
			cullFace = _glDsProx;
			cullDir = GL_FRONT;
			depthBias = 0;
			depthClamp = 0;
			fillMode = GL_FILL;
			windOrder = GL_CCW;
			multiSample = _glDsProx;
			scissorTest = _glDsProx;
			depthClip = _glDsProx;
		}
#endif
	}
}
