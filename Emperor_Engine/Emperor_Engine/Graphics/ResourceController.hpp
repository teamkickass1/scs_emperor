#ifndef __EMP_GFX_RESOURCE_CONTROLLER_HPP__
#define __EMP_GFX_RESOURCE_CONTROLLER_HPP__

#include <Graphics/iResourceController.hpp>
#include "Managers.hpp"
#include "ShaderManager.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class ResourceController : public iResourceController
         {
         private:
         protected:
         public:
            VertexFormatManager<RS> vfmMan;
            MeshManager<RS> mshMan;
            MaterialManager<RS> matMan;
            TextureManager<RS> texMan;
            SamplerManager<RS> smpMan;
            BlendStateManager<RS> blnMan;
            ShaderManager<RS> shdMan;

            //LAB: Should call the getResource function on the appropriate 
            //manager, passing in the name (n)

            const iVertexFormat* getVertexFormat(const LString& n)
               { return vfmMan.getResource(n.stdString());}
            const iMesh* getMesh(const LString& n)
               { return mshMan.getResource(n.stdString()); }
            const iMaterial* getMaterial(const LString& n)
               { return matMan.getResource(n.stdString()); }
            const iTexture* getTexture(const LString& n)
               { return texMan.getResource(n.stdString()); }
            const iTextureSampler* getSampler(const LString& n)
               { return smpMan.getResource(n.stdString()); }
            const iBlendState* getBlendState(const LString& n)
               { return blnMan.getResource(n.stdString()); }

            //LAB: Calls the appropriate object creation function on the 
            //appropriate manager, passing in the name (n)

            iVertexFormat* newVertexFormat(const LString& n)
               { return vfmMan.newResource(n.stdString()); }
            iMesh* newCustomMesh(const LString& n)
               { return mshMan.newResource(n.stdString()); }
            iMaterial* newCustomMaterial(const LString& n)
               { return matMan.newResource(n.stdString()); }
            iTexture1D* newCustomTexture1D(const LString& n)
               { return texMan.newTex1D(n.stdString()); }
            iTexture2D* newCustomTexture2D(const LString& n)
               { return texMan.newTex2D(n.stdString()); }
            iTexture3D* newCustomTexture3D(const LString& n)
               { return texMan.newTex3D(n.stdString()); }
            iTexture2D* newCustomRenderTarget(const LString& n)
               { return texMan.newTarget(n.stdString()); }
            iTexture2D* newCustomDepthStencil(const LString& n)
               { return texMan.newDepth(n.stdString()); }
            iTextureSampler* newCustomSampler(const LString& n)
               { return smpMan.newResource(n.stdString()); }
            iBlendState* newBlendState(const LString& n)
               { return blnMan.newResource(n.stdString()); }
         };
      }
   }

#endif