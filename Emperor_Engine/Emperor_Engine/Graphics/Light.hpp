#ifndef __EMP_GFX_LIGHT_HPP__
#define __EMP_GFX_LIGHT_HPP__

#include <Graphics/iLight.hpp>
#include "SceneObject.hpp"
#include "Texture.hpp"
#include "Camera.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class Light : public SceneObject<RS, iLight>
         {
         private:
            LightType type;
            Color  diffuse;
            Color specular;
            float range;
            Vector<float, 3> attenuation;
            Vector<float, 3> spot;
            Camera<RS>* shadowCam;
            DepthStencil<RS>* shadowTexture;
            bool shadowEnabled;
         protected:
            virtual ~Light() {}

            void update();
         public:
            Light();

            void setLightType(LightType);
            LightType getLightType() const { return type; }

            void setDiffuse(const Color&);
            Color getDiffuse() const { return diffuse; }

            void setSpecular(const Color&);
            Color getSpecular() const { return specular; }

            void setAttenuation(float c, float l, float q)
               { setAttenuation(Vector3(c, l, q)); }
            void setAttenuation(const Vector<float, 3>&);
            Vector<float, 3> getAttenuation() const { return attenuation; }

            void setSpot(float u, float p, float f)
               { setSpot(Vector3(u, p, f)); }
            void setSpot(const Vector<float, 3>&);
            Vector<float, 3> getSpot() const { return spot; }

            void activate();
            void deactivate();

            void setRange(float);
            float getRange() { return range; }

            void configureShadowTexture(uint32 width, uint32 height);

            void enableShadows(bool);
            bool castsShadows() { return shadowEnabled; }

            void _bindShadow(uint32);

            void destroy();
         };
      }
   }

#endif