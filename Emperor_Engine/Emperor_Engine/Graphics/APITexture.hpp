#ifndef __EMP_GFX_API_TEXTURE_HPP__
#define __EMP_GFX_API_TEXTURE_HPP__

#include "../DirectX.hpp"
#include "../OpenGL.hpp"
#include "InternalTypes.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class APITexture {};

      template <RenderSystem RS>
      class APITexture1D {};

      template <RenderSystem RS>
      class APITexture2D {};

      template <RenderSystem RS>
      class APIRenderTarget {};

      template <RenderSystem RS>
      class APIDepthStencil {};

      template <RenderSystem RS>
      class APITexture3D {};

#if EMP_USE_DIRECTX
      template <>
      class APITexture<RS_DX11>
         {
         private:
         protected:
            ID3D11ShaderResourceView* texture;
         public:
            APITexture() : texture(0) {}
            virtual ~APITexture();

            void loadFile(const String&);
            void loadFromMem(byte*, uint32);

            void bindToVertex(uint32 slot) const;
            void bindToGeometry(uint32 slot) const;
            void bindToFragment(uint32 slot) const;
         };

      template <>
      class APITexture1D<RS_DX11> : public APITexture<RS_DX11>
         {
         private:
            ID3D11Texture1D* tex1d;
         protected:
         public:
            APITexture1D() : tex1d(0) {}
            virtual ~APITexture1D();

            void init(uint32, StructureFormat);
            void fill(byte*, uint32);
         };

      template <>
      class APITexture2D<RS_DX11> : public APITexture<RS_DX11>
         {
         private:
         protected:
            ID3D11Texture2D* tex2d;
         public:
            APITexture2D() : tex2d(0) {}
            virtual ~APITexture2D();

            void init(uint32, uint32, StructureFormat);
            void fill(byte*, uint32);
         };

      template <>
      class APIRenderTarget<RS_DX11> : public APITexture2D<RS_DX11>
         {
         private:
            ID3D11RenderTargetView* rend;
         protected:
         public:
            APIRenderTarget() : rend(0) {}
            virtual ~APIRenderTarget();

            void init(uint32, uint32, StructureFormat);
            const ID3D11RenderTargetView* _exposeAPI() const { return rend; }
         };

      template <>
      class APIDepthStencil<RS_DX11> : public APITexture2D<RS_DX11>
         {
         private:
            ID3D11DepthStencilView* dsv;
         protected:
         public:
            APIDepthStencil() : dsv(0) {}
            virtual ~APIDepthStencil();

            void init(uint32, uint32, StructureFormat);
            const ID3D11DepthStencilView* _exposeAPI() const { return dsv; }
         };

      template <>
      class APITexture3D<RS_DX11> : public APITexture<RS_DX11>
         {
         private:
            ID3D11Texture3D* tex3d;
         protected:
         public:
            APITexture3D() : tex3d(0) {}
            virtual ~APITexture3D();

            void init(uint32, uint32, uint32, StructureFormat);
            void fill(byte*, uint32);
         };
#endif

#if EMP_USE_OPENGL
      template<>
      class APITexture<RS_GL43>
         {
         private:
         protected:
            GLuint textureID;
            GLint iFormat;
            GLuint format;
            GLuint type;
            GLuint dim;
         public:
            APITexture() : textureID(0), format(0), iFormat(0), type(0), dim(0) {}
            virtual ~APITexture();

            void loadFile(const String&);
            void loadFromMem(byte*, uint32);

            void bindToVertex(uint32 slot) const;
            void bindToGeometry(uint32 slot) const;
            void bindToFragment(uint32 slot) const;
            GLuint _exposeAPI() const { return textureID; }
         };

      template<>
      class APITexture1D<RS_GL43> : public APITexture<RS_GL43>
         {
         private:
         protected:
         public:
            APITexture1D() {}
            virtual ~APITexture1D() {}

            void init(uint32, StructureFormat);
            void fill(byte*, uint32);
         };

      template<>
      class APITexture2D<RS_GL43> : public APITexture<RS_GL43>
         {
         private:
         protected:
            uint32 wth, hgt;
         public:
            APITexture2D() : wth(0), hgt(0) {}
            virtual ~APITexture2D() {}

            void init(uint32, uint32, StructureFormat);
            void fill(byte*, uint32);
            void setAsTarget() {}
         };

      template <>
      class APIRenderTarget<RS_GL43> : public APITexture2D<RS_GL43>
         {
         private:
            GLuint framebuffer;
         protected:
         public:
            APIRenderTarget() : framebuffer(0) {}
            virtual ~APIRenderTarget();

            void init(uint32, uint32, StructureFormat);
            const GLuint _exposeFrame() const { return framebuffer; }
         };

      template <>
      class APIDepthStencil<RS_GL43> : public APITexture2D<RS_GL43>
         {
         private:
            GLuint depthBuffer;
         protected:
         public:
            APIDepthStencil() : depthBuffer(0) {}
            virtual ~APIDepthStencil();

            void init(uint32, uint32, StructureFormat);
            const GLuint _exposeDepth() const { return depthBuffer; }
         };

      template<>
      class APITexture3D<RS_GL43> : public APITexture<RS_GL43>
         {
         private:
            uint32 wth, hgt, dth;
         protected:
         public:
            APITexture3D() : wth(0), hgt(0), dth(0) {}
            virtual ~APITexture3D() {}

            void init(uint32, uint32, uint32, StructureFormat);
            void fill(byte*, uint32);
         };
#endif
      }
   }
#endif
