#include "APIShader.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"
#include "../Utilities/Defaults.hpp"
#include "../ShaderReader.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      APIVertexShader<RS_DX11>::~APIVertexShader()
         {
         if(vs)
            vs->Release();
         if(layout)
            layout->Release();
         }

      void APIVertexShader<RS_DX11>::init(const String& fileName, const String& function, const iVertexFormat& v)
         {
         ArrayList<char> contents;
         iFile* file = File::create();
         file->open(fileName + ".hlsl", ios::in | ios::binary, false);

         if(!file->good())
            {
            delete file;
            EMP_RESOURCE_ERROR(String("Unable to open shader file ") + fileName + ".hlsl");
            }

         while(file->good())
            contents.push_back(file->get());
         contents.pop_back();

         if(fileName.back() == 'E' && *(&fileName.back() - 1) == '.')
            decrypeShader(contents);

         //Strip \r
         String out;
         for(auto i = contents.begin(), end = contents.end(); i != end; i++)
         if(*i != '\r')
            out += *i;

         initFromDef(out, function, v);
         delete file;
         }

      void APIVertexShader<RS_DX11>::initFromDef(const String& def, const String& function, const iVertexFormat& v, const String& fn)
         {
         ID3D10Blob* sh = 0;
         ID3D10Blob* em = 0;

         //inject shader code
         String injDef = String(Defaults::VShaderInjectDX) + def;

         if(FAILED(D3DX11CompileFromMemory(injDef.c_str(), injDef.size(), fn.c_str(), 0, 0, function.c_str(),
            "vs_5_0", 1 << 15, 0, 0, &sh, &em, 0)))
            {
            EMP_DEVICE_ERROR((String("Shader Compilation failed for ") + fn + "::" + function +
               " with vs_5_0\n  Error Message: " + (em ? String((char*)em->GetBufferPointer()) : String("File unavailable")) + "\n").c_str());
            }

         EMP_DEVICE_NOT_INIT_ASSERT(RS_DX11);
         EMP_DEVICE_ASSERT_DX("create vertex shader", CreateVertexShader(sh->GetBufferPointer(), sh->GetBufferSize(), 0, &vs));

         D3D11_INPUT_ELEMENT_DESC* desc;
         auto ia = v.getVertexDescription().stdVector();
         _dxTranVertex(ia, &desc);

         EMP_DEVICE_ASSERT_DX("construct vertex layout", CreateInputLayout(desc, ia.size(), sh->GetBufferPointer(), sh->GetBufferSize(), &layout));
         }

      void APIVertexShader<RS_DX11>::bind()
         {
         Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->VSSetShader(
            vs, 0, 0);
         Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->IASetInputLayout(
            layout);
         }

      APIGeometryShader<RS_DX11>::~APIGeometryShader()
         {
         if(gs)
            gs->Release();
         }

      void APIGeometryShader<RS_DX11>::init(const String& fileName, const String& function)
         {
         ArrayList<char> contents;
         iFile* file = File::create();
         file->open(fileName + ".hlsl", ios::in | ios::binary, false);

         if(!file->good())
            {
            delete file;
            EMP_RESOURCE_ERROR(String("Unable to open shader file ") + fileName + ".hlsl");
            }

         while(file->good())
            contents.push_back(file->get());
         contents.pop_back();

         if(fileName.back() == 'E' && *(&fileName.back() - 1) == '.')
            decrypeShader(contents);

         //Strip \r
         String out;
         for(auto i = contents.begin(), end = contents.end(); i != end; i++)
         if(*i != '\r')
            out += *i;

         initFromDef(out, function);
         delete file;
         }

      void APIGeometryShader<RS_DX11>::initFromDef(const String& def, const String& function, const String& fn)
         {
         ID3D10Blob* sh = 0;
         ID3D10Blob* em = 0;

         //inject shader code
         String injDef = String(Defaults::GShaderInjectDX) + def;

         if(FAILED(D3DX11CompileFromMemory(injDef.c_str(), injDef.size(), fn.c_str(), 0, 0, function.c_str(),
            "gs_5_0", 1 << 15, 0, 0, &sh, &em, 0)))
            {
            EMP_DEVICE_ERROR((String("Shader Compilation failed for ") + fn + "::" + function +
               " with gs_5_0\n  Error Message: " + (em ? String((char*)em->GetBufferPointer()) : String("File unavailable")) + "\n").c_str());
            }

         EMP_DEVICE_ASSERT_DX("construct geomtry shader", CreateGeometryShader(sh->GetBufferPointer(), sh->GetBufferSize(), 0, &gs));
         }

      void APIGeometryShader<RS_DX11>::bind()
         {
         Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->GSSetShader(
            gs, 0, 0);
         }

      APIFragmentShader<RS_DX11>::~APIFragmentShader()
         {
         if(fs)
            fs->Release();
         }

      void APIFragmentShader<RS_DX11>::init(const String& fileName, const String& function)
         {
         ArrayList<char> contents;
         iFile* file = File::create();
         file->open(fileName + ".hlsl", ios::in | ios::binary, false);

         if(!file->good())
            {
            delete file;
            EMP_RESOURCE_ERROR(String("Unable to open shader file ") + fileName + ".hlsl");
            }

         while(file->good())
            contents.push_back(file->get());
         contents.pop_back();

         if(fileName.back() == 'E' && *(&fileName.back() - 1) == '.')
            decrypeShader(contents);

         //Strip \r
         String out;
         for(auto i = contents.begin(), end = contents.end(); i != end; i++)
         if(*i != '\r')
            out += *i;

         initFromDef(out, function);
         delete file;
         }

      void APIFragmentShader<RS_DX11>::initFromDef(const String& def, const String& function, const String& fn)
         {
         ID3D10Blob* sh = 0;
         ID3D10Blob* em = 0;

         //inject shader code
         String injDef = String(Defaults::FShaderInjectDX) + def;

         if(FAILED(D3DX11CompileFromMemory(injDef.c_str(), injDef.size(), fn.c_str(), 0, 0, function.c_str(),
            "ps_5_0", 1 << 15, 0, 0, &sh, &em, 0)))
            {
            EMP_DEVICE_ERROR((String("Shader Compilation failed for ") + fn + "::" + function +
               " with ps_5_0\n  Error Message: " + (em ? String((char*)em->GetBufferPointer()) : String("File unavailable")) + "\n").c_str());
            }

         EMP_DEVICE_ASSERT_DX("construct fragment shader", CreatePixelShader(sh->GetBufferPointer(), sh->GetBufferSize(), 0, &fs));
         }

      void APIFragmentShader<RS_DX11>::bind()
         {
         Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->PSSetShader(
            fs, 0, 0);
         }
#endif

#if EMP_USE_OPENGL
      static inline String _loadShader(const String& fn)
         {
         ArrayList<char> shaderString;
         iFile* vShader = File::create();
         vShader->open(fn + ".glsl", in | binary, false);
         if(!vShader->good())
            {
            delete vShader;
            EMP_DEVICE_ERROR(String("Unable to open ") + fn + " for reading, using default...");
            }

         String ts;
         while(vShader->good())
            {
            shaderString.push_back(vShader->get());
            }
         shaderString.pop_back();

         if(fn.back() == 'E' && *(&fn.back() - 1) == '.')
            decrypeShader(shaderString);

         //Strip \r
         String out;
         for(auto i = shaderString.begin(), end = shaderString.end(); i != end; i++)
         if(*i != '\r')
            out += *i;

         delete vShader;
         return out;
         }

      void APIVertexShader<RS_GL43>::init(const String& fn, const String& fnc, const iVertexFormat& v)
         {
         initFromDef(_loadShader(fn), fnc, v, fn);
         }

      void APIVertexShader<RS_GL43>::initFromDef(const String& def, const String& fnc, const iVertexFormat&, const String& fn)
         {
         //replace the function's name with main
         String nDef = def;
         nDef.insert(nDef.find('\n'), Defaults::VShaderInjectGL);

         if(!findReplace(nDef, fnc, "main") && fn != "" && fnc != "")
            LOG("Warning: Function name not found in " << fn << ". Will attempt to compile main function");

         EMP_DEVICE_NOT_INIT_ASSERT(RS_GL43);
         vs = glCreateShader(GL_VERTEX_SHADER);

         auto c = nDef.c_str();
         glShaderSource(vs, 1, &c, 0);
         glCompileShader(vs);

         GLint r = GL_TRUE, l = 0;
         glGetShaderiv(vs, GL_COMPILE_STATUS, &r);
         glGetShaderiv(vs, GL_INFO_LOG_LENGTH, &l);

         if(r != GL_TRUE)
            {
            String error;
            error.resize(l + 1);
            glGetShaderInfoLog(vs, l, 0, &error.front());
            EMP_DEVICE_ERROR((String("Shader Compilation failed for ") + fn + "::" + fnc +
               " with vs compiler\n  Error Message: " + error));
            }
         else
            {
            String error;
            error.resize(l + 1);
            glGetShaderInfoLog(vs, l, 0, &error.front());
            LOG(error);
            }
         }

      void APIVertexShader<RS_GL43>::bind()
         {
         //Engine<RS_GL43>::getPtr()->_getPlatformDevice().bindVS(vs);
         }

      void APIGeometryShader<RS_GL43>::init(const String& fn, const String& fnc)
         {
         initFromDef(_loadShader(fn), fnc, fn);
         }

      void APIGeometryShader<RS_GL43>::initFromDef(const String& def, const String& fnc, const String& fn)
         {
         //replace the function's name with main
         String nDef = def;
         nDef.insert(nDef.find('\n'), Defaults::GShaderInjectGL);

         if(!findReplace(nDef, fnc, "main") && fn != "" && fnc != "")
            LOG("Warning: Function name not found in " << fn << ". Will attempt to compile main function");

         gs = glCreateShader(GL_GEOMETRY_SHADER);

         auto c = nDef.c_str();
         glShaderSource(gs, 1, &c, 0);
         glCompileShader(gs);

         GLint r = GL_TRUE, l = 0;
         glGetShaderiv(gs, GL_COMPILE_STATUS, &r);
         glGetShaderiv(gs, GL_INFO_LOG_LENGTH, &l);

         if(r != GL_TRUE)
            {
            String error;
            error.resize(l + 1);
            glGetShaderInfoLog(gs, l, 0, &error.front());
            EMP_DEVICE_ERROR((String("Shader Compilation failed for ") + fn + "::" + fnc +
               " with gs compiler\n  Error Message: " + error));
            }
         else
            {
            String error;
            error.resize(l + 1);
            glGetShaderInfoLog(gs, l, 0, &error.front());
            LOG(error);
            }
         }

      void APIGeometryShader<RS_GL43>::bind()
         {
         //Engine<RS_GL43>::getPtr()->_getPlatformDevice().bindFS(gs);
         }

      void APIFragmentShader<RS_GL43>::init(const String& fn, const String& fnc)
         {
         initFromDef(_loadShader(fn), fnc, fn);
         }

      void APIFragmentShader<RS_GL43>::initFromDef(const String& def, const String& fnc, const String& fn)
         {
         //replace the function's name with main
         String nDef = def;
         nDef.insert(nDef.find('\n'), Defaults::FShaderInjectGL);
         if(!findReplace(nDef, fnc, "main") && fn != "" && fnc != "")
            LOG("Warning: Function name not found in " << fn << ". Will attempt to compile main function");
         fs = glCreateShader(GL_FRAGMENT_SHADER);

         auto c = nDef.c_str();
         glShaderSource(fs, 1, &c, 0);
         glCompileShader(fs);

         GLint r = GL_TRUE, l = 0;
         glGetShaderiv(fs, GL_COMPILE_STATUS, &r);
         glGetShaderiv(fs, GL_INFO_LOG_LENGTH, &l);

         if(r != GL_TRUE)
            {
            String error;
            error.resize(l + 1);
            glGetShaderInfoLog(fs, l, 0, &error.front());
            EMP_DEVICE_ERROR((String("Shader Compilation failed for ") + fn + "::" + fnc +
               " with fs compiler\n  Error Message: " + error));
            }
         else
            {
            String error;
            error.resize(l + 1);
            glGetShaderInfoLog(fs, l, 0, &error.front());
            LOG(error);
            }
         }

      void APIFragmentShader<RS_GL43>::bind()
         {
         //Engine<RS_GL43>::getPtr()->_getPlatformDevice().bindFS(fs);
         }
#endif
      }
   }