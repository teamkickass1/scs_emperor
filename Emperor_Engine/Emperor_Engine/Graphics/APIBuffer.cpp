#include "APIBuffer.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      APIBuffer<RS_DX11>::~APIBuffer()
         {
         Release();
         }

      void APIBuffer<RS_DX11>::Release()
         {
         if(buffer)
            {
            buffer->Release();
            buffer = 0;
            }
         }

      void APIVertexBuffer<RS_DX11>::finalize(const iVertexFormat&, uint32 size)
         {
         //Fill Me
			 D3D11_BUFFER_DESC bd;
			 ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));

			 bd.Usage = D3D11_USAGE_DYNAMIC;//how it will be used (static vs dynamic)
			 bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;//cpu access
			 bd.ByteWidth = size;//Size of the buffer
			 bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;//type of buffer

			 EMP_DEVICE_ASSERT_DX("Create Vertex buffer", CreateBuffer(&bd, 0, &buffer));

         }

      void APIIndexBuffer<RS_DX11>::finalize(uint32 size)
         {
         //Fill Me
			 D3D11_BUFFER_DESC bd;
			 ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));

			 bd.Usage = D3D11_USAGE_DYNAMIC;//how it will be used (static vs dynamic)
			 bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;//cpu access
			 bd.ByteWidth = size;//Size of the buffer
			 bd.BindFlags = D3D11_BIND_INDEX_BUFFER;//type of buffer

			 EMP_DEVICE_ASSERT_DX("Create Index buffer", CreateBuffer(&bd, 0, &buffer));

         }

      void APIUniformBuffer<RS_DX11>::finalize(uint32 size)
         {
         //Fill Me

         //size needs to be a multiple of 16, if it is not, the size must be
         //set to the next largest multiple of 16
         //Example: size of 28 would need to be changed to 32
			 if (size % 16 != 0)
			 {
				 size = size - (size % 16) + 16;
			 }

			 D3D11_BUFFER_DESC bd;
			 ZeroMemory(&bd, sizeof(D3D11_BUFFER_DESC));

			 bd.Usage = D3D11_USAGE_DYNAMIC;//how it will be used (static vs dynamic)
			 bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;//cpu access
			 bd.ByteWidth = size;//Size of the buffer
			 bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;//type of buffer
			 
			 EMP_DEVICE_ASSERT_DX("Create Uniform buffer", CreateBuffer(&bd, 0, &buffer));

         }

      void APIBuffer<RS_DX11>::fill(const byte* b, uint32 size)
         {
         //Fill Me
			 
			 lock();
			 //write to the buffer
			 memcpy(sub.pData, b, size);
			 unlock();

         }

      void APIBuffer<RS_DX11>::lock()
         {
         //maps the buffer to attribute sub, use EMP_CONTEXT_ASSERT_DX
			 //sets the buffer to be written to
			 EMP_CONTEXT_ASSERT_DX("Locking buffer", Map(buffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &sub));
         }

      void APIBuffer<RS_DX11>::unlock()
         {
         //Fill Me
			 //unmap buffer
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->Unmap(buffer, 0);
         }

      template <class T>
      void APIBuffer<RS_DX11>::setData(uint32 i, T d)
         {
         //Fill Me
			 lock();
			 memcpy(sub.pData + i, d, sizeof(T));
			 unlock();
         }

      void APIBuffer<RS_DX11>::setData(uint32 i, void* d, uint32 s)
         {
         //Fill Me
         //Note: will need to cast to byte* and offset the pointer
			 lock();
			 memcpy((byte*)sub.pData + i, d, s);
			 unlock();
         }

      void APIVertexBuffer<RS_DX11>::bind(uint32 vertexSize) const
         {
         //Fill Me
         //Note: Use singletons to expose context
			uint32 off = 0;
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->IASetVertexBuffers(0, 1, &buffer, &vertexSize, &off);
         }

      void APIIndexBuffer<RS_DX11>::bind() const
         {
         //Fill Me
         //Note: Use singletons to expose context
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->IASetIndexBuffer(buffer, DXGI_FORMAT_R32_UINT, 0);

         }

      void APIUniformBuffer<RS_DX11>::bindToVertex(uint32 slot) const
         {
         //Fill Me
         //Note: Use singletons to expose context
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->PSSetConstantBuffers(slot, 1, &buffer);
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->VSSetConstantBuffers(slot, 1, &buffer);

         }

      void APIUniformBuffer<RS_DX11>::bindToGeometry(uint32 slot) const
         {
         //Fill Me
         //Note: Use singletons to expose context
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->PSSetConstantBuffers(slot, 1, &buffer);
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->VSSetConstantBuffers(slot, 1, &buffer);
         }

      void APIUniformBuffer<RS_DX11>::bindToFragment(uint32 slot) const
         {
         //Fill Me
         //Note: Use singletons to expose context
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->PSSetConstantBuffers(slot, 1, &buffer);
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->VSSetConstantBuffers(slot, 1, &buffer);
         }
#endif

#if EMP_USE_OPENGL
	  APIBuffer<RS_GL43>::~APIBuffer()
	  {
		  Release();
	  }

	  void APIBuffer<RS_GL43>::Release()
	  {

	  }

	  static void _initVertex(const VertexFormat<RS_GL43>& v)
	  {
		  uint32 i = 0;
		  auto a = v.getVertexDescription().stdVector();
		  for (auto j = a.begin(), end = a.end(); j != end; j++, i++)
		  {
			  uint32 size;
			  auto type = _glExtractFormat(j->format, size);
			  glVertexAttribPointer(i, size, type, GL_FALSE, v.getByteSize(), (GLvoid*)j->aligned);
		  }
	  }

	  void APIVertexBuffer<RS_GL43>::finalize(const iVertexFormat& v, uint32 size)
	  {
		  //Fill for GL Component
		  //Note: you must cast from iVertexFormat to VertexFormat
		  //Throw exception with EMP_DEVICE_ERROR
		  GLenum e;
		  bSize = size;//Size of buffer

		  glGenVertexArrays(1, &vID); //vertex buffer only
		  e = glGetError();
		  glBindVertexArray(vID); //vertex buffer only
		  e = glGetError();
		  glGenBuffers(1, &bufferID);
		  e = glGetError();
		  glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		  e = glGetError();
		  //constructs a vertex definition (vertex buffer only)
		  _initVertex(*(const VertexFormat<RS_GL43>*)&v);

		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("Vertex Buffer bind failed.");
	  }

	  //get rid of useless param
	  void APIIndexBuffer<RS_GL43>::finalize(uint32 size)
	  {
		  //Fill for GL Component
		  GLenum e;
		  bSize = size;

		  glGenBuffers(1, &bufferID);
		  e = glGetError();
		  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferID);

		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("Index Buffer bind failed.");
	  }

	  void APIUniformBuffer<RS_GL43>::finalize(uint32 size)
	  {
		  //Fill for GL Component
		  GLenum e;
		  bSize = size;

		  glGenBuffers(1, &bufferID);
		  e = glGetError();
		  glBindBuffer(GL_UNIFORM_BUFFER, bufferID);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform Buffer bind failed.");
	  }

	  void APIVertexBuffer<RS_GL43>::fill(const byte* d, uint32 i)
	  {
		  //Fill for GL Component
		  lock();
		  //write to the buffer
		  GLenum e;
		  glBufferData(GL_ARRAY_BUFFER, i, d, GL_STATIC_DRAW);
		  glBindBuffer(GL_ARRAY_BUFFER, 0);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Vertex fill failed.");
		  unlock();
	  }

	  void APIIndexBuffer<RS_GL43>::fill(const byte* d, uint32 i)
	  {
		  //Fill for GL Component
		  lock();
		  //write to the buffer
		  GLenum e;
		  glBufferData(GL_ELEMENT_ARRAY_BUFFER, i, d, GL_STATIC_DRAW);
		  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Index fill failed.");
		  unlock();
	  }

	  void APIUniformBuffer<RS_GL43>::fill(const byte* d, uint32 i)
	  {
		  //Fill for GL Component
		  lock();
		  //write to the buffer
		  GLenum e;
		  glBufferData(GL_UNIFORM_BUFFER, i, d, GL_DYNAMIC_DRAW);
		  glBindBuffer(GL_UNIFORM_BUFFER, 0);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform fill failed.");
		  unlock();
	  }

	  void APIVertexBuffer<RS_GL43>::lock()
	  {
		  //Fill for GL Component
		  //set buffer to be written
		  GLenum e;
		  glBindVertexArray(vID);
		  e = glGetError();
		  glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		  e = glGetError();

		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Vertex buffer lock failed.");
	  }

	  void APIIndexBuffer<RS_GL43>::lock()
	  {
		  //Fill for GL Component
		  GLenum e;
		  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferID);
		  e = glGetError();

		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Index buffer lock failed.");
	  }

	  void APIUniformBuffer<RS_GL43>::lock()
	  {
		  //Fill for GL Component
		  GLenum e;
		  glBindBuffer(GL_UNIFORM_BUFFER, bufferID);
		  e = glGetError();

		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform buffer lock failed.");
	  }

	  void APIVertexBuffer<RS_GL43>::unlock()
	  {
		  //Fill for GL Componet
		  if (!tBuff.empty())
			  glBufferData(GL_ARRAY_BUFFER, sizeof(byte) * tBuff.size(), &tBuff, GL_DYNAMIC_DRAW);
		  
	  }

	  void APIIndexBuffer<RS_GL43>::unlock()
	  {
		  //Fill for GL Component
		  if (!tBuff.empty())
			  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(byte) * tBuff.size(), &tBuff, GL_DYNAMIC_DRAW);
	  }

	  void APIUniformBuffer<RS_GL43>::unlock()
	  {
		  //Fill for GL Component
		  if (!tBuff.empty())
			  glBufferData(GL_UNIFORM_BUFFER, sizeof(byte) * tBuff.size(), &tBuff, GL_DYNAMIC_DRAW);
	  }

	  template <class T>
	  void APIBuffer<RS_GL43>::setData(uint32 i, T d)
	  {
		  
		  //Fill for GL Component
		  if (tBuff.at(i)._Unused_capacity() >= sizeof(d))
		  {
			  //memcpy(tBuff + i, d);
			  tBuff[i] = d;
		  }
		  else {
			  //tBuff.reserve(sizeof(d));
			  tBuff.push_back(d);
		  }
	  }

	  void APIBuffer<RS_GL43>::setData(uint32 i, void* d, uint32 s)
	  {
		  //Fill for GL Component
		  if (sizeof(tBuff.at(i)) >= s)
		  {
			  memcpy(&tBuff.at(i), &d, s);
		  }
		  else {
			  //tBuff.reserve(sizeof(s));
			  memcpy(&tBuff.back(), &d, s);
		  }
	  }

	  void APIVertexBuffer<RS_GL43>::bind(uint32 vertsize) const
	  {
		  //Fill for GL Component
		  //set vertex buffer
		  auto e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Vertex buffer failed before binding.");

		  glBindVertexArray(vID);
		  e = glGetError();
		  glBindBuffer(GL_ARRAY_BUFFER, bufferID);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Vertex buffer bind failed.");
		  //don�t forget to bind the vertex attributes...
		  for (auto i = 0; i != vertsize; i++){
			  glEnableVertexAttribArray(i);
		  }
	  }

	  void APIIndexBuffer<RS_GL43>::bind() const
	  {
		  //Fill for GL Component
		  auto e = glGetError(); //Get's error from Binding 1DTexture from fill() didn't work using the obj.
		  if (e != GL_NO_ERROR) //std::cout << "API Index buffer failed before binding." << std::endl;
			  //EMP_DEVICE_ERROR("API Index buffer failed before binding.");

		  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferID);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Index bind failed failed.");
	  }

	  void APIUniformBuffer<RS_GL43>::bindToVertex(uint32 index) const
	  {
		  //Fill for GL Component
		  auto e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform buffer bind to Vertex failed before binding.");

		  glBindBufferBase(GL_UNIFORM_BUFFER, index + 8, bufferID);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform buffer bind to vertex failed.");
	  }

	  void APIUniformBuffer<RS_GL43>::bindToGeometry(uint32 index) const
	  {
		  //Fill for GL Component
		  auto e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform buffer bind to Geometry failed before binding.");

		  glBindBufferBase(GL_UNIFORM_BUFFER, index + 8, bufferID);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform buffer bind to Geometry failed.");
	  }

	  void APIUniformBuffer<RS_GL43>::bindToFragment(uint32 index) const
	  {
		  //Fill for GL Component
		  auto e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform buffer bind to fragment failed before binding.");

		  glBindBufferBase(GL_UNIFORM_BUFFER, index + 8, bufferID);
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("API Uniform buffer bind to Fragment failed.");
	  }
#endif
   }
   }