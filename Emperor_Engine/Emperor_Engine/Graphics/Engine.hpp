#ifndef __EMP_GFX_ENGINE_HPP__
#define __EMP_GFX_ENGINE_HPP__

#include <Graphics/iEngine.hpp>
#include "SceneController.hpp"
#include "ResourceController.hpp"
#include "Device.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class Engine : public iEngine, public Singleton<Engine<RS>, true>
         {
         private:
            EMP_API friend iEngine* createEngine(RenderSystem, const LString&);
            EMP_API friend void releaseEngine(iEngine*);

            uint32 multisampleCount;
            bool fullScreen;
            String resourceFolder;

            uint32 rQuality;
            uint32 rID;

            //Controllers
            ResourceController<RS> rCont;
            SceneController<RS> sCont;

            //Devices
            Device<RS> dev;
         protected:

            Engine() : fullScreen(false), resourceFolder("./"), 
               multisampleCount(1), rQuality(0), rID(0) {}
         public:
            virtual ~Engine() {}
            // Should be called by createEngine
            void setResourceFolder(const LString& rf);
            LString getResourceFolder() const
               { return resourceFolder; }

            void initialize();
            void release();

            void activateDevice(Utility::iWindow*);
            void activateDevice(WindowID, uint32, uint32);
            void render();

            uint32& renderQuality() { return rQuality; }
            uint32& renderID() { return rID; }

            iSceneController* getSceneController() { return &sCont; }
            iResourceController* getResourceController() { return &rCont; }

            void setFullScreen(bool);
            bool isFullScreen() { return fullScreen; }

            void setVsync(uint32 i) { dev.setVsync(i); }
            uint32 getVsync() { return dev.getVsync(); }

            void setMultisampleCount(Emperor::byte count);
            Emperor::byte getMultisampleCount() const
               { return multisampleCount; }

            Device<RS>& _getPlatformDevice() { return dev; }
         };

      template <RenderSystem RS>
      void _renderFromVertex(uint32 size);

      template <RenderSystem RS>
      void _renderFromIndex(uint32 size);
      }
   }

#endif