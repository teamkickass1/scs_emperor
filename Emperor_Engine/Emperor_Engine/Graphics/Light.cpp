#include "Light.hpp"
#include "Engine.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      static int textureCounter = 0;


      template <RenderSystem RS>
      Light<RS>::Light() : type(LT_DIRECTIONAL), 
         diffuse(1.0f, 1.0f, 1.0f, 1.0f), shadowEnabled(false),
         specular(1.0f, 1.0f, 1.0f, 1.0f), 
         attenuation(Vector3(1.0f, 0.0f, 0.0f)), spot(0.0f), shadowCam(0), 
         range(1000), shadowTexture(0)
         {
         buffer.addType<Matrix<float, 4> >();//Shadow Prjoection
         buffer.addType<Vector<float, 4> >();//diffuse
         buffer.addType<Vector<float, 4> >();//specular
         buffer.addType<Vector<float, 4> >();//attenuation
         buffer.addType<Vector<float, 3> >();//spot
         buffer.addType<uint32>();//node ID
         buffer.addType<uint32>();//type

         spot = Vector3(PI / 2.0f, PI / 4.0f, 16.0f);


         shadowCam = CameraManager<RS>::getPtr()->createObject();
         shadowCam->setAspect(1.0f);
         shadowCam->setNearClipping(0.01f);
         shadowCam->setPriority(9999);
         shadowCam->setFarClipping(range);
         shadowCam->useExclusiveTarget(true);
         shadowCam->setFOV(spot.x());
         shadowCam->renderID() = EMP_SHADOW_CAM_ID;
         }

      template <RenderSystem RS>
      void Light<RS>::setLightType(LightType t)
         {
         type = t;
         if(active)
            update();
         }

      template <RenderSystem RS>
      void Light<RS>::setDiffuse(const Color& c)
         {
         diffuse = c;
         if(active)
            update();
         }

      template <RenderSystem RS>
      void Light<RS>::setSpecular(const Color& c)
         {
         specular = c;
         if(active)
            update();
         }

      template <RenderSystem RS>
      void Light<RS>::setAttenuation(const Vector<float, 3>& a)
         {
         attenuation = a;
         if(active)
            update();
         }


      template <RenderSystem RS>
      void Light<RS>::setSpot(const Vector<float, 3>& s)
         {
         spot = Vector3(cos(s.x() / 2), cos(s.y() / 2), s.z());
         //insert shadow camera code here
         if(active)
            update();
         }

      template <RenderSystem RS>
      void Light<RS>::activate()
         {
         //Fill Me
			 if (!(SceneObject::node != nullptr && SceneObject::active == false)){
				 LOG("Light Object: Light may not be attached to Scene or It's not actived.");
				 return;
			 }

			 if (SceneObject::node != nullptr && SceneObject::active == false){
				 LightManager<RS>::getPtr()->activateObject(this);
				 SceneObject::active = true;
				 update();
			 }
			 
         }

      template <RenderSystem RS>
      void Light<RS>::deactivate()
         {
         //Fill Me
			 if (SceneObject::active == true){
				 LightManager<RS>::getPtr()->deactivateObject(this);
				 SceneObject::active = false;
			 }
         }

      template <RenderSystem RS>
      void Light<RS>::destroy()
         {
         //Fill Me
         //be sure to deallocate (hint: use  this  )
			 deactivate();
			 LightManager<RS>::getPtr()->removeObject(this);
			 delete this;
         }


      template <RenderSystem RS>
      void Light<RS>::update()
         {
         auto data = new byte[buffer.getByteSize()];
         uint32 s = 0, t = 0, i = node->getIndex();

         memcpy(data + s, shadowCam->_getProjection().data, t = 
            sizeof(Matrix<float, 4>));
         s += t;
         memcpy(data + s, diffuse.data, t = sizeof(Vector<float, 4>));
         s += t;
         memcpy(data + s, specular.data, t = sizeof(Vector<float, 4>));
         s += t;
         memcpy(data + s, attenuation.data, t = sizeof(Vector<float, 4>));
         s += t;
         memcpy(data + s, spot.data, t = sizeof(Vector<float, 3>));
         s += t;
         memcpy(data + s, &i, t = sizeof(uint32));
         s += t;
         memcpy(data + s, &type, t = sizeof(LightType));

         buffer.fill(data);

         delete[] data;

         shadowCam->attachTo(node);
         }

      template <RenderSystem RS>
      void Light<RS>::setRange(float r)
         {
         range = r;
         //insert shadow camera code here
         }

      template <RenderSystem RS>
      void Light<RS>::configureShadowTexture(uint32 width, uint32 height)
         {
         //Fill Me
         }

      template <RenderSystem RS>
      void Light<RS>::enableShadows(bool b)
         {
         //Fill Me
         }

      template <RenderSystem RS>
      void Light<RS>::_bindShadow(uint32 slot)
         {
         //Fill Me
         }

#if (EMP_USE_OPENGL == ON)
      template class Light<RS_GL43>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Light<RS_DX11>;
#endif
      }
   }