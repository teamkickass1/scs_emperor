#include "Camera.hpp"
#include "Texture.hpp"
#include "Engine.hpp"
#include "Managers.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      Camera<RS>::Camera() : fov(0.9f), aspect(1.6f), nearClip(1.0f), farClip(1000.0f), target(0),
         depth(0), exclusive(false), priority(0.0), rID(0), viewPort(0u)
         {
         RasterizerStateDescription rs;
         setRasterState(rs);

         projMatrix.identity();
         _updateProjection();
         
         buffer.addType<Matrix<float, 4> >();//projection
         }

      template <RenderSystem RS>
      void Camera<RS>::_updateProjection()
         {
         //Constructs a projection matrix based on the FOV, farClip, nearClip
         // and aspect variables. For this projection, it is assumed that
         //forward is -z
		  projMatrix.identity();
		  projMatrix.data[0] = (1.0f / tanf(fov * 0.5f)) / aspect;
		  projMatrix.data[5] = 1.0f / tanf(fov * 0.5f);
		  projMatrix.data[10] = -(farClip / (farClip - nearClip));
		  projMatrix.data[11] = -1;
		  projMatrix.data[14] = nearClip * -(farClip / (farClip - nearClip));
		  projMatrix.data[15] = 0;	
         
         //Leave this code in
         if(active)
            update();
         }

      template <RenderSystem RS>
      void Camera<RS>::activate()
         {
         if(node && !active)
            {
            update();
            CameraManager<RS>::getPtr()->activateObject(this);
            CameraManager<RS>::getPtr()->orderCameras();
            active = true;
            }
         else
            LOG("Only inactive cameras attached to a node can become active...");
         }

      template <RenderSystem RS>
      void Camera<RS>::setRenderTarget(const iTexture* r)
         {
         if(!r || ((Texture<RS>*)r)->getType() == TT_RENDER_TARGET)
            target = (RenderTarget<RS>*)r;
         else
            LOG("Attempted to bind a non render target to camera's target, aborting...");
         }

      template <RenderSystem RS>
      void Camera<RS>::setDepthStencil(const iTexture* d)
         {
         if(!d || ((Texture<RS>*)d)->getType() == TT_DEPTH_STENCIL)
            depth = (DepthStencil<RS>*)d;
         else
            LOG("Attempted to bind a non depth stencil to camera's depth, aborting...");
         }

      template <RenderSystem RS>
      void Camera<RS>::deactivate()
         {
         if(active)
            {
            CameraManager<RS>::getPtr()->deactivateObject(this);
            active = false;
            }
         }

      template <RenderSystem RS>
      void Camera<RS>::setPriority(float p)
         {
         priority = p;
         if(active)
            CameraManager<RS>::getPtr()->orderCameras();
         }

      template <RenderSystem RS>
      void Camera<RS>::setFOV(float f)
         {
         fov = f;
         _updateProjection();
         }

      template <RenderSystem RS>
      void Camera<RS>::setAspect(float a)
         {
         aspect = a;
         _updateProjection();
         }

      template <RenderSystem RS>
      void Camera<RS>::setNearClipping(float n)
         {
         nearClip = n;
         _updateProjection();
         }

      template <RenderSystem RS>
      void Camera<RS>::setFarClipping(float f)
         {
         farClip = f;
         _updateProjection();
         }

      template <RenderSystem RS>
      void Camera<RS>::destroy()
         {
         deactivate();
         CameraManager<RS>::getPtr()->removeObject(this);
         delete this;
         }

      template <RenderSystem RS>
      void Camera<RS>::update()
         {
         buffer.fill((byte*)projMatrix.data);
         }

      template <RenderSystem RS>
      void Camera<RS>::setRasterState(const RasterizerStateDescription& a)
         {
         try
            {
            rend.initRasterState(a);
            }
         catch(DeviceFailureException e)
            {
            LOG(e.what());//Can't really do anything about this...
            }
         }

      template <RenderSystem RS>
      void Camera<RS>::_bindRenderer()
         {
         rend.bindRenderer();
         Engine<RS>::getPtr()->renderID() = rID;
         Engine<RS>::getPtr()->_getPlatformDevice().bindTargets(target, depth, exclusive, viewPort);
         }

#if (EMP_USE_OPENGL == ON)
      template class Camera<RS_GL43>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Camera<RS_DX11>;
#endif
      }
   }
