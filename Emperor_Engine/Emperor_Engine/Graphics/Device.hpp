#ifndef __EMP_GFX_DEVICE_HPP__
#define __EMP_GFX_DEVICE_HPP__

#include "InternalTypes.hpp"
#include "../DirectX.hpp"
#include "../OpenGL.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>	
      class VertexShader;

      template <RenderSystem RS>
      class FragmentShader;

      template <RenderSystem RS>
      class RenderTarget;

      template <RenderSystem RS>
      class DepthStencil;

      template <RenderSystem RS>
      class Device {};

#if EMP_USE_DIRECTX
      template <>
      class Device<RS_DX11>
         {
         private:
            IDXGISwapChain* swap;
            ID3D11Device* dev;
            ID3D11DeviceContext* con;

            ID3D11RenderTargetView* backbuffer;
            ID3D11DepthStencilView* depthBuffer;

            bool active;

            uint32 vsync;
         protected:
         public:
            Device() : swap(0), dev(0), con(0), active(false), vsync(0) {}
            virtual ~Device();

            void init(Utility::Window*);
            void release();

            void renderStart();
            void renderEnd();

            void setFullScreen(bool);
            void setVsync(uint32);
            uint32 getVsync() {return vsync;}

            bool isActive() { return active; }

            //May want to move this...
            void bindPrimitive(const PrimitiveTopology&);

            void draw(uint32, uint32);
            void drawIndexed(uint32, uint32, uint32);

            void bindTargets(RenderTarget<RS_DX11>*, DepthStencil<RS_DX11>*, bool,
               const Vector<uint32, 4>&);

            IDXGISwapChain* _exposeSwap() { return swap; }
            ID3D11Device* _exposeDevice() { return dev; }
            ID3D11DeviceContext* _exposeContext() { return con; }
         };
#endif

#if EMP_USE_OPENGL
      template<>
      class Device<RS_GL43>
         {
         private:
            HGLRC context;
            HDC hdc;
            HWND wh;
            GLuint prim;
            Utility::Window* window;

            GLuint dummyDepth;
            GLuint dummyColor;
            GLuint dummyFrame;

            uint32 vsync;

            bool active;
         protected:
         public:
            Device() : context(0), hdc(0), active(false), window(0), vsync(0) {}
            virtual ~Device();

            void init(Utility::Window*);
            void release();

            void renderStart();
            void renderEnd();

            void setFullScreen(bool);

            void setVsync(uint32);
            uint32 getVsync() { return vsync; }

            bool isActive() { return active; }

            //May want to move this...
            void bindPrimitive(const PrimitiveTopology&);

            void draw(uint32, uint32);
            void drawIndexed(uint32, uint32, uint32);

            void bindTargets(RenderTarget<RS_GL43>*, DepthStencil<RS_GL43>*, bool,
               const Vector<uint32, 4>&);

         };
#endif
      }
   }
#endif