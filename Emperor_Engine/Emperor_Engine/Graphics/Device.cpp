#include "Device.hpp"
#include "APITranslations.hpp"
#include "Engine.hpp"
#include "../Utilities/WindowController.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_DIRECTX
      Device<RS_DX11>::~Device()
         {
         release();
         }

      void Device<RS_DX11>::init(Utility::Window* w)
         {
         //Use EMP_INIT_ASSERT_DX to throw exceptions when API calls fail
         //Create the device and swap chain using reasonable settings (check
         //course slides for examples)

         //Create and set the backbuffer and depth buffer

         //Create and set view port (based on window size)

         //set active to true
			 //Set Up Device & Swap Chain
			 DXGI_SWAP_CHAIN_DESC d;
			 ZeroMemory(&d, sizeof(DXGI_SWAP_CHAIN_DESC));
			 d.BufferCount = 1;
			 d.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			 d.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			 d.OutputWindow = (HWND)w->getWindowID();
			 d.SampleDesc.Count = 1;
			 d.SampleDesc.Quality = 0;
			 d.Windowed = TRUE;
			 d.BufferDesc.Width = w->getSize().x();
			 d.BufferDesc.Height = w->getSize().y();
			 d.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
			 d.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
			 D3D_FEATURE_LEVEL lev = D3D_FEATURE_LEVEL_11_0;
			 EMP_INIT_ASSERT_DX("create device and swap chain. ", D3D11CreateDeviceAndSwapChain(0, D3D_DRIVER_TYPE_HARDWARE, 0, 0, &lev, 1, D3D11_SDK_VERSION, &d, &swap, &dev, 0, &con));
			 //Set Up Back Buffer
			 ID3D11Texture2D* bb;
			 EMP_INIT_ASSERT_DX("to set up back buffer. ", swap->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&bb));
			 EMP_INIT_ASSERT_DX("create render target view. ", dev->CreateRenderTargetView(bb, 0, &backbuffer));
			 bb->Release();
			 //Set Up Depth Buffer
			 ID3D11Texture2D* pDepthStencil = NULL;
			 D3D11_TEXTURE2D_DESC descDepth;
			 descDepth.Width = d.BufferDesc.Width;
			 descDepth.Height = d.BufferDesc.Height;
			 descDepth.MipLevels = descDepth.ArraySize = 1;
			 descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			 descDepth.SampleDesc.Count = 1;
			 descDepth.SampleDesc.Quality = 0;
			 descDepth.Usage = D3D11_USAGE_DEFAULT;
			 descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
			 descDepth.CPUAccessFlags = 0;
			 descDepth.MiscFlags = 0;
			 EMP_INIT_ASSERT_DX("create Texture2D. ", dev->CreateTexture2D(&descDepth, 0, &pDepthStencil));
			 D3D11_DEPTH_STENCIL_DESC dsDesc;
			 ZeroMemory(&dsDesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
			 //Depth test parameters
			 dsDesc.DepthEnable = true;
			 dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
			 dsDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;
			 //Stencil test parameters
			 dsDesc.StencilEnable = false;
			 dsDesc.StencilReadMask = dsDesc.StencilWriteMask = 0xFF;
			 //Stencil operations if pixel is front facing
			 dsDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			 dsDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
			 dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			 dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
			 //Stencil operations if pixel is back facing
			 dsDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
			 dsDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
			 dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			 dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS; 
			 //Create depth stencil state
			 ID3D11DepthStencilState * pDSState;
			 EMP_INIT_ASSERT_DX("create depth stencil state. ", dev->CreateDepthStencilState(&dsDesc, &pDSState));
			 //Bind Depth Stencil State
			 con->OMSetDepthStencilState(pDSState, 1); 
			 //Stencil View description
			 D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
			 descDSV.Format = descDepth.Format;
			 descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
			 descDSV.Texture2D.MipSlice = 0;
			 descDSV.Flags = 0;
			 //Create depth stencil view
			 EMP_INIT_ASSERT_DX("create depth stencil view. ", dev->CreateDepthStencilView(pDepthStencil, &descDSV, &depthBuffer));
			 //Bind depth stencil view
			 con->OMSetRenderTargets(1, &backbuffer, depthBuffer);
			 //Set up view port
			 D3D11_VIEWPORT vp;
			 ZeroMemory(&vp, sizeof(D3D11_VIEWPORT));
			 vp.TopLeftX = 0;
			 vp.TopLeftY = 0;
			 vp.MinDepth = 0.0f;
			 vp.MaxDepth = 1.0f;
			 vp.Width = (float)w->getSize().x();
			 vp.Height = (float)w->getSize().y();
			 con->RSSetViewports(1, &vp);
			 //Set active to true
			 active = true;
         }

      void Device<RS_DX11>::setVsync(uint32 v)
         {
         vsync = v;
         }

      void Device<RS_DX11>::renderStart()
         {
         //Fill Me
			 //Clear back & depth buffer
			 con->ClearRenderTargetView(backbuffer, D3DXCOLOR(0.0f, 0.0f, 0.0f, 1.0f));
			 con->ClearDepthStencilView(depthBuffer, D3D11_CLEAR_DEPTH, 1.0f, 0);
         }

      void Device<RS_DX11>::renderEnd()
         {
         //Fill Me
			 //Flip back buffer
			 swap->Present(0, 0);
         }

      void Device<RS_DX11>::setFullScreen(bool b)
         {
         //Fill Me
			 swap->SetFullscreenState(b, 0);
			 ShowCursor(!b);
         }

      void Device<RS_DX11>::release()
         {
         //Turns off fullscreen, releases swap chain, backbuffer, device, and
         //context
			 setFullScreen(false);
			 swap->Release();
			 backbuffer->Release();
			 depthBuffer->Release();
			 dev->Release();
			 con->Release();
         }

      void Device<RS_DX11>::draw(uint32 size, uint32 offset)
         {
         //Fill Me
			 con->Draw(size, offset);
         }

      void Device<RS_DX11>::drawIndexed(uint32 size, uint32 iOffset, uint32 vOffset)
         {
         //Fill Me
			 con->DrawIndexed(size, iOffset, vOffset);
         }

      void Device<RS_DX11>::bindTargets(RenderTarget<RS_DX11>* r, DepthStencil<RS_DX11>* d, bool exclusive, const Vector<uint32, 4>& vp)
         {
         //Fill Me
         }

      void Device<RS_DX11>::bindPrimitive(const PrimitiveTopology& p)
         {
         //Fill Me
			 con->IASetPrimitiveTopology(_dxTranPrim(p.pte));
         }
#endif

#if EMP_USE_OPENGL
      Device<RS_GL43>::~Device()
         {
         release();
         }

	  void Device<RS_GL43>::init(Utility::Window* win)
	  {
		  //Fill for GL Component
		  GLenum e;
		  window = win;
		  wh = (HWND)win->getWindowID();
		  hdc = GetDC((HWND)win->getWindowID());
		  PIXELFORMATDESCRIPTOR pfd;
		  memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
		  pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
		  pfd.nVersion = 1;
		  pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
		  pfd.iPixelType = PFD_TYPE_RGBA;
		  pfd.cColorBits = pfd.cDepthBits = 32;
		  pfd.iLayerType = PFD_MAIN_PLANE;

		  int pixFmt = ChoosePixelFormat(hdc, &pfd);
		  SetPixelFormat(hdc, pixFmt, &pfd);
		  auto tc = wglCreateContext(hdc);
		  wglMakeCurrent(hdc, tc);
		  glewInit();
		  e = glGetError();
		  if (e != GL_NO_ERROR)
			  EMP_DEVICE_ERROR("glewInit error.");
		  int attribs[] = {WGL_CONTEXT_MAJOR_VERSION_ARB, 4, WGL_CONTEXT_MINOR_VERSION_ARB, 3, WGL_CONTEXT_FLAGS_ARB, 0, 0};
		  context = wglCreateContextAttribsARB(hdc, 0, attribs);
		  wglMakeCurrent(0, 0);
		  wglDeleteContext(tc);
		  wglMakeCurrent(hdc, context);
		  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		  //Enable Depth testing
		  glEnable(GL_DEPTH_TEST);
		  glDepthRange(0, 1);
		  glDepthMask(GL_TRUE);
		  glDepthFunc(GL_LEQUAL);
		  glClearDepth(1.0);
		  //Set up the view port
		  glViewport(0, 0, win->getSize().x(), win->getSize().y());
		  active = true;
         }

      void Device<RS_GL43>::release()
         {
         //Fill for GL Component
			 setFullScreen(false);
			 wglMakeCurrent(0, 0);
			 wglDeleteContext(context);
			 ReleaseDC(wh, hdc);
         }
      void Device<RS_GL43>::setVsync(uint32 v)
         {
         //Possible Enhancement
         }

      void Device<RS_GL43>::renderStart()
         {
         //Fill for GL Component
			 glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
         }

      void Device<RS_GL43>::renderEnd()
         {
         //Fill for GL Component
			 SwapBuffers(hdc);
         }


	  void Device<RS_GL43>::setFullScreen(bool b)
	  {
		  //Fill for GL Component
		  if (b)
		  {
			  SetWindowLongPtr(wh, GWL_STYLE, WS_SYSMENU | WS_POPUP | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE);
			  SetWindowPos(wh, HWND_TOP, 0, 0, window->getSize().x(), window->getSize().y(), SWP_FRAMECHANGED);
			  DEVMODE dmScreenSettings;
			  memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
			  dmScreenSettings.dmSize = sizeof(dmScreenSettings);
			  dmScreenSettings.dmPelsWidth = window->getSize().x();
			  dmScreenSettings.dmPelsHeight = window->getSize().y();
			  dmScreenSettings.dmBitsPerPel = 32;
			  dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
			  ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
			  window->showWindow(true);
			  ShowCursor(FALSE);
		  }
		  else
		  {
			  ChangeDisplaySettings(NULL, 0);
			  SetWindowLongPtr(wh, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);
			  SetWindowPos(wh, HWND_TOP, 0, 0, window->getSize().x(), window->getSize().y(), SWP_FRAMECHANGED);
			  window->showWindow(true);
			  ShowCursor(TRUE);
		  }
	  }
      void Device<RS_GL43>::bindPrimitive(const PrimitiveTopology& a)
         {
         //Fill for GL Component
			 prim = _glTranPrim(a.pte);
         }

      void Device<RS_GL43>::draw(uint32 size, uint32 offset)
         {
         //Fill for GL Component
			 glDrawArrays(prim, offset,size);
         }

      void Device<RS_GL43>::drawIndexed(uint32 size, uint32 iOffset, uint32 vOffset)
         {
         //Fill for GL Component
			 glDrawElements(prim, size, GL_UNSIGNED_INT, (void*)iOffset);
         }

      void Device<RS_GL43>::bindTargets(RenderTarget<RS_GL43>* r, DepthStencil<RS_GL43>* d, bool exclusive, const Vector<uint32, 4>& vp)
         {
         //Fill Me
         }


      void buildTangents(ArrayList<Vector<float, 4>>& tangents,
         ArrayList<Vector<float, 4>>& binormals,
         const ArrayList<Vector<float, 4>>& positions,
         const ArrayList<uint32>& indices,
         const ArrayList<Vector<float, 4>>& normals,
         const ArrayList<Vector<float, 4>>& texCoords)
         {
         for(uint32 i = 0; i < indices.size(); i += 3)
            {
            Vector<uint32, 3> tri;
            tri.x() = indices[i];
            tri.y() = indices[i + 1];
            tri.z() = indices[i + 2];
            int i1 = tri.x();
            int i2 = tri.y();
            int i3 = tri.z();

            auto& v1 = positions[i1];
            auto& v2 = positions[i2];
            auto& v3 = positions[i3];

            auto& w1 = texCoords[i1];
            auto& w2 = texCoords[i2];
            auto& w3 = texCoords[i3];

            float x1 = v2.x() - v1.x();
            float x2 = v3.x() - v1.x();
            float y1 = v2.y() - v1.y();
            float y2 = v3.y() - v1.y();
            float z1 = v2.z() - v1.z();
            float z2 = v3.z() - v1.z();

            float s1 = w2.x() - w1.x();
            float s2 = w3.x() - w1.x();
            float t1 = w2.y() - w1.y();
            float t2 = w3.y() - w1.y();

            float den = s1 * t2 - s2 * t1;
            float r = den != 0.0f ? 1.0f / den : 0;
            auto sdir = Vector3(
               (t2 * x1 - t1 * x2) * r,
               (t2 * y1 - t1 * y2) * r,
               (t2 * z1 - t1 * z2) * r
               );
            auto tdir = Vector3(
               (s1 * x2 - s2 * x1) * r,
               (s1 * y2 - s2 * y1) * r,
               (s1 * z2 - s2 * z1) * r
               );

            auto tan1 = Vector4(sdir, crossProduct(normals[i1].xyz(), sdir).dot(tdir) < 0 ? -1.0f : 1.0f);
            auto tan2 = Vector4(sdir, crossProduct(normals[i2].xyz(), sdir).dot(tdir) < 0 ? -1.0f : 1.0f);
            auto tan3 = Vector4(sdir, crossProduct(normals[i3].xyz(), sdir).dot(tdir) < 0 ? -1.0f : 1.0f);

            tangents[i1] = tan1;
            tangents[i2] = tan2;
            tangents[i3] = tan3;

            binormals[i1] = Vector4(crossProduct(tangents[i1].xyz(), normals[i1].xyz()), tan1.w());
            binormals[i2] = Vector4(crossProduct(tangents[i2].xyz(), normals[i1].xyz()), tan2.w());
            binormals[i3] = Vector4(crossProduct(tangents[i3].xyz(), normals[i1].xyz()), tan3.w());
            }
         }
#endif
      }
   }

