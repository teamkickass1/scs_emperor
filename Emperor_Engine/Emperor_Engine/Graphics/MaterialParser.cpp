#include "MaterialParser.hpp"
#include "Managers.hpp"
#include "Engine.hpp"

namespace Emperor
   {
   namespace Graphics
      {

      void _shaderBindParse(const String& type, Jval& val, MaterialPassDescription& ps, const String& r, const Json::ValueIterator& it)
         {
         if(!val[type].isNull())
            {
            Jval cam = val[type];
            if(!cam["slot"].isNull())
               _EMP_PARSE_FIND_(cameraSlot, singleLightSlot, actorSlot, materialSlot) = cam["slot"].asInt();

            Jval ar = cam["bind"];
            bool ve = false, ge = false, fr = false;
            for(uint32 i = 0; i < ar.size(); i++)
               {
               if(ar[i].asString() == "vertex")
                  ve = true;
               else if(ar[i].asString() == "geometry")
                  ge = true;
               else if(ar[i].asString() == "fragment")
                  fr = true;
               else
                  LOG("Error parsing " << r << " camera binding for " << it.key() <<
                  "is not a valid shader type");
               }

            if(ar.size() != 0)
               {
               _EMP_PARSE_FIND_(useCamera, useSingleLight, useActor, useMaterialBuffer)
                  = _EMP_GET_MPDO_(ve, ge, fr);
               auto test = _EMP_GET_MPDO_(ve, ge, fr);
               ps.useCamera = test;
               int i = 0;
               }
            }
         }

      String _addNmsp(Jval& root, const String& name, const String& nmsp)
         {
         if(root[name].isNull())
            return name;
         else
            return nmsp + name;
         }

#define _OVERRIDE(I) (or.count(I.asString()) ? or[I.asString()] : I.asString())

      void _parsePass(const String& file, const String& nmsp, HashMap<String, MaterialPassDescription>& passes, const Json::ValueIterator& i,
         Jval& root, HashMap<String, String>& or)
         {
         //create pass
         if(!passes.count(nmsp + _OVERRIDE(i.key())))
            {
            MaterialPassDescription& ps = passes[nmsp + _OVERRIDE(i.key())];

            auto val = (*i)["pass"];

            if(!val["extend"].isNull())
               {

               if(passes.count(_OVERRIDE(val["extend"])) > 0)
                  {
                  ps = passes[_OVERRIDE(val["extend"])];
                  }
               else if(passes.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                  {
                  ps = passes[nmsp + _OVERRIDE(val["extend"])];
                  }
               else
                  {
                  if(!root[_OVERRIDE(val["extend"])].isNull())
                     {
                     Json::ValueIterator iter = std::find(root.begin(), root.end(), root[_OVERRIDE(val["extend"])]);
                     _parsePass(file, nmsp, passes, iter, root,or);

                     if(passes.count(_OVERRIDE(val["extend"])) > 0)
                        {
                        ps = passes[_OVERRIDE(val["extend"])];
                        }
                     else if(passes.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                        {
                        ps = passes[nmsp + _OVERRIDE(val["extend"])];
                        }
                     else
                        LOG("Pass extending has failed for an unknown reason in " << _OVERRIDE(i.key()) <<
                        " in file " << file);
                     }
                  else
                     LOG("Unable to find " << _OVERRIDE(val["extend"]) << " pass, "
                     << _OVERRIDE(i.key()) << " extend ignored in " << file);
                  //go find it and parse it first
                  }
               }

            if(!val["loopCount"].isNull())
               ps.loopCount = val["loopCount"].asInt();

            if(!val["loopThroughLights"].isNull())
               ps.loopThroughLights = val["loopThroughLights"].asBool();

            if(!val["loopLightType"].isNull())
               {
               ps.loopThroughOneLightType = true;
               auto tp = _OVERRIDE(val["loopLightType"]);
               if(tp == "directional")
                  ps.lightLoopType = LT_DIRECTIONAL;
               else if(tp == "point")
                  ps.lightLoopType = LT_POINT;
               else if(tp == "spot")
                  ps.lightLoopType = LT_SPOT;
               else
                  LOG("Error parsing " << file << " loopLightType for " << i.key() <<
                  "is not a valid light type");
               }

            if(!val["loopThroughShadowLights"].isNull())
               ps.loopThroughShadowLights = val["loopThroughShadowLights"].asBool();

            _shaderBindParse("camera", val, ps, file, i);
            _shaderBindParse("light", val, ps, file, i);
            _shaderBindParse("actor", val, ps, file, i);
            _shaderBindParse("material", val, ps, file, i);

            if(!val["useShadow"].isNull())
               ps.useShadowTexture = val["useShadow"].asBool();

            if(!val["textures"].isNull())
               {
               auto ar = val["textures"];

               for(uint32 i = 0; i < ar.size(); i++)
                  {
                  if(!ar[i]["file"].isNull())
                     {
                     if(i < ps.textures.size())
                        ps.textures[i] = _OVERRIDE(ar[i]["file"]);
                     else
                        ps.textures.push_back(_OVERRIDE(ar[i]["file"]));
                     }

                  if(!ar[i]["bind"].isNull())
                     {
                     bool ve = false, ge = false, fr = false;

                     auto arar = ar[i]["bind"];
                     for(uint32 j = 0; j < arar.size(); j++)
                        {
                        if(_OVERRIDE(arar[j]) == "vertex")
                           ve = true;
                        if(_OVERRIDE(arar[j]) == "geometry")
                           ge = true;
                        if(_OVERRIDE(arar[j]) == "fragment")
                           fr = true;
                        }

                     if(i < ps.textureOptions.size())
                        ps.textureOptions[i] = _EMP_GET_MPDO_(ve, ge, fr);
                     else
                        ps.textureOptions.push_back(_EMP_GET_MPDO_(ve, ge, fr));
                     }
                  }
               }

            if(!val["samplers"].isNull())
               {
               auto ar = val["samplers"];

               for(uint32 i = 0; i < ar.size(); i++)
                  {
                  if(!ar[i]["name"].isNull())
                     {
                     auto name = _addNmsp(root, _OVERRIDE(ar[i]["name"]), nmsp);
                     if(i < ps.samplers.size())
                        ps.samplers[i] = name;
                     else
                        ps.samplers.push_back(name);
                     }

                  if(!ar[i]["bind"].isNull())
                     {
                     bool ve = false, ge = false, fr = false;

                     auto arar = ar[i]["bind"];
                     for(uint32 j = 0; j < arar.size(); j++)
                        {
                        if(_OVERRIDE(arar[j]) == "vertex")
                           ve = true;
                        if(_OVERRIDE(arar[j]) == "geometry")
                           ge = true;
                        if(_OVERRIDE(arar[j]) == "fragment")
                           fr = true;
                        }

                     if(i < ps.samplerOptions.size())
                        ps.samplerOptions[i] = _EMP_GET_MPDO_(ve, ge, fr);
                     else
                        ps.samplerOptions.push_back(_EMP_GET_MPDO_(ve, ge, fr));
                     }
                  }
               }

            if(!val["blendState"].isNull())
               ps.blendState = _addNmsp(root, _OVERRIDE(val["blendState"]), nmsp);

            if(!val["vertexFilename"].isNull())
               ps.vertexFilename = _OVERRIDE(val["vertexFilename"]);

            if(!val["vertexFunction"].isNull())
               ps.vertexFunction = _OVERRIDE(val["vertexFunction"]);

            if(!val["geometryFilename"].isNull())
               ps.geometryFilename = _OVERRIDE(val["geometryFilename"]);

            if(!val["geometryFunction"].isNull())
               ps.geometryFunction = _OVERRIDE(val["geometryFunction"]);

            if(!val["fragmentFilename"].isNull())
               ps.fragmentFilename = _OVERRIDE(val["fragmentFilename"]);

            if(!val["fragmentFunction"].isNull())
               ps.fragmentFunction = _OVERRIDE(val["fragmentFunction"]);

            if(!val["vertexFormat"].isNull())
               ps.vertexFormat = _addNmsp(root, _OVERRIDE(val["vertexFormat"]), nmsp);

            }
         }

      void _parseTech(const String& file, const String& nmsp, HashMap<String, _techDesc>& techs, const Json::ValueIterator& i,
         Jval& root, HashMap<String, String>& or)
         {

         if(!techs.count(nmsp + _OVERRIDE(i.key())))
            {
            _techDesc& ps = techs[nmsp + _OVERRIDE(i.key())];

            auto val = (*i)["technique"];

            if(!val["extend"].isNull())
               {

               if(techs.count(_OVERRIDE(val["extend"])) > 0)
                  {
                  ps = techs[_OVERRIDE(val["extend"])];
                  }
               else if(techs.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                  {
                  ps = techs[nmsp + _OVERRIDE(val["extend"])];
                  }
               else
                  {
                  if(!root[_OVERRIDE(val["extend"])].isNull())
                     {
                     Json::ValueIterator iter = std::find(root.begin(), root.end(), root[_OVERRIDE(val["extend"])]);
                     _parseTech(file, nmsp, techs, iter, root,or);

                     if(techs.count(_OVERRIDE(val["extend"])) > 0)
                        {
                        ps = techs[_OVERRIDE(val["extend"])];
                        }
                     else if(techs.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                        {
                        ps = techs[nmsp + _OVERRIDE(val["extend"])];
                        }
                     else
                        LOG("Technique extending has failed for an unknown reason in " << _OVERRIDE(i.key()) <<
                        " in file " << file);
                     }
                  else
                     LOG("Unable to find " << _OVERRIDE(val["extend"]) << " technique, "
                     << _OVERRIDE(i.key()) << " extend ignored in " << file);
                  }
               }

            if(!val["id"].isNull())
               ps.id = val["id"].asInt();

            if(!val["quality"].isNull())
               ps.quality = val["quality"].asInt();

            if(!val["LOD"].isNull())
               ps.LOD = val["LOD"].asInt();

            auto ar = val["passes"];
            for(uint32 i = 0; i < ar.size(); i++)
               {
               if(i < ps.passes.size())
                  ps.passes[i] = _addNmsp(root, _OVERRIDE(ar[i]), nmsp);
               else
                  ps.passes.push_back(_addNmsp(root, _OVERRIDE(ar[i]), nmsp));
               }
            }
         }

      BlendOperation _strToBlendOp(const String& s)
         {
         if(s == "add")
            return BO_ADD;
         if(s == "max")
            return BO_MAX;
         if(s == "min")
            return BO_MIN;
         if(s == "subtract")
            return BO_SUBTRACT;
         if(s == "reverseSubtract")
            return BO_REV_SUBTRACT;
         LOG("Blend operation provided invalid, defaulting to additive blending");
         return BO_ADD;
         }

      BlendOption _strToBlend(const String& s)
         {
         if(s == "blendFactor")
            return BO_BLEND_FACTOR;
         if(s == "destinationAlpha")
            return BO_DEST_ALPHA;
         if(s == "destinationColor")
            return BO_DEST_COLOR;
         if(s == "inverseBlendFactor")
            return BO_INV_BLEND_FACTOR;
         if(s == "inverseDestinationAlpha")
            return BO_INV_DEST_ALPHA;
         if(s == "inverseDestinationColor")
            return BO_INV_DEST_COLOR;
         if(s == "inverseSource1Alpha")
            return BO_INV_SRC1_ALPHA;
         if(s == "inverseSource1Color")
            return BO_INV_SRC1_COLOR;
         if(s == "inverseSourceAlpha")
            return BO_INV_SRC_ALPHA;
         if(s == "inverseSourceColor")
            return BO_INV_SRC_COLOR;
         if(s == "one")
            return BO_ONE;
         if(s == "source1Alpha")
            return BO_SRC1_ALPHA;
         if(s == "source1Color")
            return BO_SRC1_COLOR;
         if(s == "sourceAlpha")
            return BO_SRC_ALPHA;
         if(s == "sourceAlphaSaturate")
            return BO_SRC_ALPHA_SAT;
         if(s == "sourceColor")
            return BO_SRC_COLOR;
         if(s == "zero")
            return BO_ZERO;
         LOG("Blend option provided invalid, defaulting to one factor");
         return BO_ONE;
         }

      void _parseBlend(const String& file, const String& nmsp, HashMap<String, BlendStateDescription>& blends, const Json::ValueIterator& i,
         Jval& root, HashMap<String, String>& or)
         {

         if(!blends.count(nmsp + _OVERRIDE(i.key())))
            {
            BlendStateDescription& ps = blends[nmsp + _OVERRIDE(i.key())];

            auto val = (*i)["blendState"];

            if(!val["extend"].isNull())
               {

               if(blends.count(_OVERRIDE(val["extend"])) > 0)
                  {
                  ps = blends[_OVERRIDE(val["extend"])];
                  }
               else if(blends.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                  {
                  ps = blends[nmsp + _OVERRIDE(val["extend"])];
                  }
               else
                  {
                  if(!root[_OVERRIDE(val["extend"])].isNull())
                     {
                     Json::ValueIterator iter = std::find(root.begin(), root.end(), root[_OVERRIDE(val["extend"])]);
                     _parseBlend(file, nmsp, blends, iter, root,or);

                     if(blends.count(_OVERRIDE(val["extend"])) > 0)
                        {
                        ps = blends[_OVERRIDE(val["extend"])];
                        }
                     else if(blends.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                        {
                        ps = blends[nmsp + _OVERRIDE(val["extend"])];
                        }
                     else
                        LOG("Technique extending has failed for an unknown reason in " << _OVERRIDE(i.key()) <<
                        " in file " << file);
                     }
                  else
                     LOG("Unable to find " << _OVERRIDE(val["extend"]) << " technique, "
                     << _OVERRIDE(i.key()) << " extend ignored in " << file);
                  }
               }


            if(!val["blend"].isNull())
               ps.blendEnabled = val["blend"].asBool();

            if(!val["writeMask"].isNull())
               ps.writeMask = val["writeMask"].asInt();

            if(!val["blendOperation"].isNull())
               ps.blendOp = _strToBlendOp(_OVERRIDE(val["blendOperation"]));

            if(!val["blendOperationAlpha"].isNull())
               ps.blendOpAlpha = _strToBlendOp(_OVERRIDE(val["blendOperationAlpha"]));

            if(!val["source"].isNull())
               ps.srcBlend = _strToBlend(_OVERRIDE(val["source"]));

            if(!val["destination"].isNull())
               ps.destBlend = _strToBlend(_OVERRIDE(val["destination"]));

            if(!val["sourceAlpha"].isNull())
               ps.srcBlendAlpha = _strToBlend(_OVERRIDE(val["sourceAlpha"]));

            if(!val["destinationAlpha"].isNull())
               ps.destBlendAlpha = _strToBlend(_OVERRIDE(val["destinationAlpha"]));

            }
         }

      TextureComparisonMethod _strToTcm(const String& s)
         {
         if(s == "always")
            return TC_ALWAYS;
         if(s == "equal")
            return TC_EQUAL;
         if(s == "greater")
            return TC_GREATER;
         if(s == "greaterOrEqual")
            return TC_GREATER_EQUAL;
         if(s == "less")
            return TC_LESS;
         if(s == "lessOrEqual")
            return TC_LESS_EQUAL;
         if(s == "never")
            return TC_NEVER;
         if(s == "notEqual")
            return TC_NOT_EQUAL;
         LOG("Comparison method provided invalid, defaulting to never compare");
         return TC_NEVER;
         }

      TextureAddressMode _strToTam(const String& s)
         {
         if(s == "border")
            return TA_BORDER;
         if(s == "clamp")
            return TA_CLAMP;
         if(s == "mirror")
            return TA_MIRROR;
         if(s == "mirrorOnce")
            return TA_MIRROR_ONCE;
         if(s == "wrap")
            return TA_WRAP;
         LOG("Address mode provided invalid, defaulting to clamp");
         return TA_CLAMP;
         }

      void _parseSampler(const String& file, const String& nmsp, HashMap<String, _sampDesc>& samps, const Json::ValueIterator& i,
         Jval& root, HashMap<String, String>& or)
         {

         if(!samps.count(nmsp + _OVERRIDE(i.key())))
            {
            _sampDesc& ps = samps[nmsp + _OVERRIDE(i.key())];

            auto val = (*i)["textureSampler"];

            if(!val["extend"].isNull())
               {

               if(samps.count(_OVERRIDE(val["extend"])) > 0)
                  {
                  ps = samps[_OVERRIDE(val["extend"])];
                  }
               else if(samps.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                  {
                  ps = samps[nmsp + _OVERRIDE(val["extend"])];
                  }
               else
                  {
                  if(!root[_OVERRIDE(val["extend"])].isNull())
                     {
                     Json::ValueIterator iter = std::find(root.begin(), root.end(), root[_OVERRIDE(val["extend"])]);
                     _parseSampler(file, nmsp, samps, iter, root, or);

                     if(samps.count(_OVERRIDE(val["extend"])) > 0)
                        {
                        ps = samps[_OVERRIDE(val["extend"])];
                        }
                     else if(samps.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                        {
                        ps = samps[nmsp + _OVERRIDE(val["extend"])];
                        }
                     else
                        LOG("Technique extending has failed for an unknown reason in " << _OVERRIDE(i.key()) <<
                        " in file " << file);
                     }
                  else
                     LOG("Unable to find " << _OVERRIDE(val["extend"]) << " technique, "
                     << _OVERRIDE(i.key()) << " extend ignored in " << file);
                  }
               }

            if(!val["filter"].isNull())
               {
               auto fil = val["filter"];

               if(!fil["linearMin"].isNull())
                  ps.filter.linearMin = fil["linearMin"].asBool();

               if(!fil["linearMag"].isNull())
                  ps.filter.linearMag = fil["linearMag"].asBool();

               if(!fil["linearMip"].isNull())
                  ps.filter.linearMip = fil["linearMip"].asBool();

               if(!fil["anisotraphic"].isNull())
                  ps.filter.anisotropic = fil["anisotraphic"].asBool();

               if(!fil["comparison"].isNull())
                  ps.filter.comparison = fil["comparison"].asBool();
               }


            if(!val["borderColor"].isNull())
               {
               auto clr = val["borderColor"];

               if(clr.size() == 4)
                  {
                  ps.bc.r() = clr[0].asFloat();
                  ps.bc.g() = clr[1].asFloat();
                  ps.bc.b() = clr[2].asFloat();
                  ps.bc.a() = clr[3].asFloat();
                  }
               else
                  LOG("Border color in " << _OVERRIDE(i.key()) << " in file "
                  << file << " is not a properly constructed color, aborting...");
               }

            if(!val["comparisonMethod"].isNull())
               ps.cm = _strToTcm(_OVERRIDE(val["comparisonMethod"]));

            if(!val["maxAnisotropy"].isNull())
               ps.maxAni = val["maxAnisotropy"].asUInt();

            if(!val["maxLOD"].isNull())
               ps.maxLOD = val["maxLOD"].asFloat();

            if(!val["minLOD"].isNull())
               ps.minLOD = val["minLOD"].asFloat();

            if(!val["mipLODBias"].isNull())
               ps.mipBias = val["mipLODBias"].asFloat();

            if(!val["addressMode"].isNull())
               {
               auto ar = val["addressMode"];

               if(!ar[0].isNull())
                  ps.u = _strToTam(_OVERRIDE(ar[0]));
               if(!ar[1].isNull())
                  ps.v = _strToTam(_OVERRIDE(ar[1]));
               if(!ar[2].isNull())
                  ps.w = _strToTam(_OVERRIDE(ar[2]));
               }
            }
         }

      void _parseFormat(const String& file, const String& nmsp, HashMap<String, _vertFormat>& fmts, const Json::ValueIterator& i,
         Jval& root, HashMap<String,String>& or)
         {

         if(!fmts.count(nmsp + _OVERRIDE(i.key())))
            {
            _vertFormat& ps = fmts[nmsp + _OVERRIDE(i.key())];

            auto val = (*i)["vertexFormat"];

            if(!val["extend"].isNull())
               {

               if(fmts.count(_OVERRIDE(val["extend"])) > 0)
                  {
                  ps = fmts[_OVERRIDE(val["extend"])];
                  }
               else if(fmts.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                  {
                  ps = fmts[nmsp + _OVERRIDE(val["extend"])];
                  }
               else
                  {
                  if(!root[_OVERRIDE(val["extend"])].isNull())
                     {
                     Json::ValueIterator iter = std::find(root.begin(), root.end(), root[_OVERRIDE(val["extend"])]);
                     _parseFormat(file, nmsp, fmts, iter, root,or);

                     if(fmts.count(_OVERRIDE(val["extend"])) > 0)
                        {
                        ps = fmts[_OVERRIDE(val["extend"])];
                        }
                     else if(fmts.count(nmsp + _OVERRIDE(val["extend"])) > 0)
                        {
                        ps = fmts[nmsp + _OVERRIDE(val["extend"])];
                        }
                     else
                        LOG("Vertex format extending has failed for an unknown reason in " << _OVERRIDE(i.key()) <<
                        " in file " << file);
                     }
                  else
                     LOG("Unable to find " << _OVERRIDE(val["extend"]) << " vertex format, "
                     << _OVERRIDE(i.key()) << " extend ignored in " << file);
                  }
               }

            if(!val["types"].isNull())
               {
               auto typ = val["types"];

               for(uint32 i = 0; i < typ.size(); i++)
                  {
                  if(i < ps.types.size())
                     ps.types[i] = _OVERRIDE(typ[i]);
                  else
                     ps.types.push_back(_OVERRIDE(typ[i]));
                  }

               }

            if(!val["symantics"].isNull())
               {
               auto sym = val["symantics"];

               for(uint32 i = 0; i < sym.size(); i++)
                  {
                  if(i < ps.syms.size())
                     ps.syms[i] = _OVERRIDE(sym[i]);
                  else
                     ps.syms.push_back(_OVERRIDE(sym[i]));
                  }

               }

            }
         }

      void _parseMaterialFile(const String& file, const String& path, _matDesc& mat, HashMap<String, MaterialPassDescription>& passes, HashMap<String, _techDesc>& techs,
         HashMap<String, BlendStateDescription>& blends, HashMap<String, _sampDesc>& samps, HashMap<String, _vertFormat>& fmts, HashMap<String, String>& or)
         {
         iFile* f = File::create();
         f->open(path + file, ios::in, false);
         if(f->isLoaded())
            {
            String def;
            while(f->good())
               {
               def.push_back(f->get());
               }
            def.pop_back();
            _parseMaterialDef(file, def, path, mat, passes, techs, blends, samps, fmts,or);
            }
         else
            {
            delete f;
            EMP_RESOURCE_ERROR(String("Failed to open ") + file + " for parsing, aborting...");
            }
         delete f;
         }

      void _parseMaterialDef(const String& file, const String& def, const String& path, _matDesc& mat, HashMap<String, MaterialPassDescription>& passes, HashMap<String, _techDesc>& techs,
         HashMap<String, BlendStateDescription>& blends, HashMap<String, _sampDesc>& samps, HashMap<String, _vertFormat>& fmts, HashMap<String, String>& or)
         {
         Json::Value rt;
         Json::Reader read;
         if(read.parse(def, rt, false))
            {
            Jval overRide = rt["override"];

            for(Json::ValueIterator i = overRide.begin(), end = overRide.end(); i != end; i++)
               {
               if(!or.count(i.key().asString()))
                  or[i.key().asString()] = (*i).asString();
               }


            Jval directives = rt["directives"];

            String emtlNameSpace;
            HashMap<String, _matDesc> importMats;

            //parse and store directives
            if(!directives.isNull())
               {
               Jval nmsp = directives["namespace"];
               if(!nmsp.isNull())
                  emtlNameSpace = nmsp.asString() + "::";

               Jval imp = directives["imports"];


               for(uint32 i = 0; i < imp.size(); i++)
                  {
                  if(!importMats.count(imp[i].asString()))
                     {
                     _parseMaterialFile(imp[i].asString(), path, importMats[imp[i].asString()],
                        passes, techs, blends, samps, fmts, or);
                     }
                  }
               }




            Jval material = rt["material"];
            mat.nmsp = emtlNameSpace;

            if(!material.isNull())
               {

               if(!material["extend"].isNull())
                  {
                  mat = importMats[material["extend"].asString()];
                  }
               auto tec = material["techniques"];

               for(uint32 i = 0; i < tec.size(); i++)
                  {
                  if(i < mat.techs.size())
                     mat.techs[i] = _addNmsp(rt, tec[i].asString(), emtlNameSpace);
                  else
                     mat.techs.push_back(_addNmsp(rt, tec[i].asString(), emtlNameSpace));
                  }

               auto values = material["values"];

               for(uint32 i = 0; i < values.size(); i++)
                  {
                  if(!values[i]["type"].isNull() &&
                     !values[i]["values"].isNull())
                     {
                     if(i < mat.types.size())
                        {
                        mat.types[i] = values[i]["type"].asString();

                        mat.vals[i].clear();
                        for(uint32 j = 0; j < values[i]["values"].size(); j++)
                           mat.vals[i].push_back(values[i]["values"][j].asFloat());
                        }
                     else
                        {
                        mat.types.push_back(values[i]["type"].asString());

                        mat.vals.push_back(ArrayList<float>());
                        for(uint32 j = 0; j < values[i]["values"].size(); j++)
                           mat.vals[i].push_back(values[i]["values"][j].asFloat());
                        }
                     }
                  }
               }

            //loop through all elements
            for(Json::ValueIterator i = rt.begin(), end = rt.end(); i != end; i++)
               {
               //skip directives if found
               if(i.key() == "emtl directives" || i.key() == "material")
                  continue;

               if(!(*i)["pass"].isNull())
                  {
                  _parsePass(file, emtlNameSpace, passes, i, rt,or);
                  }
               if(!(*i)["technique"].isNull())
                  {
                  _parseTech(file, emtlNameSpace, techs, i, rt,or);
                  }
               if(!(*i)["blendState"].isNull())
                  {
                  _parseBlend(file, emtlNameSpace, blends, i, rt,or);
                  }
               if(!(*i)["textureSampler"].isNull())
                  {
                  _parseSampler(file, emtlNameSpace, samps, i, rt,or);
                  }
               if(!(*i)["vertexFormat"].isNull())
                  {
                  _parseFormat(file, emtlNameSpace, fmts, i, rt,or);
                  }

               }
            }
         else
            LOG("Failed to parse material " << file << ", Error Message: " << read.getFormattedErrorMessages());
         }

      template <RenderSystem RS>
      Material<RS>* _buildMaterial(const String& r, Material<RS>* out, const String& def)
         {
         HashMap<String, MaterialPassDescription> passes;
         HashMap<String, _techDesc> techs;
         HashMap<String, BlendStateDescription> blends;
         HashMap<String, _sampDesc> samps;
         HashMap<String, _vertFormat> fmts;
         HashMap<String, String> or;
         _matDesc mat;

         try
            {
            if(def == "")
               _parseMaterialFile(r + ".emtl", Engine<RS>::getPtr()->getResourceFolder().stdString(), mat, passes, techs, blends, samps, fmts, or);
            else
               _parseMaterialDef(r, def, Engine<RS>::getPtr()->getResourceFolder().stdString(), mat, passes, techs, blends, samps, fmts, or);
            }
         catch(std::exception& er)
            {
            EMP_RESOURCE_ERROR(String("Error occured while parsing material: ") + er.what());
            }

         //create vertex formats
         for(auto i = fmts.begin(), end = fmts.end(); i != end; i++)
            {
            try
               {
               VertexFormat<RS>* vf = VertexFormatManager<RS>::getPtr()->newResource(i->first);
               auto& fd = i->second;

               for(auto j = fd.types.begin(), end = fd.types.end(); j != end; j++)
                  {
                  if(*j == "Vector4")
                     vf->addVector4(fd.syms[j - fd.types.begin()]);
                  else if(*j == "Vector3")
                     vf->addVector3(fd.syms[j - fd.types.begin()]);
                  else if(*j == "Vector2")
                     vf->addVector2(fd.syms[j - fd.types.begin()]);
                  else if(*j == "Float")
                     vf->addFloat(fd.syms[j - fd.types.begin()]);
                  else if(*j == "Uint")
                     vf->addUint(fd.syms[j - fd.types.begin()]);
                  else if(*j == "Int")
                     vf->addInt(fd.syms[j - fd.types.begin()]);
                  else
                     LOG("Invalid vertex format type provided in " <<
                     i->first << " in file " << r << ". Ignoring type...");
                  }
               }
            catch(ResourceLoadFailException&)
               {/*do nothing, likely a merge artifact*/ }
            }

         //create texture Samplers
         for(auto i = samps.begin(), end = samps.end(); i != end; i++)
            {
            try
               {
               TextureSampler<RS>* ts = SamplerManager<RS>::getPtr()->newResource(i->first);
               auto& fd = i->second;

               ts->setAddressModeU(fd.u);
               ts->setAddressModeV(fd.v);
               ts->setAddressModeW(fd.w);
               ts->setBorderColor(fd.bc);
               ts->setComparisonMethod(fd.cm);
               ts->setMaxAnisotropy(fd.maxAni);
               ts->setMaxLOD(fd.maxLOD);
               ts->setMinLOD(fd.minLOD);
               ts->setMipLODBias(fd.mipBias);
               ts->setFilter(fd.filter);

               ts->build();
               }
            catch(ResourceLoadFailException&)
               {/*do nothing, likely a merge artifact*/ }
            }

         //create blend states
         for(auto i = blends.begin(), end = blends.end(); i != end; i++)
            {
            try
               {
               BlendState<RS>* bs = BlendStateManager<RS>::getPtr()->newResource(i->first);

               bs->setAlphaToCoverage(false);
               bs->setIndependantBlend(false);
               bs->setDescription(i->second);

               bs->finalize();
               }
            catch(ResourceLoadFailException&)
               {/*do nothing, likely a merge artifact*/ }
            }

         try
            {
            if(!out)
               out = MaterialManager<RS>::getPtr()->newResource(r);

            for(auto i = mat.types.begin(), end = mat.types.end(); i != end; i++)
               {
               uint32 count = i - mat.types.begin();

               if(!mat.vals.size())
                  {
                  LOG("Value array for material buffer was empty, skipping element...");
                  continue;
                  }
               if(*i == "Vector4")
                  {
                  out->addSize(sizeof(Vector<float, 4>));
                  out->setValue(count, &mat.vals[count].front());
                  }
               else if(*i == "Vector3")
                  {
                  out->addSize(sizeof(Vector<float, 3>));
                  out->setValue(count, &mat.vals[count].front());
                  }
               else if(*i == "Vector2")
                  {

                  out->addSize(sizeof(Vector<float, 2>));
                  out->setValue(count, &mat.vals[count].front());
                  }
               else if(*i == "Float")
                  {
                  out->addSize(sizeof(float));
                  out->setValue(count, &mat.vals[count].front());

                  }
               else if(*i == "Uint")
                  {
                  out->addSize(sizeof(uint32));
                  uint32 val = (uint32)mat.vals[count].front();
                  out->setValue(i - mat.types.begin(), &val);
                  }
               else if(*i == "Int")
                  {
                  out->addSize(sizeof(int));
                  int val = (int)mat.vals[count].front();
                  out->setValue(i - mat.types.begin(), &val);
                  }
               else
                  {
                  LOG("Invalid type specified for material buffer, ignoring element...");
                  }
               }

            HashMap<String, uint32> passids;

            for(auto i = mat.techs.begin(), end = mat.techs.end(); i != end; i++)
               {
               if(!techs.count(*i))
                  {
                  LOG("Technique in material was not found, ignoring technique...");
                  continue;
                  }

               auto& tdef = techs[*i];
               auto techid = out->createTechnique(tdef.id, tdef.quality, tdef.LOD);
               for(auto j = tdef.passes.begin(), end = tdef.passes.end(); j != end; j++)
                  {
                  if(!passes.count(*j))
                     LOG("Pass in technique was not found, ignoring pass...");
                  else
                     {
                     if(!passids.count(*j))
                        passids[*j] = out->createPass(passes[*j]);
                     out->addPassToTechnique(techid, passids[*j]);
                     }
                  }
               }
            }
         catch(ResourceLoadFailException& er)
            {
            LOG("Failed to create material: " << er.what());
            //out = MaterialManager<RS>::getPtr()->getResource(r);
            }

         return out;
         }

#if (EMP_USE_OPENGL == ON)
      template Material<RS_GL43>* _buildMaterial(const String& r, Material<RS_GL43>*, const String& def);
#endif

#if (EMP_USE_DIRECTX == ON)
      template Material<RS_DX11>* _buildMaterial(const String& r, Material<RS_DX11>*, const String& def);
#endif
      }
   }