#ifndef __EMP_GFX_CAMERA_HPP__
#define __EMP_GFX_CAMERA_HPP__

#include <Graphics/iCamera.hpp>
#include "SceneObject.hpp"
#include "RasterizerState.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS> class RenderTarget;
      template <RenderSystem RS> class DepthStencil;

      template <RenderSystem RS>
      class Camera : public SceneObject<RS, iCamera>
         {
         private:
            Matrix<float, 4> projMatrix;

            float fov;
            float aspect;
            float nearClip;
            float farClip;
            float priority;
            bool exclusive;

            RasterizerState<RS> rend;
            RenderTarget<RS>* target;
            DepthStencil<RS>* depth;

            uint32 rID;
            Vector<uint32, 4> viewPort;

            void _updateProjection();
         protected:
            virtual ~Camera() {}

            void update();
         public:
            Camera();

            void destroy();

            void setFOV(float);
            float getFOV() const { return fov; }

            void setAspect(float);
            float getAspect() const { return aspect; }

            void setPriority(float);
            float getPriority() const { return priority; }

            void setNearClipping(float);
            float getNearClipping() const { return nearClip; }

            void setFarClipping(float);
            float getFarClipping() const { return farClip; }

            void setViewPort(uint32 left, uint32 top, uint32 width, uint32 height)
               {
               viewPort.x() = left;
               viewPort.y() = top;
               viewPort.z() = width;
               viewPort.w() = height;
               }

            void setViewPort(const Vector<uint32, 4>& vp) { setViewPort(vp.x(), vp.y(), vp.z(), vp.w()); }
            Vector<uint32, 4> getViewPort() const { return viewPort; }

            Matrix<float, 4> _getProjection() { return projMatrix; }

            void setRenderTarget(const iTexture*);
            void setDepthStencil(const iTexture*);

            void setRasterState(const RasterizerStateDescription&);

            void activate();
            void deactivate();

            void useExclusiveTarget(bool a) { exclusive = a; }

            uint32& renderID() { return rID; }

            void _bindRenderer();
         };
      }
   }

#endif