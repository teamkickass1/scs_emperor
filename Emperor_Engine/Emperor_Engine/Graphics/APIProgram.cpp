#include "APIProgram.hpp"

#include "InternalTypes.hpp"

namespace Emperor
   {
   namespace Graphics
      {
#if EMP_USE_OPENGL
      void APIProgram<RS_GL43>::init(uint32 vertexShader, uint32 geometryShader, uint32 fragmentShader)
         {
         prg = glCreateProgram();
         glAttachShader(prg, vertexShader);
         if(geometryShader)
            glAttachShader(prg, geometryShader);
         glAttachShader(prg, fragmentShader);
         glLinkProgram(prg);
         /*
         GLint ls;
         glGetProgramiv(prg, GL_LINK_STATUS, &ls);*/

         auto er = glGetError();
         if(er != GL_NO_ERROR)// || !ls)
            EMP_DEVICE_ERROR(String("Failed to link program!\nError code: "));
         }

      void APIProgram<RS_GL43>::bind() const
         {
         glUseProgram(prg);
         }
#endif
      }
   }