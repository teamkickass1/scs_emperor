#include "Engine.hpp"
#include "ResourceController.hpp"
#include <Utilities\Settings.hpp>
#include "../AIPath/SolverManager.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      void Engine<RS>::initialize()
         {
         sCont.nodMan.init();
         sCont.lgtMan.init();
         rCont.vfmMan.init();
         rCont.shdMan.init();
         rCont.texMan.init();
         rCont.mshMan.init();
         rCont.matMan.init();
		 SolverManager<>* n = new SolverManager<>();
         }

      template <RenderSystem RS>
      void Engine<RS>::release()
         {

         }


      template <RenderSystem RS>
      void Engine<RS>::render()
         {
         rCont.texMan.tickResources();
         rCont.matMan.tickResources();
         rCont.mshMan.tickResources();

		 //Note: This was giving a compile error, is it right?
		 Emperor::SolverManager<>::getPtr()->tickSolver();

		 //this should be moved out of the graphics engine
         Emperor::NodeManager::getPtr()->updateValues();

         //being rendering
         if(dev.isActive())
            {
            dev.renderStart();

            //render for each active camera
            sCont.camMan.prepareLoop();
            while(sCont.camMan.bindNext())
               {
               sCont.nodMan._updateBuffers();
               sCont.actMan.prepareLoop();

               while(sCont.actMan.bindNext())
                  {
                  //potentially pointless, will need a light cap to make use of this
                  sCont.lgtMan.orderLights(
                     getPosition(
                     NO_VIRT(Node<>, sCont.actMan.getCurrent()->getNode(), _getAbs)()
                     )
                     );
                  sCont.actMan.render();
                  }
               }
            dev.renderEnd();
            }
         }

      template <RenderSystem RS>
      void Engine<RS>::activateDevice(Utility::iWindow* win)
         {
         if(dev.isActive())
            dev.release();
         dev.init((Utility::Window*)win);
         }

      template <RenderSystem RS>
      void Engine<RS>::activateDevice(WindowID id, uint32 width, uint32 height)
         {
         LOG("Unimplemented function, prepare for impact");
         }

      template <RenderSystem RS>
      void Engine<RS>::setFullScreen(bool b)
         {
         if(b != fullScreen)
            {
            dev.setFullScreen(b);
            fullScreen = b;
            }
         }

      template <RenderSystem RS>
      void Engine<RS>::setMultisampleCount(Emperor::byte count)
         {
         multisampleCount = Utility::Settings::getValidMultisampleCount(count);
         }

      template <RenderSystem RS>
      void Engine<RS>::setResourceFolder(const LString& rf)
         {
         resourceFolder = rf.stdString();
         if(!resourceFolder.size())
            resourceFolder = "./";
         if(resourceFolder.back() != '\\' && resourceFolder.back() != '/')
            resourceFolder += '/';
         }


      EMP_API iEngine* createEngine(RenderSystem t, 
         const LString& resourceFolder)
         {
         if(t == RS_DX11)
            {
#if EMP_USE_DIRECTX
            auto* engine = new Engine<RS_DX11>();
            engine->setResourceFolder(resourceFolder);
            return engine;
#else
            return nullptr;
#endif
            }
         else
            {
#if EMP_USE_OPENGL
            auto* engine = new Engine<RS_GL43>();
            engine->setResourceFolder(resourceFolder);
            return engine;
#else
            return nullptr;
#endif
            }
         }

      EMP_API void releaseEngine(iEngine* a)
         {
         delete a;
         }

      template <RenderSystem RS>
      void _renderFromVertex(uint32 size)
         {
         Engine<RS>::getPtr()->_getPlatformDevice().draw(size, 0);
         }

      template <RenderSystem RS>
      void _renderFromIndex(uint32 size)
         {
         Engine<RS>::getPtr()->_getPlatformDevice().drawIndexed(size, 0, 0);
         }

#if (EMP_USE_OPENGL == ON)
      template class Engine<RS_GL43>;
      template void _renderFromVertex<RS_GL43>(uint32);
      template void _renderFromIndex<RS_GL43>(uint32);
#endif

#if (EMP_USE_DIRECTX == ON)
      template class Engine<RS_DX11>;
      template void _renderFromVertex<RS_DX11>(uint32);
      template void _renderFromIndex<RS_DX11>(uint32);
#endif
      }
   }