#ifndef __EMP_GFX_BUFFERS_HPP__
#define __EMP_GFX_BUFFERS_HPP__

#include "InternalTypes.hpp"
#include "VertexFormat.hpp"
#include "APIBuffer.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class Buffer
         {
         private:
         protected:
            APIBuffer<RS>* buffer;
            ArrayList<uint32> dynamIndex;
            uint32 byteSize;
            uint32 indexSize;
            uint32 padding;

            ArrayList<byte> content;

            bool finalized;
            virtual void finalize() = 0;
            void releaseBuffer() { buffer->Release(); }

            Buffer(APIBuffer<RS>* b) : byteSize(0), indexSize(0), padding(0),
               finalized(false), buffer(b) {}
         public:
            virtual ~Buffer();

            template <class T>
            void addType();

            template <class T>
            void addType(T);

            void addSize(uint32);

            void fill(const byte*);
            void fill(const byte*, uint32);

            template <class T>
            void setData(uint32 index, T);

            void setData(uint32 index, void* data);

            uint32 getByteSize() const { return byteSize; }
            uint32 getIndexSize() const { return indexSize; }
         };

      template <RenderSystem RS>
      class VertexBuffer : public Buffer<RS>
         {
         private:
            const VertexFormat<RS>* vertDef;
         protected:
            void finalize();
         public:
            VertexBuffer() : Buffer(new APIVertexBuffer<RS>()) {}

            void setVertexDef(const iVertexFormat& v) { vertDef = (const VertexFormat<RS>*)&v; }
            void bindVertex() const;
         };

      template <RenderSystem RS>
      class IndexBuffer : public Buffer<RS>
         {
         private:
         protected:
            Buffer<RS>* vBuff;
            void finalize();
         public:
            IndexBuffer() : Buffer(new APIIndexBuffer<RS>()), vBuff(0) {}
            void bindVector(VertexBuffer<RS>* a) { vBuff = a; }

            void bindIndex() const;
         };

      template <RenderSystem RS>
      class UniformBuffer : public Buffer<RS>
         {
         private:
         protected:
            void finalize();
         public:
            UniformBuffer() : Buffer(new APIUniformBuffer<RS>()) {}

            void bindToVertex(uint32 slot) const;
            void bindToGeometry(uint32 slot) const;
            void bindToFragment(uint32 slot) const;
         };

      template <RenderSystem RS>
      template <class T>
      void Buffer<RS>::addType()
         {
         addSize(sizeof(T));
         }

      template <RenderSystem RS>
      template <class T>
      void Buffer<RS>::addType(T d)
         {
         addSize(sizeof(T));
         }

      template <RenderSystem RS>
      template<class T>
      void Buffer<RS>::setData(uint32 index, T d)
         {
         if(!finalized)
            finalize();
         if(finalized)
            {
            for(uint32 i = 0; i < sizeof(T); i++)
               content[dynamIndex[index] + i] = ((byte*)&d)[i];
            buffer->fill(&content.front(), byteSize);
            }
         else
            LOG("Failed to finalize buffer");
         }

      template <RenderSystem RS>
      void Buffer<RS>::setData(uint32 index, void* d)
         {
         if(!finalized)
            finalize();
         if(finalized)
            {
            uint32 size = 0;
            if(dynamIndex.size() != index + 1)
               size = dynamIndex[index + 1] - dynamIndex[index];
            else
               size = byteSize - dynamIndex[index];
            for(uint32 i = 0; i < size; i++)
               content[dynamIndex[index] + i] = ((byte*)d)[i];
            buffer->fill(&content.front(), byteSize);
            }
         else
            LOG("Failed to finalize buffer");
         }
      }
   }
#endif
