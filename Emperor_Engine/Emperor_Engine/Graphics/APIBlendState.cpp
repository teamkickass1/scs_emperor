#include "APIBlendState.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
{
	namespace Graphics
	{
#if EMP_USE_DIRECTX
		APIBlendState<RS_DX11>::~APIBlendState()
		{
			if (bs)
				bs->Release();
		}


		void APIBlendState<RS_DX11>::bindBlendState() const
		{
			//Fill Me
			//Note: use singleton to expose context
			//blend state, blend factor, sample mask
			Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->OMSetBlendState(bs, Vector4(0, 0, 0, 0).data, 0xffffffff);

		}


		void APIBlendState<RS_DX11>::finalize(const HashMap<uint32, BlendStateDescription>& dList,
			bool independantBlend, bool alphaToCoverage)
		{
			//Iterate over (dList) and set the variables for each associated
			//render target.
			//Note: many of the variables require translation, check
			//APITranslations.hpp for a list of translation functions
			//Use EMP_DEVICE_ASSERT_DX when creating the blend state
			D3D11_BLEND_DESC bsd;
			ZeroMemory(&bsd, sizeof(D3D11_BLEND_DESC));

			int rt = 0;
			for (auto itr = dList.begin(); itr != dList.end(); itr++)
			{
				bsd.RenderTarget[rt].BlendEnable = itr->second.blendEnabled;//true or false
				bsd.RenderTarget[rt].BlendOp = _dxTranOp(itr->second.blendOp);//Blend operation
				bsd.RenderTarget[rt].BlendOpAlpha = _dxTranOp(itr->second.blendOpAlpha);//Blend operation
				bsd.RenderTarget[rt].DestBlend = _dxTranBl(itr->second.destBlend);//Destination color
				bsd.RenderTarget[rt].DestBlendAlpha = _dxTranBl(itr->second.destBlendAlpha);//Dest alpha
				bsd.RenderTarget[rt].RenderTargetWriteMask = 0x0f;
				bsd.RenderTarget[rt].SrcBlend = _dxTranBl(itr->second.srcBlend);//Source Color
				bsd.RenderTarget[rt].SrcBlendAlpha = _dxTranBl(itr->second.srcBlendAlpha);//Source Alpha
				rt++;
			}

			bsd.IndependentBlendEnable = independantBlend;//true or false
			bsd.AlphaToCoverageEnable = alphaToCoverage;//true or false

			EMP_DEVICE_ASSERT_DX("Creating API Blend State", CreateBlendState(&bsd, &bs));

		}
#endif

#if EMP_USE_OPENGL
		void APIBlendState<RS_GL43>::bindBlendState() const
		{
			//Iterate over (states), enabling each state (by index) and
			//setting the equation and function for each state
			for (auto t = states.begin(); t != states.end(); t++)
			{
				t->enableFunc(t->index);
				glBlendEquationSeparatei(t->index, t->blendOp, t->blendOpAlpha);
				glBlendFuncSeparatei(in, t->srcBlend, t->destBlend, t->srcBlendAlpha, t->destBlendAlpha);
			}
		}



		inline void _glEnProxi(GLuint i)
		{
			glEnablei(GL_BLEND, i);
		}

		inline void _glDsProxi(GLuint i)
		{
			glDisablei(GL_BLEND, i);
		}

		void APIBlendState<RS_GL43>::finalize(const HashMap<uint32, BlendStateDescription>& dList,
			bool independantBlend, bool alphaToCoverage)
		{
			//Iterate over (dList), creating a _glBlend object and setting each
			//attribute to the associated value in (dList).
			//Note: many of the variables require translation, check
			//APITranslations.hpp for a list of translation functions
			//Enable function should be set to either _glEnProxi or 
			//_glDsProxi
			for (auto i : dList)
			{
				_glBlend t;

				t.index = i.first; //The render target
				t.enableFunc = i.second.blendEnabled ? _glEnProxi : _glDsProxi;//proxy function (Emperor code)
				t.blendOp = _glTranOp(i.second.blendOp);//Blend operation
				t.blendOpAlpha = _glTranOp(i.second.blendOpAlpha);//Blend operation
				t.destBlend = _glTranBl(i.second.destBlend);//Destination Color
				t.destBlendAlpha = _glTranBl(i.second.destBlendAlpha);//Destination Alpha
				t.srcBlend = _glTranBl(i.second.srcBlend);//Source Color
				t.srcBlendAlpha = _glTranBl(i.second.srcBlendAlpha);//Source Alpha

				states.push_back(t);
			}

		}
#endif
	}
}