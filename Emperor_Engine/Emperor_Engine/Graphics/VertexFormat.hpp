#ifndef __EMP_GFX_VERTEX_FORMAT_HPP__
#define __EMP_GFX_VERTEX_FORMAT_HPP__

#include <Graphics/iVertexFormat.hpp>
#include "InternalTypes.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class VertexFormat : public iVertexFormat
         {
         private:
         protected:
            ArrayList<InputElement> desc;
            HashMap<String, uint32> semanticCounter;

            ArrayList<uint32> dynamIndex;
            uint32 byteSize;
            uint32 indexSize;

            void _addSize(uint32 size);
            uint32 _getSemanticCount(const String&);

            virtual ~VertexFormat() {}
         public:
            VertexFormat() : byteSize(0), indexSize(0) {}

            void addFloat(const LString&);
            void addInt(const LString&);
            void addUint(const LString&);

            void addVector2(const LString&);
            void addVector3(const LString&);
            void addVector4(const LString&);

            uint32 getByteSize() const { return byteSize; }
            uint32 getNumOfElements() const { return indexSize; }

            LArray<InputElement> getVertexDescription() const
               { return desc; }
            LArray<uint32> getDynamicIndex() const
               { return dynamIndex; }

            void destroy();
         };
      }
   }
#endif
