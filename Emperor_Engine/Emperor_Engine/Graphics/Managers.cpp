#include "Managers.hpp"
#include "Engine.hpp"
#include "Light.hpp"
#include "../NodeManager.hpp"
#include "MaterialParser.hpp"



namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      bool ActorManager<RS>::bindNext()
         {
         if(it != activeObjects.end())
            {

            it++;
            return true;
            }
         return false;
         }

      template <RenderSystem RS>
      void ActorManager<RS>::render()
         {
         auto i = it - 1;
         auto mat = NO_VIRT(Actor<RS>, *i, getMaterial)();
         auto mesh = NO_VIRT(Actor<RS>, *i, getMesh)();


         NO_VIRT(Mesh<RS>, mesh, bindMesh)();
         Engine<RS>::getPtr()->_getPlatformDevice().bindPrimitive(((Mesh<RS>*)mesh)->getPrimitive());
         NO_VIRT(Material<RS>, mat, draw)(Engine<RS>::getPtr()->renderID(), Engine<RS>::getPtr()->renderQuality(), 0, NO_VIRT(Mesh<RS>, mesh, getIndexCount)());
         }

      template <RenderSystem RS>
      void NodeManager<RS>::init()
         {
         nodeBuffer = TextureManager<RS>::getPtr()->newTex1D("__EMP_NODES__");
         nodeBuffer->initCustom(MAX_NODES * 4, SF_4X4BYTE_FLOAT);

         nodeSampler = SamplerManager<RS>::getPtr()->newResource("__EMP_NODES__");
         nodeSampler->setAddressModeU(TA_WRAP);
         nodeSampler->build();
         }

      template <RenderSystem RS>
      void NodeManager<RS>::_updateBuffers()
         {
         auto inv = CameraManager<RS>::getPtr()->getView();
         auto& objects = Emperor::NodeManager::getPtr()->_getObjects();
         auto& nodeMatrices = Emperor::NodeManager::getPtr()->_getRoster();

         for(auto i = objects.begin(), end = objects.end(); i < end; i++)
            {
            nodeMatrices[(*i)->getIndex()] = (*i)->_getAbs() * inv;
            }

         nodeBuffer->fill((byte*)nodeMatrices.getArray(), MAX_NODES * NODE_SIZE);
         }

      template <RenderSystem RS>
      void NodeManager<RS>::_bindBuffers()
         {
         nodeBuffer->bindToVertex(0);
         nodeSampler->bindToVertex(0);
         }

      template <RenderSystem RS>
      bool CameraManager<RS>::bindNext()
         {
         if(it != activeObjects.end())
            {
            auto node = NO_VIRT(Camera<RS>, *it, getNode)();
            view = inverse(NO_VIRT(Node<>, node, _getAbs)());
            (*it)->_bindRenderer();

            it++;
            return true;
            }
         return false;
         }

      template <RenderSystem RS>
      static bool camCompare(Camera<RS>* a, Camera<RS>* b)
         {
         return a->getPriority() > b->getPriority();
         }

      template <RenderSystem RS>
      void CameraManager<RS>::orderCameras()
         {
         sortList(activeObjects.begin(), activeObjects.end(), camCompare<RS>);
         }

      template <RenderSystem RS>
      Vector<float, 3> LightManager<RS>::compPoint(0.0f);

      template <RenderSystem RS>
      bool LightManager<RS>::lightCompare(Light<RS>* a, Light<RS>* b)
         {
         return ((*a).getNode()->getPosition() - LightManager::compPoint).fLength() <
            ((*b).getNode()->getPosition() - LightManager::compPoint).fLength();
         }

      template <RenderSystem RS>
      void LightManager<RS>::orderLights(const Vector<float, 3>& position)
         {
         LightManager::compPoint = position;
         sortList(activeObjects.begin(), activeObjects.end(), LightManager::lightCompare);
         }

      template <RenderSystem RS>
      bool LightManager<RS>::bindNext(const Vector<float, 3>& pos, bool shadows)
         {
         while(it != activeObjects.end() &&
            (((*it)->getNode()->_getAbsTransform()[3].xyz() - pos).length() > (*it)->getRange() ||
            (((Light<RS>*)(*it))->castsShadows() != shadows)))
            it++;
         if(it != activeObjects.end())
            {


            it++;

            return true;
            }
         return false;
         }

      template <RenderSystem RS>
      bool LightManager<RS>::bindNext(const Vector<float, 3>& pos, LightType t, bool shadows)
         {
         while(it != activeObjects.end() &&
            (((*it)->getNode()->_getAbsTransform()[3].xyz() - pos).length() > (*it)->getRange() ||
            (*it)->getLightType() != t || (((Light<RS>*)(*it))->castsShadows() != shadows)))
            it++;
         if(it != activeObjects.end())
            {


            it++;
            return true;
            }
         return false;
         }


      template <RenderSystem RS>
      void LightManager<RS>::init()
         {
         //Fill Me
         }

      template <RenderSystem RS>
      void VertexFormatManager<RS>::init()
         {
         auto* def = createResource(Defaults::vertexFormatName);
         def->addVector3("POSITION");

         def = createResource(Defaults::vertexFormatPNT);
         def->addVector3("POSITION");
         def->addVector3("NORMAL");
         def->addVector2("TEXCOORD");
         }

      template <RenderSystem RS>
      VertexFormat<RS>* VertexFormatManager<RS>::createResource(const String& r)
         {
         auto out = new VertexFormat<RS>();
         resources[r] = out;
         return out;
         }

      template <RenderSystem RS>
      void MeshManager<RS>::init()
         {
         auto r = newResource(Defaults::mapName);
         ArrayList<String> vals(Defaults::modelDef, Defaults::modelDef + Defaults::modelSize);
         r->_loadOBJ(vals);
         }

      template <RenderSystem RS>
      Mesh<RS>* MeshManager<RS>::createResource(const String& r)
         {
         auto out = new Mesh<RS>();
         resources[r] = out;
         deferLoad(out, resources[Defaults::mapName],
            Engine<RS>::getPtr()->getResourceFolder().stdString() + r);
         return out;
         }

      template <RenderSystem RS>
      void MeshManager<RS>::resolveLoad(byte* d, uint32 s, Mesh<RS>* o)
         {
         try
            {
            o->loadFromMem((char*)d);
            }
         catch(ResourceLoadFailException e)
            {
            LOG(e.what());
            //out->destroy();
            //out = resources[Defaults::mapName];
            }
         }


      template <RenderSystem RS>
      void MaterialManager<RS>::init()
         {
         _buildMaterial<RS>("__EMP_DEFAULTS__",0, Defaults::materialDef);
         }

      template <RenderSystem RS>
      Material<RS>* MaterialManager<RS>::createResource(const String& r)
         {
         Material<RS>* out = new Material<RS>;
         resources[r] = out;
         deferLoad(out, resources[Defaults::mapName],
            Engine<RS>::getPtr()->getResourceFolder().stdString() + r + ".emtl");
         return out;
         }

      template <RenderSystem RS>
      void MaterialManager<RS>::resolveLoad(byte* d, uint32 s, Material<RS>* o)
         {
         try
            {
            _buildMaterial("[Unkown File]", o, (char*)d);
            }
         catch(ResourceLoadFailException e)
            {
            LOG(e.what());
            //out->destroy();
            //out = resources[Defaults::mapName];
            }
         }


      template <RenderSystem RS>
      void TextureManager<RS>::init()
         {
         auto def = newTex2D(Defaults::mapName);
         def->initCustom(2, 2, SF_4X4BYTE_FLOAT);
         def->fill((byte*)Defaults::textureDef, sizeof(Defaults::textureDef));
         }

      template <RenderSystem RS>
      Texture<RS>* TextureManager<RS>::createResource(const String& r)
         {
         auto out = new Texture<RS>();
         resources[r] = out;
         deferLoad(out, resources[Defaults::mapName],
            Engine<RS>::getPtr()->getResourceFolder().stdString() + r);
         return out;
         }

      template <RenderSystem RS>
      void TextureManager<RS>::resolveLoad(byte* d, uint32 s, Texture<RS>* o)
         {
         try
            {
            o->loadFromMem(d, s);
            }
         catch(ResourceLoadFailException e)
            {
            LOG(e.what());
            //out->destroy();
            //out = resources[Defaults::mapName];
            }
         }

      template <RenderSystem RS>
      Texture1D<RS>* TextureManager<RS>::newTex1D(const String& r)
         {
         Texture1D<RS>* out = 0;
         if(resources.find(r) == resources.end())
            {
            out = new Texture1D<RS>();
            resources[r] = out;
            }
         else
            LOG("A resource with that name already exists, aborting...");
         return out;
         }

      template <RenderSystem RS>
      Texture2D<RS>* TextureManager<RS>::newTex2D(const String& r)
         {
         Texture2D<RS>* out = 0;
         if(resources.find(r) == resources.end())
            {
            out = new Texture2D<RS>();
            resources[r] = out;
            }
         else
            LOG("A resource with that name already exists, aborting...");
         return out;
         }

      template <RenderSystem RS>
      Texture3D<RS>* TextureManager<RS>::newTex3D(const String& r)
         {
         Texture3D<RS>* out = 0;
         if(resources.find(r) == resources.end())
            {
            out = new Texture3D<RS>();
            resources[r] = out;
            }
         else
            LOG("A resource with that name already exists, aborting...");
         return out;
         }

      template <RenderSystem RS>
      RenderTarget<RS>* TextureManager<RS>::newTarget(const String& r)
         {
         RenderTarget<RS>* out = 0;
         if(resources.find(r) == resources.end())
            {
            out = new RenderTarget<RS>();
            resources[r] = out;
            }
         else
            LOG("A resource with that name already exists, aborting...");
         return out;
         }


      template <RenderSystem RS>
      DepthStencil<RS>* TextureManager<RS>::newDepth(const String& r)
         {
         DepthStencil<RS>* out = 0;
         if(resources.find(r) == resources.end())
            {
            out = new DepthStencil<RS>();
            resources[r] = out;
            }
         else
            LOG("A resource with that name already exists, aborting...");
         return out;
         }

      template <RenderSystem RS>
      TextureSampler<RS>* SamplerManager<RS>::createResource(const String& r)
         {
         LOG("Create Resource not implemented yet");

         return 0;
         }

      template <RenderSystem RS>
      BlendState<RS>* BlendStateManager<RS>::createResource(const String& r)
         {
         LOG("Create Resource not implemented yet");

         return 0;
         }

#if (EMP_USE_OPENGL == ON)
      template class BlendStateManager<RS_GL43>;
      template class CameraManager<RS_GL43>;
      template class ActorManager<RS_GL43>;
      template class VertexFormatManager<RS_GL43>;
      template class MeshManager<RS_GL43>;
      template class NodeManager<RS_GL43>;
      template class TextureManager<RS_GL43>;
      template class SamplerManager<RS_GL43>;
      template class MaterialManager<RS_GL43>;
      template class LightManager<RS_GL43>;
#endif

#if (EMP_USE_DIRECTX == ON)
      template class BlendStateManager<RS_DX11>;
      template class CameraManager<RS_DX11>;
      template class ActorManager<RS_DX11>;
      template class VertexFormatManager<RS_DX11>;
      template class MeshManager<RS_DX11>;
      template class NodeManager<RS_DX11>;
      template class TextureManager<RS_DX11>;
      template class SamplerManager<RS_DX11>;
      template class MaterialManager<RS_DX11>;
      template class LightManager<RS_DX11>;
#endif

      }
   }