#include "APITextureSampler.hpp"
#include "Engine.hpp"
#include "APITranslations.hpp"

namespace Emperor
{
	namespace Graphics
	{
#if EMP_USE_DIRECTX
		APITextureSampler<RS_DX11>::~APITextureSampler()
         {
         if(sampler)
            sampler->Release();
         }

      void APITextureSampler<RS_DX11>::bindToVertex(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->VSSetSamplers(slot, 1, &sampler);
         }

      void APITextureSampler<RS_DX11>::bindToGeometry(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->GSSetSamplers(slot, 1, &sampler);
         }

      void APITextureSampler<RS_DX11>::bindToFragment(uint32 slot) const
         {
         //Fill Me
         //Use singleton to expose the context
			 Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->VSSetSamplers(slot, 1, &sampler);
         }

      void APITextureSampler<RS_DX11>::build(const TextureFilter& tf, TextureAddressMode u,
         TextureAddressMode v, TextureAddressMode w,
         float mlb, uint32 ma, TextureComparisonMethod cm,
         const Color& bc, float nl, float xl)
         {
         //Use translation functions to set sampler description, see
         //APITranslations.hpp
         //Use EMP_DEVICE_ASSERT_DX to emit exception if creation fails
			 D3D11_SAMPLER_DESC sd;

			 sd.Filter = _dxTranFilter(tf);//Texture Filter for min/mag/mip
			 sd.AddressU = _dxTranAddress(u);//Address mode for the U coord
			 sd.AddressV = _dxTranAddress(v);//Address mode for the V coord
			 sd.AddressW = _dxTranAddress(w);//Address mode for the W coord
			 sd.MipLODBias = mlb;//Mip�s Level of detail bias
			 sd.MaxAnisotropy = ma;//Max level of anisotropy
			 memcpy(sd.BorderColor, bc.data, sizeof(float) * 4); //Copy in the color value for the border
			 sd.MinLOD = nl;//Minimum Level of Detail
			 sd.MaxLOD = xl;//Maximum Level of Detail
			 sd.ComparisonFunc = _dxTranCompare(cm);//Compares sample data

			 EMP_DEVICE_ASSERT_DX("Creating sampler state", CreateSamplerState(&sd, &sampler));

         }
#endif

#if EMP_USE_OPENGL
		APITextureSampler<RS_GL43>::~APITextureSampler()
		{

		}

		void APITextureSampler<RS_GL43>::build(const TextureFilter& tf, TextureAddressMode u,
			TextureAddressMode v, TextureAddressMode w, float mlb, uint32 ma, TextureComparisonMethod cm,
			const Color& bc, float nl, float xl)
		{
			//Use translation functions to set sampler parameters, see
			//APITranslations.hpp
			//If a gl error occurs, emit error with EMP_DEVICE_ERROR
			GLuint e = glGetError();
			glGenSamplers(1, &samplerID); 
			glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_S, _glTranAddress(u));			
			glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_T, _glTranAddress(v)); 
			glSamplerParameteri(samplerID, GL_TEXTURE_WRAP_R, _glTranAddress(w));
			glSamplerParameterf(samplerID, GL_TEXTURE_MAX_LOD, xl);
			glSamplerParameterf(samplerID, GL_TEXTURE_MIN_LOD, nl);
			glSamplerParameteri(samplerID, GL_TEXTURE_MAG_FILTER, _glTranFilter(tf)); //something wrong here
			glSamplerParameteri(samplerID, GL_TEXTURE_MIN_FILTER, _glTranFilter(tf)); 
			glSamplerParameterf(samplerID, GL_TEXTURE_LOD_BIAS, mlb);
			glSamplerParameteri(samplerID, GL_TEXTURE_COMPARE_FUNC, _glTranCompare(cm));
			glSamplerParameterfv(samplerID, GL_TEXTURE_BORDER_COLOR, bc.data);
			if (GLEW_EXT_texture_filter_anisotropic) {
				GLfloat mA;
				glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &mA);
				if (ma > mA)//maxAnisotropy) 
					ma = (uint32)mA;//maxAnisotropy;
				glSamplerParameterf(samplerID, GL_TEXTURE_MAX_ANISOTROPY_EXT, mA);
			}
			
			if (e != GL_NO_ERROR){
				EMP_DEVICE_ERROR(String("APITextureSampler: build: Failed"));
			}
			else if (e == GL_NO_ERROR){
				EMP_DEVICE_ERROR(String("APITextureSampler: build: Passed"));
			}
		}

		//pass
		void APITextureSampler<RS_GL43>::bindToVertex(uint32 slot) const
		{
			//Fill Me
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindSampler(slot, samplerID);			
		}
		//no output, black screen
		void APITextureSampler<RS_GL43>::bindToGeometry(uint32 slot) const
		{
			//Fill Me			
			glActiveTexture(GL_TEXTURE0 + slot);			
			glBindSampler(slot, samplerID);
		}
		//pass
		void APITextureSampler<RS_GL43>::bindToFragment(uint32 slot) const
		{
			//Fill Me
			glActiveTexture(GL_TEXTURE0 + slot);
			glBindSampler(slot, samplerID);				
		}
#endif
	}
}