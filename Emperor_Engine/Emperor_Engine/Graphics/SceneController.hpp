#ifndef __EMP_GFX_SCENE_CONTROLLER_HPP__
#define __EMP_GFX_SCENE_CONTROLLER_HPP__

#include <Graphics/iSceneController.hpp>
#include "Managers.hpp"
#include "../NodeManager.hpp"
#include "../AIPath/PathnodeManager.hpp"
#include "../AIPath/SolverManager.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      class SceneController : public iSceneController
         {
         private:
         protected:
         public:
            ActorManager<RS> actMan;
            CameraManager<RS> camMan;
            LightManager<RS> lgtMan;
            NodeManager<RS> nodMan;
			PathnodeManager pthMan;

            //LAB: Should call the create object function on the appropriate
            //manager

            iActor* createActor()
               { return actMan.createObject(); }
            iCamera* createCamera()
               { return camMan.createObject(); }
            iLight* createLight()
               { return lgtMan.createObject(); }

			iPathnode* createPathnode()
			{
				return pthMan.createPathnode();
			}

			iPathSolver* createPathSolver(iPathnode* n, iNode* a)
			{
				return SolverManager<>::getPtr()->getPathSolver(n, a);
			}
            //This one is correct, leave it alone
            iNode* createNode()
               { return Emperor::NodeManager::getPtr()->createObject(); }
	  };
   }
   }

#endif