#include "VertexFormat.hpp"
#include "Managers.hpp"

namespace Emperor
   {
   namespace Graphics
      {
      template <RenderSystem RS>
      void VertexFormat<RS>::_addSize(uint32 size)
         {
         dynamIndex.push_back(byteSize);
         byteSize += size;
         indexSize++;
         }

      template <RenderSystem RS>
      uint32 VertexFormat<RS>::_getSemanticCount(const String& s)
         {
         auto i = semanticCounter.find(s);
         if(i != semanticCounter.end())
            {
            uint32 r = (*i).second + 1;
            semanticCounter[s] = r;
            return r;
            }
         else
            {
            semanticCounter[s] = 0;
            return 0;
            }
         }

      template <RenderSystem RS>
      void VertexFormat<RS>::addFloat(const LString& s)
         {
         InputElement d = {&s.front(), _getSemanticCount(s.stdString()),
            SF_1X4BYTE_FLOAT, 0, byteSize, 0};
         desc.push_back(d);
         _addSize(sizeof(float));
         }

      template <RenderSystem RS>
      void VertexFormat<RS>::addUint(const LString& s)
         {
         InputElement d = {&s.front(), _getSemanticCount(s.stdString()),
            SF_1X4BYTE_UINT, 0, byteSize, 0};
         desc.push_back(d);
         _addSize(sizeof(uint32));
         }

      template <RenderSystem RS>
      void VertexFormat<RS>::addInt(const LString& s)
         {
         InputElement d = {&s.front(), _getSemanticCount(s.stdString()),
            SF_1X4BYTE_SINT, 0, byteSize, 0};
         desc.push_back(d);
         _addSize(sizeof(int));
         }

      template <RenderSystem RS>
      void VertexFormat<RS>::addVector2(const LString& s)
         {
         InputElement d = {&s.front(), _getSemanticCount(s.stdString()),
            SF_2X4BYTE_FLOAT, 0, byteSize, 0};
         desc.push_back(d);
         _addSize(sizeof(Vector<float, 2>));
         }

      template <RenderSystem RS>
      void VertexFormat<RS>::addVector3(const LString& s)
         {
         InputElement d = {&s.front(), _getSemanticCount(s.stdString()),
            SF_3X4BYTE_FLOAT, 0, byteSize, 0};
         desc.push_back(d);
         _addSize(sizeof(Vector<float, 3>));
         }

      template <RenderSystem RS>
      void VertexFormat<RS>::addVector4(const LString& s)
         {
         InputElement d = {&s.front(), _getSemanticCount(s.stdString()),
            SF_4X4BYTE_FLOAT, 0, byteSize, 0};
         desc.push_back(d);
         _addSize(sizeof(Vector<float, 4>));
         }

      template <RenderSystem RS>
      void VertexFormat<RS>::destroy()
         {
         VertexFormatManager<RS>::getPtr()->removeResource(this);
         delete this;
         }

#if EMP_USE_DIRECTX
      template class VertexFormat<RS_DX11>;
#endif

#if EMP_USE_OPENGL
      template class VertexFormat<RS_GL43>;
#endif
      }
   }