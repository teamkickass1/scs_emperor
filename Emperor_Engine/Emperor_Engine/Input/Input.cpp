#include "Input.hpp"
#include "InputManager.hpp"

namespace Emperor
   {
   namespace Input
      {
      void Input::registerCallback(InputID id,InputState state,INPUT_CALLBACK(func),void* data)
         {
         //Fill Me
         }

      void Input::unregisterCallback(InputID id,InputState state,INPUT_CALLBACK(func),void* data)
         {
         auto& a = callbacks[_KeyPair(id,state)];

         for(auto i = a.begin(),end = a.end(); i != end; i++)
            {
            if(i->first == func && i->second == data)
               {
               a.erase(i);
               return;
               }
            }
         LOG("Callback was not found, unregisterCallback aborted");
         }


      void Input::destroy()
         {
         InputManager::getPtr()->removeResource(this);
         delete this;
         }

      InputState _convertMouseButton(InputID i, const DIMOUSESTATE& s)
         {
         switch(i)
            {
            case II_MOUSE_LEFT:
               return (InputState)((int)(s.rgbButtons[0] >> 7) + 1);
            case II_MOUSE_RIGHT:
               return (InputState)((int)(s.rgbButtons[1] >> 7) + 1);
            case II_MOUSE_MIDDLE:
               return (InputState)((int)(s.rgbButtons[2] >> 7) + 1);
            case II_MOUSE_BUTTON1:
               return (InputState)((int)(s.rgbButtons[3] >> 7) + 1);
            default:
               return IS_NONE;
            }
         }

      void Input::_fireEvent(const DIMOUSESTATE& s)
         {
         //Fill Me


         }


      uint32 _convertKey(InputID r)
         {
         switch(r)
            {
            case II_KEY_UNKNOWN:
               return 0;
            case II_KEY_DOT:
               return DIK_PERIOD;
            case II_KEY_COMMA:
               return DIK_COMMA;
            case II_KEY_SEMICOLON:
               return DIK_SEMICOLON;
            case II_KEY_TILDE:
               return DIK_GRAVE;
            case II_KEY_HYPHON:
               return DIK_MINUS;
            case II_KEY_EQUAL:
               return DIK_EQUALS;
            case II_KEY_BACKSLASH:
               return DIK_BACKSLASH;
            case II_KEY_RIGHT_BRACE:
               return DIK_RBRACKET;
            case II_KEY_LEFT_BRACE:
               return DIK_LBRACKET;
            case II_KEY_QUOTE:
               return DIK_APOSTROPHE;
            case II_KEY_SLASH:
               return DIK_SLASH;
            case II_KEY_BACKSPACE:
               return DIK_BACKSPACE;
            case II_KEY_TAB:
               return DIK_TAB;
            case II_KEY_ENTER:
               return DIK_RETURN;
            case II_KEY_PAUSE:
               return DIK_PAUSE;
            case II_KEY_CAPS_LOCK:
               return DIK_CAPSLOCK;
            case II_KEY_ESC:
               return DIK_ESCAPE;
            case II_KEY_SPACE:
               return DIK_SPACE;
            case II_KEY_PG_UP:
               return DIK_PGUP;
            case II_KEY_PG_DOWN:
               return DIK_PGDN;
            case II_KEY_END:
               return DIK_END;
            case II_KEY_HOME:
               return DIK_HOME;
            case II_KEY_LEFT:
               return DIK_LEFT;
            case II_KEY_UP:
               return DIK_UP;
            case II_KEY_RIGHT:
               return DIK_RIGHT;
            case II_KEY_DOWN:
               return DIK_DOWN;
            case II_KEY_PRT_SC:
               return DIK_SYSRQ;
            case II_KEY_INS:
               return DIK_INSERT;
            case II_KEY_DEL:
               return DIK_DELETE;
            case II_KEY_0:
               return DIK_0;
            case II_KEY_1:
               return DIK_1;
            case II_KEY_2:
               return DIK_2;
            case II_KEY_3:
               return DIK_3;
            case II_KEY_4:
               return DIK_4;
            case II_KEY_5:
               return DIK_5;
            case II_KEY_6:
               return DIK_6;
            case II_KEY_7:
               return DIK_7;
            case II_KEY_8:
               return DIK_8;
            case II_KEY_9:
               return DIK_9;
            case II_KEY_A:
               return DIK_A;
            case II_KEY_B:
               return DIK_B;
            case II_KEY_C:
               return DIK_C;
            case II_KEY_D:
               return DIK_D;
            case II_KEY_E:
               return DIK_E;
            case II_KEY_F:
               return DIK_F;
            case II_KEY_G:
               return DIK_G;
            case II_KEY_H:
               return DIK_H;
            case II_KEY_I:
               return DIK_I;
            case II_KEY_J:
               return DIK_J;
            case II_KEY_K:
               return DIK_K;
            case II_KEY_L:
               return DIK_L;
            case II_KEY_M:
               return DIK_M;
            case II_KEY_N:
               return DIK_N;
            case II_KEY_O:
               return DIK_O;
            case II_KEY_P:
               return DIK_P;
            case II_KEY_Q:
               return DIK_Q;
            case II_KEY_R:
               return DIK_R;
            case II_KEY_S:
               return DIK_S;
            case II_KEY_T:
               return DIK_T;
            case II_KEY_U:
               return DIK_U;
            case II_KEY_V:
               return DIK_V;
            case II_KEY_W:
               return DIK_W;
            case II_KEY_X:
               return DIK_X;
            case II_KEY_Y:
               return DIK_Y;
            case II_KEY_Z:
               return DIK_Z;
            case II_KEY_LEFT_WINDOW:
               return DIK_LWIN;
            case II_KEY_RIGHT_WINDOW:
               return DIK_RWIN;
            case II_KEY_NP_0:
               return DIK_NUMPAD0;
            case II_KEY_NP_1:
               return DIK_NUMPAD1;
            case II_KEY_NP_2:
               return DIK_NUMPAD2;
            case II_KEY_NP_3:
               return DIK_NUMPAD3;
            case II_KEY_NP_4:
               return DIK_NUMPAD4;
            case II_KEY_NP_5:
               return DIK_NUMPAD5;
            case II_KEY_NP_6:
               return DIK_NUMPAD6;
            case II_KEY_NP_7:
               return DIK_NUMPAD7;
            case II_KEY_NP_8:
               return DIK_NUMPAD8;
            case II_KEY_NP_9:
               return DIK_NUMPAD9;
            case II_KEY_NP_MULTIPLY:
               return DIK_NUMPADSTAR;
            case II_KEY_NP_ADD:
               return DIK_NUMPADPLUS;
            case II_KEY_NP_ENTER:
               return DIK_NUMPADENTER;
            case II_KEY_NP_SUBTRACT:
               return DIK_NUMPADMINUS;
            case II_KEY_NP_DECIMAL:
               return DIK_NUMPADPERIOD;
            case II_KEY_NP_DIVIDE:
               return DIK_NUMPADSLASH;
            case II_KEY_F1:
               return DIK_F1;
            case II_KEY_F2:
               return DIK_F2;
            case II_KEY_F3:
               return DIK_F3;
            case II_KEY_F4:
               return DIK_F4;
            case II_KEY_F5:
               return DIK_F5;
            case II_KEY_F6:
               return DIK_F6;
            case II_KEY_F7:
               return DIK_F7;
            case II_KEY_F8:
               return DIK_F8;
            case II_KEY_F9:
               return DIK_F9;
            case II_KEY_F10:
               return DIK_F10;
            case II_KEY_F11:
               return DIK_F11;
            case II_KEY_F12:
               return DIK_F12;
            case II_KEY_NUMLK:
               return DIK_NUMLOCK;
            case II_KEY_SCR_LK:
               return DIK_SCROLL;
            case II_KEY_LEFT_SHIFT:
               return DIK_LSHIFT;
            case II_KEY_RIGHT_SHIFT:
               return DIK_RSHIFT;
            case II_KEY_LEFT_CTRL:
               return DIK_LCONTROL;
            case II_KEY_RIGHT_CTRL:
               return DIK_RCONTROL;
            case II_KEY_LEFT_ALT:
               return DIK_LALT;
            case II_KEY_RIGHT_ALT:
               return DIK_RALT;
            }
            return 0;
         }



      void Input::_fireEvent(const byte* s)
         {
         //Fill Me
         }
      }
   }