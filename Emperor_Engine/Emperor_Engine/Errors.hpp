#ifndef __EMP_ERRORS_HPP__
#define __EMP_ERRORS_HPP__

#include <exception>
#include <string>

//derives exceptions from base exception
#define EMP_DEXP(n) class n : public Exception {public:\
   n(const char* m) : Exception(m) {}; n(const std::string& m) : Exception(m) {} };

namespace Emperor
   {

   class Exception : public std::exception
      {
      private:
         Exception();
      protected:
      public:
         Exception(const char* message) : exception(message) {}
         Exception(const std::string& message) : exception(message.c_str()) {}
      };

   EMP_DEXP(FatalException);
   EMP_DEXP(DeviceFailureException);
   EMP_DEXP(DeviceInitFailureException);
   EMP_DEXP(ResourceLoadFailException);
   EMP_DEXP(NotImplementedException);

#undef EMP_DEXP

   }



#define EMP_ERROR(m) throw(Exception(m))
#define EMP_FATAL_ERROR(m) throw(FatalException(m))
#define EMP_DEVICE_ERROR(m) throw(DeviceFailureException(m))
#define EMP_INIT_ERROR(m) throw(DeviceInitFailureException(m))
#define EMP_RESOURCE_ERROR(m) throw(ResourceLoadFailException(m))
#define EMP_NOT_IMP_ERROR(m) throw(NotImplementedException(m))

#ifdef _DEBUG
#define EMP_DEVICE_NOT_INIT_ASSERT(RS) Engine<RS>::getPtr()->_getPlatformDevice().isActive() ? 0 : EMP_INIT_ERROR("Device was not initialzied")
#else
#define EMP_DEVICE_NOT_INIT_ASSERT()
#endif

   //Specialized Errors
#define EMP_DEVICE_ASSERT_DX(d, f) {HRESULT er;\
   if(FAILED(er = Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeDevice()->f))\
   throw(DeviceFailureException(String("Failed to ") + d + \
   " in DirectX 11\n   Error code: " + std::to_string(er)));}

#define EMP_AUD_DEVICE_ASSERT_DX(d, f) {HRESULT er;\
   if(FAILED(er = Engine<AS_DX11>::getPtr()->_getPlatformDevice()._exposeDevice()->f))\
   throw(DeviceFailureException(String("Failed to ") + d + \
   " in DirectX 11 (XAudio2)\n   Error code: " + std::to_string(er)));}

#define EMP_CONTEXT_ASSERT_DX(d, f) {HRESULT er;\
   if(FAILED(er = Engine<RS_DX11>::getPtr()->_getPlatformDevice()._exposeContext()->f))\
   throw(DeviceFailureException(String("Failed to ") + d + \
   " in DirectX 11\n   Error code: " + std::to_string(er)));}

#define EMP_INIT_ASSERT_DX(d, f) {HRESULT er;\
   if(FAILED(er = f))\
   throw(DeviceInitFailureException(String("Failed to ") + d + \
   " in DirectX 11\n   Error code: " + std::to_string(er)));}

#define EMP_INIT_ASSERT_GL(d, f) {GLenum er = f;\
   if(f != GLEW_OK)\
   throw(DeviceInitFailureException(String("Failed to ") + d + \
   " in OpenGL 4.3\n   Error code: " + (char*)glewGetErrorString(er)));}

#define EMP_INIT_ASSERT_AL(d, f) {uint32 er = f;\
   if(f == 0)\
   throw(DeviceInitFailureException(String("Failed to ") + d + \
   " in OpenAL 1.1\n   Error code: " ));}

#define EMP_CHECK_ERRORS_AL(d) {ALenum er = alGetError();\
   char* em = 0;\
   if(er != AL_NO_ERROR){\
   switch(er) {\
        case AL_INVALID_NAME: em = "Invalid Name";break;\
        case AL_INVALID_ENUM: em = "Invalid Enum";break;\
        case AL_INVALID_VALUE: em = "Invalid Value";break;\
        case AL_INVALID_OPERATION: em = "Invalid Operation";break;\
        case AL_OUT_OF_MEMORY: em = "Out of Memory";break;\
    }\
   throw(DeviceFailureException(String("Failed to ") + d + \
   " in OpenAL 1.1\n   Error: " + em));}}

#endif
