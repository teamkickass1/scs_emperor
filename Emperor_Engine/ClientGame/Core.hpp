#ifndef __I_CORE_HPP__
#define __I_CORE_HPP__

#include <EmperorEngine.hpp>
#define String std::string
#define ArrayList std::vector
#define LOG(m) std::cout << m << std::endl 

class Core
   {
   private:
      Emperor::Graphics::iEngine* gfxEngine;
      Emperor::Utility::Settings settings;
   protected:
      Emperor::Utility::iWindow* w1;
   public:
      Core();
      ~Core();
      void init();
      void run();
      void halt();
   };

#endif